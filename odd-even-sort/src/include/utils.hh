/*
 * $File: utils.hh
 * $Date: Tue Aug 14 18:33:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_UTILS_
#define _HEADER_UTILS_

#include <sys/time.h>

void __log_printf__(const char *fname, const char *func, int line,
		const char *fmt, ...) __attribute__((format(printf, 4, 5)));

#define log_printf(fmt, ...) \
	__log_printf__(__FILE__, __func__, __LINE__, fmt, ## __VA_ARGS__)


/*!
 * \brief print error information and exit the process
 */
void error_exit(const char *fmt, ...) __attribute__((format(printf, 1, 2),
			noreturn));

/*!
 * \brief a timer using the hardware clock to measure real time
 */
class HWTimer
{
	timeval m_start;

	public:
		
		HWTimer();
		double get_sec() const;
};

#endif

// vim: syntax=cpp11.doxygen

