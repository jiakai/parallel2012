#!/usr/bin/env gnuplot
# $File: rt-nthread-cpu.plt
# $Date: Sat Aug 25 11:46:22 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set terminal postscript enhanced color
set output "rt-nthread-cpu.eps"
set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

set title "Total CPU Time Comparison (niter=10000,size=5000x5000)"
set xlabel "# Threads/Processes"
set ylabel "Total CPU Time [sec]" 
set y2label "Total CPU Time(MPI) [sec]" 

set xrange[-1:38]
set xtics 0 4

set y2tics nomirror tc lt 4

cd "data/nthread"
plot \
	"pthread" using 2:7 title "pthread", \
	"omp" using 2:7 title "OMP", \
	"omp_static" using 2:7 title "OMP static", \
	"mpi" using 2:7 title "MPI" axes x1y2

