#!/bin/bash -e
# $File: gen-data.sh
# $Date: Sat Aug 25 16:30:01 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

rm -rf data

mkdir -p data/niter
for bk in seq omp pthread
do
	./extract.py "backend=='$bk' and nthread=='12' and xres=='5000'" > data/niter/$bk
done

mkdir -p data/nthread
for bk in omp omp_static mpi pthread
do
	./extract.py "backend=='$bk' and niter=='10000' and xres=='5000'" > data/nthread/$bk
done

./extract.py "backend=='pthread' and niter=='10000' and nthread=='12'" > data/xres
./extract.py "(backend=='seq' or backend=='seq_allblack') \
	and int(niter)>=30 and xres=='5000'" > data/est-area

join -j 2 data/nthread/{omp,pthread} > data/nthread/omp-pthread
