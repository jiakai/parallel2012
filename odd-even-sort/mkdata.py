#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# $File: mkdata.py
# $Date: Thu Aug 09 21:54:04 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import random
import sys
import array
if len(sys.argv) != 2:
    sys.exit('usage: {} <data size>'.format(sys.argv[0]))


seq = [random.randint(1, 2 ** 30) for i in range(int(sys.argv[1]))]

with open('sort.in', 'w') as f:
    arr = array.array('i', [int(sys.argv[1])])
    arr.extend(seq)
    arr.tofile(f)

seq.sort()
with open('sort.ans', 'w') as f:
    arr = array.array('i', seq)
    arr.tofile(f)

