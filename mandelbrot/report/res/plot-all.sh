#!/bin/bash -e
# $File: plot-all.sh
# $Date: Sat Aug 25 11:46:10 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

./gen-data.sh
find . -name "*.eps" -delete
for i in *.plt
do
	./$i
done

