#!/usr/bin/env gnuplot
# $File: rt-fixsize-bubble.plt
# $Date: Wed Aug 15 23:12:50 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set title "Real Run Time Comparison (Bubble Sort, n=1e6)"
set xlabel "# Processes"
set ylabel "Real Run Time [sec]"
set y2label "Decrease/Original" tc lt 3

set xrange[10:65]
set xtics 12 12
set y2range[-0.5:0.5]
set y2tics 0 0.1 nomirror tc lt 3

set terminal postscript enhanced color
set output "rt-fixsize-bubble.eps"

set style data linespoints

cd "../../data"
plot \
	"bubble-ext/size1000000" using 2:4 title "original", \
	"optimized/bubble-ext/size1000000" using 2:4 title "optimized", \
	"cmp-bubble-size1000000" using 1:(1-$9/$4) title "decrease/original" axes x1y2, \
	0 lt 3 axes x1y2 notitle
