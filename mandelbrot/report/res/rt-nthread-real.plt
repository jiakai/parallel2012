#!/usr/bin/env gnuplot
# $File: rt-nthread-real.plt
# $Date: Sat Aug 25 11:46:30 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set terminal postscript enhanced color
set output "rt-nthread-real.eps"
set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

set title "Real Time Comparison (niter=10000,size=5000x5000)"
set xlabel "# Threads/Processes"
set ylabel "Real Time [sec]" 
set y2label "omp - pthread"

set xrange[-1:38]
set xtics 0 4
set y2tics nomirror tc lt 4
set logscale y2

cd "data/nthread"
plot \
	"pthread" using 2:6 title "pthread", \
	"omp" using 2:6 title "OMP", \
	"omp_static" using 2:6 title "OMP static", \
	"mpi" using 2:6 title "MPI", \
	"omp-pthread" using 1:($6-$12) title "OMP - pthread" axes x1y2

