/*
 * $File: tcp.cc
 * $Date: Wed Aug 29 19:51:01 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "server/tcp.hh"
#include "utils.hh"
#include "config.hh"

#include <cstdio>

#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


struct TCPServer::DVector
{
	double x, y, z;
	DVector& operator = (const Body &b)
	{
		const Vector &v = b.center;
		x = v.x; y = v.y; z = v.z;
		return *this;
	}
} __attribute__((packed));

TCPServer::TCPServer(int port, int buf_size):
	m_port(port),
	m_buf_head(0), m_buf_tail(0), m_buf_size(0),
	m_buf_max_size(buf_size)
{
}

TCPServer::~TCPServer()
{
	for (int i = 0; i < m_buf_max_size; i ++)
		delete []m_buf[i];
	delete []m_buf;
}

void TCPServer::do_run(WorkerBase &worker)
{
	int nbody = worker.get_nr_body();
	Body *body = worker.get_body_buf();
	m_buf = new DVector*[m_buf_max_size];
	for (int i = 0; i < m_buf_max_size; i ++)
		m_buf[i] = new DVector[nbody];

	m_worker = &worker;
	m_stop_flag = false;
	pthread_t pid_calc;
	PTHREAD_CHKERR_CALL(pthread_create, &pid_calc,
			NULL, thread_calc_wrapper, this);

	int fd = wait_client();

	// send metadata
	{
		double cbuf[7] = {conf::time_delta};
		memcpy(cbuf + 1, conf::range, sizeof(double) * 6);
		errchk_write(fd, cbuf, sizeof(double) * 7);
		write_var<int32_t>(fd, nbody);
		double *tmp = new real_t[nbody * 2];
		for (int i = 0, p = 0; i < nbody; i ++)
		{
			tmp[p ++] = body[i].mass;
			tmp[p ++] = body[i].radius;
		}
		errchk_write(fd, tmp, sizeof(double) * nbody * 2);
		delete []tmp;
	}


	DVector *tmp = new DVector[nbody];
	for (int cur_frnum = 0; ; cur_frnum ++)
	{
		int frnum = read_var<int32_t>(fd);
		if (frnum == -1)
			break;
		{
			LOCK(m_cond_buf);
			while (!m_buf_size)
			{
				m_cond_buf.wait();
				printf("frame %d: computation too slow ...\n", frnum);
			}
			m_buf_size --;
			memcpy(tmp, m_buf[m_buf_head], sizeof(DVector) * nbody);
			if ((++ m_buf_head) == m_buf_max_size)
				m_buf_head = 0;
		}
		m_cond_buf.signal();
		if (cur_frnum != frnum)
			fprintf(stderr, "cur_frnum=%d req_frnum=%d\n", cur_frnum, frnum);
		errchk_write(fd, tmp, sizeof(DVector) * nbody);
	}
	read(fd, tmp, 4); // avoid failure while binding in next startup
	m_stop_flag = true;
	m_cond_buf.signal();
	if (close(fd))
		error_exit("failed to close socket: %m");
	delete []tmp;
	PTHREAD_CHKERR_CALL(pthread_join, pid_calc, NULL);
}

void* TCPServer::thread_calc_wrapper(void*self)
{
	static_cast<TCPServer*>(self)->thread_calc();
	return NULL;
}

void TCPServer::thread_calc()
{
	int nbody = m_worker->get_nr_body();
	Body *body = m_worker->get_body_buf();
	while (!m_stop_flag)
	{
		{
			LOCK(m_cond_buf);
			while (m_buf_size == m_buf_max_size && !m_stop_flag)
				m_cond_buf.wait();
			if (m_stop_flag)
				return;
			m_buf_size ++;
			DVector *p = m_buf[m_buf_tail ++];
			if (m_buf_tail == m_buf_max_size)
				m_buf_tail = 0;
			for (int i = 0; i < nbody; i ++)
				p[i] = body[i];
		}
		m_cond_buf.signal();
		m_worker->next_step();
	}
}

int TCPServer::wait_client()
{
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0)
		error_exit("failed to open socket: %m");

	struct sockaddr_in srv_addr;
	bzero(&srv_addr, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(m_port);
	if (bind(sockfd, (struct sockaddr*)&srv_addr, sizeof(srv_addr)) < 0)
		error_exit("faied to bind: %m");

	printf("listen on %d ...\n", m_port);
	listen(sockfd, 1);

	struct sockaddr_in cli_addr;
	socklen_t cli_addr_len = sizeof(cli_addr);
	int clifd = accept(sockfd, (struct sockaddr*)&cli_addr, &cli_addr_len);

	char hostbuf[NI_MAXHOST] = "?", servbuf[NI_MAXSERV] = "?";
	getnameinfo((struct sockaddr*)&cli_addr, cli_addr_len,
			hostbuf, NI_MAXHOST,
			servbuf, NI_MAXSERV,
			NI_NUMERICHOST | NI_NUMERICSERV);
	printf("connected by %s:%s\n", hostbuf, servbuf);

	if (close(sockfd))
		error_exit("failed to close socket: %m");
	return clifd;
}

// vim: syntax=cpp11.doxygen

