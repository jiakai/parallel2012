/*
 * $File: seq.hh
 * $Date: Mon Aug 27 21:58:39 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SPTREE_SEQ_
#define _HEADER_SPTREE_SEQ_

#include "sptree/base.hh"

/*!
 * \brief build the tree sequentially
 */
class SeqSPTreeFactory: public SPTreeFactoryBase
{
	class SPTree;
	friend class SeqSPTreeFactory::SPTree;

	static SPTree* do_mktree(bodypbuf_t &body, int depth);

	public:
		sptptr_t mktree(bodypbuf_t &body);
};

#endif // _HEADER_SPTREE_SEQ_

// vim: syntax=cpp11.doxygen

