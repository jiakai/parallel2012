#!/bin/bash -e
# $File: generate-ext.sh
# $Date: Wed Aug 15 23:07:15 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in bubble qsort optimized/bubble optimized/qsort
do
	rm -f $i-ext/*
	./extract.py $i $i-ext
done

join qsort-ext/nproc48 optimized/qsort-ext/nproc48 > cmp-qsort-nproc48
join bubble-ext/nproc60 optimized/bubble-ext/nproc60 > cmp-bubble-nproc60
join -j 2 bubble-ext/size1000000 optimized/bubble-ext/size1000000 | tail -n +2 > cmp-bubble-size1000000 
join -j 2 qsort-ext/size1000000000 optimized/qsort-ext/size1000000000 | tail -n +2 > cmp-qsort-size1000000000

