#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# $File: bin2text.py
# $Date: Thu Aug 09 21:53:06 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import array
import sys

arr = array.array('i')
try:
    while True:
        arr.fromfile(sys.stdin, 1024 * 1024)
except EOFError:
    pass

print '\n'.join([str(i) for i in arr])

