# -*- coding: utf-8 -*-
# $File: phasecpu.py
# $Date: Thu Aug 30 22:24:02 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""proportion of CPU time in each phase"""

from nbdplt.utils import *

from matplotlib import cm
import matplotlib.pyplot as plt
import numpy as np

stat_items = ['gravity_cpu', 'collision_cpu', 'mktree_cpu']
worker_lst = ['seq', 'pthread', 'pthreadpf', 'omp', 'mpi']
def plot_tot(data, ax):
    width = 0.25
    s = sumentry(data)
    rects = list()
    ind = np.arange(len(worker_lst))
    for i in range(len(stat_items)):
        rects.append(ax.bar(
            ind + width * i,
            [getattr(w, stat_items[i])/w.tot_cpu for w in
                [s[x] for x in worker_lst]],
            width, color = cm.jet(float(i) / len(stat_items))))
    ax.set_title('Different Workers (average of all available data)')
    ax.set_ylabel('Phase CPU Time/Total CPU Time')
    ax.set_xlabel('Worker Type')
    ax.set_xticks(ind + width * 1.5)
    ax.set_xticklabels(worker_lst)
    ax.legend((i[0] for i in rects), (i[:-4] for i in stat_items))

def plot_size(data, ax):
    data = filter(default_filter(worker = 'seq', size = None), data)
    data.sort(cmp = lambda x, y: cmp(x.size, y.size))
    ax.set_title('Different Input Size (worker=seq)')
    ax.set_ylabel('Phase CPU Time/Total CPU Time')
    ax.set_xlabel('Input Size')
    ax.set_xscale('log')
    do_plot(data, ax, 'size')

def mktitle(worker, thread_name):
    return 'Different Number of {1} ({0},{2} bodies,{3} iter)'. \
            format(worker, thread_name, default_filter.size,
                    default_filter.niter)
def plot_nthread_pthread(data, ax):
    data = filter(default_filter(worker = 'pthread', nthread = None), data)
    data.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
    ax.set_title(mktitle('pthread', 'Thread'))
    ax.set_ylabel('Phase CPU Time/Total CPU Time')
    ax.set_xlabel('Number of Threads')
    do_plot(data, ax, 'nthread_real')

def plot_nthread_mpi(data, ax):
    data = filter(default_filter(worker = 'mpi', nthread = None), data)
    data.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
    ax.set_title(mktitle('mpi', 'Process'))
    ax.set_ylabel('Phase CPU Time/Total CPU Time')
    ax.set_xlabel('Number of Processes')
    do_plot(data, ax, 'nthread_real')

def do_plot(data, ax, xattr):
    cyc = get_line_cycler()
    for item in stat_items:
        ax.plot([getattr(i, xattr) for i in data], [getattr(i, item)/i.tot_cpu for i in
            data], next(cyc), label = item[:-4])
    ax.legend()

def plot(data):
    fig = plt.figure()
    fig.suptitle('Proportion of CPU Time of Different Phases')
    plot_tot(data, fig.add_subplot(221))
    plot_size(data, fig.add_subplot(222))
    plot_nthread_pthread(data, fig.add_subplot(223))
    plot_nthread_mpi(data, fig.add_subplot(224))
    plt.savefig('phasecpu.pdf')

