/*
 * $File: seq.hh
 * $Date: Sun Sep 02 10:47:40 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_SEQ_
#define _HEADER_WORKER_SEQ_

#include "worker/base.hh"

class SeqWorker: public WorkerBase
{
	public:
		SeqWorker(int w, int h, real_t t0);
		real_t **next_step(BoundaryBase &bd_cond);
};

#endif // _HEADER_WORKER_SEQ_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

