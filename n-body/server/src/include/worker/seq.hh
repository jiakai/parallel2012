/*
 * $File: seq.hh
 * $Date: Wed Aug 29 16:50:15 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_SEQ_
#define _HEADER_WORKER_SEQ_

#include "worker/base.hh"

class SeqWorker: public WorkerBase
{
	public:
		SeqWorker(Body *body, int nbody, SPTreeFactoryBase &tree_factory);

		const char* get_name()
		{ return "sequential"; }

		void next_step();
};

#endif // _HEADER_WORKER_SEQ_

// vim: syntax=cpp11.doxygen

