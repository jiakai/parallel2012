/*
 * $File: x11.cc
 * $Date: Mon Sep 03 00:31:52 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "disp/x11.hh"

#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <cstdio>
#include <cassert>

#include <algorithm>

X11Disp::X11Disp(real_t val_max, int width, int height,
		const char *window_name, const char *disp_name):
	m_window_name(window_name), m_disp_name(disp_name),
	m_stop_flag(false),
	m_val_max(val_max), m_width(width), m_height(height)
{
}

void X11Disp::init_x11()
{
	m_pt_gc = alloc_2d<GC>(m_width, m_height);
	if (!XInitThreads())
		error_exit("failed to init X threads");
	if ((m_display = XOpenDisplay(m_disp_name)) == NULL)
		error_exit("failed to open display: %s", XDisplayName(m_disp_name));

	int screen = DefaultScreen(m_display),
		disp_w = DisplayWidth(m_display, screen),
		disp_h = DisplayHeight(m_display, screen);
	m_win = XCreateSimpleWindow(m_display, RootWindow(m_display, screen),
			(disp_w - m_width) / 2, (disp_h - m_height) / 2, // x, y
			m_width, m_height,
			1, // border_m_width
			WhitePixel(m_display, screen),	// border
			BlackPixel(m_display, screen));	// background

	XStoreName(m_display, m_win, m_window_name);
	init_gc();
	
	XMapWindow(m_display, m_win);
	XSync(m_display, false);

	pthread_t pid;
	PTHREAD_CHKERR_CALL(pthread_create, &pid, NULL,
			thread_draw_wrapper, this);
}

X11Disp::~X11Disp()
{
	if (m_stop_flag)
	{
		XDestroyWindow(m_display, m_win);
		XCloseDisplay(m_display);
		delete []m_gc;
		free_2d(m_pt_gc, m_height);
	}
}

void X11Disp::init_gc()
{
	Pixel *colors = load_colormap(m_ngc);
	m_gc = new GC[m_ngc];
	Colormap colormap = DefaultColormap(m_display, DefaultScreen(m_display));
	assert(m_ngc > 0);
	for (int i = 0; i < m_ngc; i ++)
	{
		m_gc[i] = XCreateGC(m_display, m_win, 0, NULL);
		char name[10];
		sprintf(name, "#%.2X%.2X%.2X", colors[i].r, colors[i].g, colors[i].b);
		XColor color;
		XParseColor(m_display, colormap, name, &color);
		XAllocColor(m_display, colormap, &color);
		XSetForeground(m_display, m_gc[i], color.pixel);
	}
	for (int i = 0; i < m_height; i ++)
		std::fill(m_pt_gc[i], m_pt_gc[i] + m_width, m_gc[0]);
	delete []colors;
}

void X11Disp::do_run(WorkerBase &worker, BoundaryBase &bd_cond)
{
	init_x11();
	FPSCalculator fps("calc");
	for (; ;)
	{
		{
			LOCK(m_lock_stop_flag);
			if (m_stop_flag)
				return;
		}
		fps.trigger();
		real_t **buf = worker.next_step(bd_cond);
		for (int i = 0; i < m_height; i ++)
			for (int j = 0; j < m_width; j ++)
				m_pt_gc[i][j] = m_gc[std::max(
						std::min(
							int(buf[i][j] / m_val_max * m_ngc), m_ngc - 1), 0)];
	}
}

void* X11Disp::thread_draw_wrapper(void*self)
{
	pthread_detach(pthread_self());
	static_cast<X11Disp*>(self)->thread_draw();
	return NULL;
}

void X11Disp::thread_draw()
{
	Atom wm_del_msg = XInternAtom(m_display, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(m_display, m_win, &wm_del_msg, 1);
	XSelectInput(m_display, m_win, KeyPressMask);
	FPSCalculator fps("draw");
	for (; ;)
	{
		fps.trigger();
		for (int i = 0; i < m_height; i ++)
			for (int j = 0; j < m_width; j ++)
				XDrawPoint(m_display, m_win, m_pt_gc[i][j], j, i);
		// f{{{ event handling
		while (XPending(m_display))
		{
			XEvent ev;
			XNextEvent(m_display, &ev);
			switch(ev.type)
			{
				case KeyPress:
					{
						int key = XLookupKeysym(&ev.xkey, 0);
						if (key == XK_q)
						{
							LOCK(m_lock_stop_flag);
							m_stop_flag = true;
							return;
						}
					}
					break;
				case ClientMessage:
					if (ev.xclient.data.l[0] == wm_del_msg)
					{
						LOCK(m_lock_stop_flag);
						m_stop_flag = true;
						return;
					}
					break;
			}
		}
		// f}}}
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

