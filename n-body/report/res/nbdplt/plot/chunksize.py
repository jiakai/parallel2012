# -*- coding: utf-8 -*-
# $File: chunksize.py
# $Date: Fri Aug 31 02:02:44 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from nbdplt.utils import *
import matplotlib.pyplot as plt

def plot(data):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('Total CPU Time vs Chunksize ({0} bodies, {1} iters)'
            .format(default_filter.size, default_filter.chunksize))
    ax.set_xlabel('Chunksize')
    ax.set_ylabel('Relative Total CPU time')
    cyc = get_line_cycler()
    for nthread in [12, 24, 36, 48]:
        d = filter(default_filter(worker = 'mpi', chunksize = None, nthread =
            nthread), data)
        d.sort(cmp = lambda x, y: cmp(x.chunksize, y.chunksize))
        ax.plot([i.chunksize for i in d], [i.tot_cpu/d[0].tot_cpu for i in d],
                next(cyc), label = '#processes={}'.format(nthread))
    ax.legend()
    plt.savefig('chunksize.pdf')
