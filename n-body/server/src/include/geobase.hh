/*
 * $File: geobase.hh
 * $Date: Wed Aug 29 16:45:14 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

/*!
 * \brief basic geometric structures and helper functions
 */

#ifndef _HEADER_GEOBASE_
#define _HEADER_GEOBASE_

#include <cmath>
#include <cassert>

#include <limits>

#include "utils.hh"

typedef double real_t;
static const real_t
	EPS = 1e-6,
	EPS_COLLIDE = 1e-4,	// check for collision used in Body::collide
	REAL_MAX = std::numeric_limits<real_t>::max();

template<typename T>
bool update_min(T &dest, const T &val)
{ if (val < dest) {dest = val; return true;} return false; }

template<typename T>
bool update_max(T &dest, const T &val)
{ if (dest < val) {dest = val; return true;} return false; }

static inline real_t sqr(real_t x)
{ return x * x; }

/*!
 * \brief return -1 if x < 0, 0 if x == 0, and 1 if x > 0
 */
static inline int get_sign(real_t x)
{
	if (fabs(x) < EPS)
		return 0;
	return x < 0 ? -1 : 1;
}

/*!
 * \brief 3D vector
 */
struct Vector
{
	real_t x, y, z;

	explicit Vector(real_t x_ = 0, real_t y_ = 0, real_t z_ = 0):
		x(x_), y(y_), z(z_)
	{}

	Vector(const Vector &p0, const Vector &p1):
		x(p1.x - p0.x), y(p1.y -p0.y), z(p1.z - p0.z)
	{}

	/*!
	 * \brief return a component in this vector; c = 0, 1, 2 for x, y, z
	 */
	real_t comp(int c) const
	{ return *(&x + c); }

	/*!
	 * \brief return a component in this vector; c = 0, 1, 2 for x, y, z
	 */
	real_t& comp(int c)
	{ return *(&x + c); }

	int get_max_comp(real_t &v) const
	{ 
		int ret = 0;
		v = x;
		if (y > v) v = y, ret = 1;
		if (z > v) v = z, ret = 2;
		return ret;
	}

	real_t mod_sqr() const
	{ return x * x + y * y + z * z; }

	real_t mod() const
	{ return sqrt(mod_sqr()); }

	real_t dot(const Vector &v) const
	{ return x * v.x + y * v.y + z * v.z; }

	Vector cross(const Vector &v) const
	{ return Vector(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }


	Vector operator + (const Vector &v) const
	{ return Vector(x + v.x, y + v.y, z + v.z); }

	Vector& operator += (const Vector &v)
	{ x += v.x; y += v.y; z += v.z; return *this; }

	Vector operator - (const Vector &v) const
	{ return Vector(x - v.x, y - v.y, z - v.z); }

	Vector operator - () const
	{ return Vector(-x, -y, -z); }

	Vector& operator -= (const Vector &v)
	{ x -= v.x; y -= v.y; z -= v.z; return *this; }


	Vector operator * (real_t f) const
	{ return Vector(x * f, y * f, z * f); }

	Vector& operator *= (real_t f)
	{ x *= f; y *= f; z *= f; return *this; }

	Vector operator / (real_t f) const
	{ return *this * (1 / f); }

	void normalize()
	{
		real_t m = 1 / mod(); 
		assert(std::isnormal(m));
		x *= m; y *= m; z *= m;
		m = mod();
		if (fabs(m - 1) > EPS)
		{
			m = 1 / mod();
			x *= m; y *= m; z *= m;
		}
	}

	Vector get_normalized() const
	{ Vector ret(*this); ret.normalize(); return ret; }

	bool is_zero() const
	{ return fabs(x) < EPS && fabs(y) < EPS && fabs(z) < EPS; }

	bool operator == (const Vector &v) const
	{ return fabs(x - v.x) < EPS && fabs(y - v.y) < EPS && fabs(z - v.z) < EPS; }
};
typedef Vector Point;

static inline real_t get_dist(const Point &p0, const Point &p1)
{ return Vector(p0, p1).mod(); }
static inline real_t get_dist_sqr(const Point &p0, const Point &p1)
{ return Vector(p0, p1).mod_sqr(); }

struct Body
{
	Point center;
	Vector velocity;
	real_t radius,
		   mass; ///< Note: mass is the original mass times gravitional constant
	int num;

	/*!
	 * \brief return the time for first collision with target body, or -1 if no
	 *		colission will happenn
	 */
	real_t get_collision_time(const Body &body) const;

	/*!
	 * \brief collide with target body and update the velocity
	 */
	bool collide(Body &body);

	/*!
	 * \param eps the larger the value, the stricter the checking
	 */
	bool chk_overlap(const Body &b, real_t eps = -EPS) const
	{ return get_dist_sqr(center, b.center) <= sqr(radius + b.radius) + eps; }
};
///< buffer for Body pointer
typedef ArrayWrapper<Body*> bodypbuf_t;

#endif // _HEADER_GEOBASE_

// vim: syntax=cpp11.doxygen

