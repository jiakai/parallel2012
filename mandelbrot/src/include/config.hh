/*
 * $File: config.hh
 * $Date: Fri Aug 24 23:37:03 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_CONFIG_
#define _HEADER_CONFIG_

typedef long double real_t;
#define REAL_T_FMT	"%.3Le"
#define REAL_T_MPI	MPI_LONG_DOUBLE

const int X11_DRAW_REFERESH_INTERVAL = 100000;	//!< in microseconds

#define COLORMAP_FILEPATH	"res/colormap.txt"
#define DOMAIN_FILEPATH		"res/range.txt"

/*!
 * \brief configuration variables
 */
namespace conf
{
	extern real_t domain_xmin, domain_ymin, domain_width;
	extern int
		res_x,		//!< X resolution
		res_y,		//!< Y resolution
		nthread,	//!< number of threads
		niter_max;	//!< maximal number of iterations

	void save_domain();
	void load_domain();
}


#endif // _HEADER_CONFIG_

// vim: syntax=cpp11.doxygen

