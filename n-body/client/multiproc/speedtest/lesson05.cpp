/*
 *  simple.c
 *  This program draws a white rectangle on a black background.
 */


/* E. Angel, Interactive Computer Graphics */

/* A Top-Down Approach with OpenGL, Third Edition */

/* Addison-Wesley Longman, 2003 */



#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>         /* glut.h includes gl.h and glu.h*/
#endif
#include <sys/time.h>

#include <stdio.h>
#include <stdlib.h>
static struct timeval prev;
static int cnt = 0;

double rrnd(double min = 0, double max = 1)
{
	return rand() / (RAND_MAX + 1.0) * (max - min) + min;
}

void display(void)

{;
	struct timeval cur;
	gettimeofday(&cur, NULL);
	if (cur.tv_sec - prev.tv_sec >= 1)
	{
		printf("%.3lf\n", cnt / (cur.tv_sec - prev.tv_sec + (cur.tv_usec - prev.tv_usec) * 1e-6));
		cnt = 0;
		prev = cur;
	}
	cnt ++;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(0, 0, 100, 0, 0, 0, 0, 1, 0);

	glBegin(GL_TRIANGLES);
	for (int i = 0; i < 8000; i ++)
	{
		glColor3f(rrnd(), rrnd(), rrnd());
		for (int j = 0; j < 12; j ++)
		{
			glVertex3f(rrnd(-50, 50), rrnd(-50, 50), rrnd(-100, 0));
			glVertex3f(rrnd(-50, 50), rrnd(-50, 50), rrnd(-100, 0));
			glVertex3f(rrnd(-50, 50), rrnd(-50, 50), rrnd(-100, 0));
		}
		// glutSolidSphere(1, 10, 10);
	}
	glEnd();
    glutSwapBuffers();
}


void init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0)	;
    glClearDepth(1.0)					;
    glDepthFunc(GL_LESS)				;
    glEnable(GL_DEPTH_TEST)				;
    glShadeModel(GL_SMOOTH)				;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity()					;
    gluPerspective(45.0, 1, 0.1, 10000);
    glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv)
{

	/* Initialize mode and open a window in upper left corner of screen */
	/* Window title is name of program (arg[0]) */

	/* You must call glutInit before any other OpenGL/GLUT calls */
	gettimeofday(&prev, NULL);
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(0,0); 
	glutCreateWindow("simple"); 
	glutDisplayFunc(display);
    glutIdleFunc(display);
	init();
	glutMainLoop();

}
