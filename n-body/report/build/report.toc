\contentsline {section}{\numberline {1}Instruction}{2}{section.1}
\contentsline {section}{\numberline {2}Design Overview}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Worker: pthread and pthread-prefork}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Worker: MPI}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Experiment Results and Analysis}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Parallel Algorithm Efficiency}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}CPU Time for Different Phases}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Algorithm Complexity}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}MPI: CPU Time and Chunk Size}{4}{subsection.3.4}
\contentsline {section}{\numberline {4}Experience}{7}{section.4}
\contentsline {section}{\numberline {5}Appendix}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}List of Figures and Tables}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Source List}{7}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}/}{7}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}include/}{13}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}server/}{26}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}worker/}{29}{subsubsection.5.2.4}
\contentsline {subsubsection}{\numberline {5.2.5}sptree/}{41}{subsubsection.5.2.5}
