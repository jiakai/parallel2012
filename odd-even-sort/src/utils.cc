/*
 * $File: utils.cc
 * $Date: Mon Aug 13 11:05:39 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"
#include "config.hh"

#include <mpi.h>

#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cstdarg>

void __log_printf__(const char *fname, const char *func, int line,
		const char *fmt, ...)
{
	static FILE *fout = NULL;
	if (!fout)
	{
		char fpath[20];
		int rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		sprintf(fpath, "/tmp/oesort-log-%d", rank);
		fout = fopen(fpath, "w");
		assert(fout);
	}

	char msg[1024];
	FILE *ftmp = fmemopen(msg, sizeof(msg), "w");

	time_t cur_time;
	time(&cur_time);
	char timestr[64];
	strftime(timestr, sizeof(timestr), LOG_TIMEFMT, localtime(&cur_time));
	fprintf(ftmp, "[%s] [%s@%s:%d] ", timestr,
			func, basename(strdupa(fname)), line);

	va_list ap;
	va_start(ap, fmt);
	vfprintf(ftmp, fmt, ap);
	va_end(ap);

	fputc('\n', ftmp);

	fclose(ftmp);

	fputs(msg, stdout);
	fputs(msg, fout);
	fflush(stdout);
	fflush(fout);
}

void error_exit(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

HWTimer::HWTimer()
{
	gettimeofday(&m_start, NULL);
}

double HWTimer::get_sec() const
{
	timeval tv;
	gettimeofday(&tv, NULL);
	double ret = tv.tv_sec - m_start.tv_sec +
		1e-6 * (tv.tv_usec - m_start.tv_usec);
	if (ret < 0)
		ret += 24 * 3600;
	return ret;
}

// vim: syntax=cpp11.doxygen

