/*
 * $File: frogriver.hh
 * $Date: Sat Aug 18 12:09:11 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_FROGRIVER_
#define _HEADER_FROGRIVER_

#include <gtkmm/window.h>
#include <gtkmm/image.h>
#include <gtkmm/fixed.h>

#include <pthread.h>

#include "movable.hh"

class FrogRiverWindow: public Gtk::Window
{
	public:
		FrogRiverWindow();
		virtual ~FrogRiverWindow();

	private:
		int m_nvehicle;
		double m_frog_velocity, m_frog_accel;
		Gtk::Fixed m_fixed_container;
		Gtk::Image m_frog_img, *m_vehicle_imgs;
		ThreadData *m_frog_pos;

		HWTimer m_timer;

		struct ThreadInfo
		{
			ThreadData *data;
			pthread_t pid;
		};
		ThreadInfo* m_vehicle_threads;

		void add_vehicles();
		bool on_timeout();
		bool on_key_press_event(GdkEventKey *event);
		bool on_key_release_event(GdkEventKey *event);

		void try_move_hori(int delta);
		void try_move_vert(int delta);
};

#endif // _HEADER_FROGRIVER_

// vim: syntax=cpp11.doxygen

