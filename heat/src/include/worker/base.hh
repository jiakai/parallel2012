/*
 * $File: base.hh
 * $Date: Mon Sep 03 01:00:05 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_BASE_
#define _HEADER_WORKER_BASE_

#include "config.hh"
#include "boundary/base.hh"

class WorkerBase
{
	public:
		WorkerBase(int w, int h, real_t t0);
		virtual real_t** next_step(BoundaryBase &bd_cond) = 0;
		virtual ~WorkerBase();
	protected:
		real_t **m_buf[2];
		int m_cur_buf, m_width, m_height;

		struct update_arg_t
		{
			real_t **src, **dest;
			int x0, y0, width, height;

			update_arg_t(real_t **s, real_t **d,
					int x0_, int y0_, int w, int h):
				src(s), dest(d), x0(x0_), y0(y0_), width(w), height(h)
			{
			}
			
			update_arg_t(): src(0), dest(0) {}
		};

		static void update(const update_arg_t &arg);
		void divide_task(update_arg_t *range, int n) const;

	private:
		real_t *m_buf_realpos[2];
		int m_buf_size;
};

#endif // _HEADER_WORKER_BASE_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

