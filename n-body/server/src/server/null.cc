/*
 * $File: null.cc
 * $Date: Wed Aug 29 19:50:10 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "server/null.hh"
#include "utils.hh"
#include "config.hh"

#include <cstdio>

void NullServer::do_run(WorkerBase &worker)
{
	double cputime = get_cputime();
	HWTimer timer;
	for (int i = 0; i < m_niter; i ++)
		worker.next_step();
	cputime = get_cputime() - cputime;
	real_t realtime = timer.get_sec();
	printf("tot_cputime=%.3lf tot_realtime=%.3lf fps=%.3lf simu_time=%.3lf\n",
			cputime, realtime, m_niter / realtime, m_niter * conf::time_delta);
}

// vim: syntax=cpp11.doxygen

