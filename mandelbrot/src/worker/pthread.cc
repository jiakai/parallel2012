/*
 * $File: pthread.cc
 * $Date: Fri Aug 24 17:18:45 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker.hh"
#include "utils.hh"

struct WorkerArg
{
	int pool_size, nfinished;
	MutexLock lock_pool;
	Condition cond;
	ImgoutBase *imgout;

	WorkerArg(ImgoutBase *imo):
		pool_size(conf::res_x), nfinished(0), imgout(imo)
	{}
};

static void* worker(void *arg);

void worker_pthread(ImgoutBase *imgout)
{
	if (!imgout)
		return;
	WorkerArg arg(imgout);
	for (int i = 0; i < conf::nthread; i ++)
	{
		pthread_t p;
		PTHREAD_CHKERR_CALL(pthread_create, &p, NULL, worker, &arg);
	}

	LOCK(arg.cond);
	for (; ;)
	{
		arg.cond.wait();
		if (arg.nfinished == conf::nthread)
			return;
	}
}

void* worker(void *arg_)
{
	WorkerArg *arg = static_cast<WorkerArg*>(arg_);
	ImgoutBase::Point *buf = new ImgoutBase::Point[conf::res_y];
	WORKER_INIT;
	for (; ;)
	{
		int cur;
		{
			LOCK(arg->lock_pool);
			if (!arg->pool_size)
				break;
			cur = -- arg->pool_size;
		}
		real_t x = WORKER_COORD2X(cur);
		for (int i = 0; i < conf::res_y; i ++)
			buf[i].set(cur, i, get_niter(x, WORKER_COORD2Y(i)));
		arg->imgout->draw(buf, conf::res_y);
	}
	delete []buf;
	{
		LOCK(arg->cond);
		arg->nfinished ++;
	}
	arg->cond.signal();
	return NULL;
}

// vim: syntax=cpp11.doxygen

