/*
 * $File: mpi.hh
 * $Date: Mon Sep 03 00:15:12 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_MPI_
#define _HEADER_WORKER_MPI_

#include "worker/base.hh"

class MPIWorker: public WorkerBase
{
	int m_rank, m_nworker;
	update_arg_t *m_worker_range, *m_self_range;
	BoundaryBase *m_bd_cond;
	real_t *m_send_buf, *m_recv_buf;
	int *m_displs, *m_recv_cnt;

	void ns_master();
	void ns_slave();
	void init_master();
	void init_slave();
	void finalize_master();
	void finalize_slave();
	void calc_and_sync();
	void sync_left(); // sync with lower rank worker
	void sync_right(); // sync with higher rank worker

	public:
		MPIWorker(int &argc, char **&argv, int w, int h, real_t t0);
		real_t **next_step(BoundaryBase &bd_cond);
		~MPIWorker();

		bool is_master()
		{ return !m_rank; }
};

#endif // _HEADER_WORKER_MPI_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

