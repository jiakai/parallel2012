# -*- coding: utf-8 -*-
# $File: effi_size.py
# $Date: Fri Aug 31 01:00:58 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency vs niter"""


from nbdplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers, nthread = 12, threadname = 'thread'):
    baseline = filter(default_filter(worker = 'seq', size = None), data)
    baseline = {i.size: i for i in baseline}
    ax.set_ylabel('Efficiency')
    ax.set_xlabel("Number of Bodies")
    ax.set_xscale("log")
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk, nthread = nthread, size = None),
                data)
        d.sort(cmp = lambda x, y: cmp(x.size, y.size))
        ax.plot([i.size for i in d], [baseline[i.size].tot_real / (i.tot_real *
            i.nthread_real) for i in d], next(cyc), label = i.worker)
    ax.set_title('Efficiency vs # Bodies({0} {2}, {1} iters)'.format(nthread,
        default_filter.niter, threadname))
    ax.legend()

def plot(data):
    plot_efficiency(data, plt.figure().add_subplot(111), ['pthread',
        'pthreadpf', 'omp'])
    plt.savefig('effi-size-other.pdf')
    plot_efficiency(data, plt.figure().add_subplot(111), ['mpi'], 72,
            'processes')
    plt.savefig('effi-size-mpi.pdf')


