/*
 * $File: omp.hh
 * $Date: Fri Aug 31 10:30:00 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_OMP_
#define _HEADER_WORKER_OMP_

#include "worker/base.hh"
#include "utils.hh"

class OMPWorker: public WorkerBase
{
	MutexLock m_lock_collog;

	void log_collision(Body *body);

	public:
		OMPWorker(int nthread, Body *body, int nbody, SPTreeFactoryBase &tree_factory);

		const char* get_name()
		{ return "OpenMP"; }

		void next_step();
};

#endif // _HEADER_WORKER_PTHREAD_

// vim: syntax=cpp11.doxygen

