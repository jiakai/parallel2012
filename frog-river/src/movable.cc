/*
 * $File: movable.cc
 * $Date: Sat Aug 18 11:54:08 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "movable.hh"
#include "config.hh"

#include <unistd.h>

void* thread_mover(ThreadData *data)
{
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	int &v = data->velocity;
	while(1)
	{
		usleep(REFRESH_INTERVAL);
		int lb = -1, rb = WINDOW_WIDTH; // left and right boundary
		if (data->adj_left)
		{
			Movable *p = data->adj_left->vehicle;
			LOCK(p->lock);
			lb = p->x0 + p->width;
		}
		if (data->adj_right)
		{
			Movable *p = data->adj_right->vehicle;
			LOCK(p->lock);
			rb = p->x0;
		}

		{
			Movable *cur = data->vehicle;
			LOCK(cur->lock);
			int p = (cur->x0 += v);
			if (p <= lb || p + cur->width >= rb)
			{
				v = -v;
				cur->x0 += v * 2;
			}
		}
		{
			LOCK(data->lock_load);
			if (data->load)
			{
				LOCK(data->load->lock);
				data->load->x0 += v;
			}
		}
	}
	return NULL;
}

// vim: syntax=cpp11.doxygen

