/*
 * $File: main.cc
 * $Date: Mon Aug 13 08:27:24 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sort.hh"
#include "utils.hh"

#include <mpi.h>

#include <cstdlib>
#include <cassert>

#include <sstream>
#include <algorithm>

#ifdef GPERF
#include <cstdio>
#include <google/profiler.h>
#endif

class CallBack: public CallBackBase
{
	bool m_use_qsort;
	FILE *m_fin, *m_fout;
	size_t m_size, m_read_cnt;

	static void bubble_sort(int *begin, int *end)
	{
		for (int *i = begin; i != end; i ++)
		{
			int *p = i;
			for (int *j = i; j != end; j ++)
				if (*j < *p)
					p = j;
			if (p != i)
				std::swap(*i, *p);
		}
	}

	public:

		/*!
		 * If fin == NULL, size should be supplied and random data are used
		 */
		CallBack(bool qsort, FILE *fin, FILE *fout, size_t size = 0):
			m_use_qsort(qsort), m_fin(fin), m_fout(fout), m_size(size),
			m_read_cnt(0)
		{
			if (fin)
			{
				int s;
				fread(&s, sizeof(int), 1, fin);
				m_size = s;
			}
		}

		void sort(int *begin, int *end)
		{
			if (m_use_qsort)
				std::sort(begin, end);
			else
				bubble_sort(begin, end);
		}

		size_t get_data_size()
		{ return m_size; }

		size_t read_data(int *dest, size_t len)
		{
			if (m_fin)
				return fread(dest, sizeof(int), len, m_fin);
			size_t i = 0;
			while (i < len && m_read_cnt < m_size)
			{
				dest[i ++] = rand();
				m_read_cnt ++;
			}
			return i;
		}

		void write_data(const int *src, size_t size)
		{
			if (m_fout)
				fwrite(src, sizeof(int), size, m_fout);
		}
};

int main(int argc, char **argv)
{
	int rank, comm_size;
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

#ifdef GPERF
	char fpath[255];
	sprintf(fpath, "/tmp/oesort-gperf-%d", rank);
	ProfilerStart(fpath);
#endif

	HWTimer timer;
	size_t size = 0;
	FILE *fin = NULL, *fout = NULL;
	if (!rank)
	{
		if (argc == 2)
		{
			std::istringstream sin(argv[1]);
			sin >> size;
		} else if (argc == 3)
		{
			fin = fopen(argv[1], "rb");
			fout = fopen(argv[2], "wb");
			assert(fin && fout);
		} else
		{
			fprintf(stderr, "usage:\n"
					"  %s <size for random data>\n"
					"  %s <input filename> <output filename>\n"
					"environment vairable:\n"
					"  QSORT -- if set, use std::sort instead of bubble sort in each node\n",
					argv[0], argv[0]);
			return -1;
		}
	}

	CallBack cb(getenv("QSORT") != NULL, fin, fout, size);
	sort(rank, comm_size, cb);

	if (!rank)
		log_printf("time elapsed: %.2lfsec", timer.get_sec());

#ifdef GPERF
	ProfilerStop();
#endif

	MPI_Finalize();
}

// vim: syntax=cpp11.doxygen

