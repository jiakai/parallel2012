/*
 * $File: config.hh
 * $Date: Wed Aug 29 10:01:00 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_CONFIG_
#define _HEADER_CONFIG_

#include "geobase.hh"

namespace conf
{
	extern real_t
		opening_agl_threshold,
		time_delta,
		range[3][2];	// xmin, xmax, ymin, ymax, zmin, zmax
	extern int
		mpi_chunk_size;
}

#endif // _HEADER_CONFIG_

// vim: syntax=cpp11.doxygen

