#!/usr/bin/env gnuplot
# $File: comm-fixnproc.plt
# $Date: Wed Aug 15 21:58:24 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale x
set title "Relative Communication Cost Improvement (nproc=60)"
set xlabel "Data Size"
set ylabel "Decrease/Original Value"

set xrange [900:1e10]
set yrange [0:1]

set terminal postscript enhanced color
set output "comm-fixnproc.eps"

set style data linespoints

cd "../../data/optimized/qsort-ext"
plot "nproc60" using 1:(1-$6/$5) notitle
