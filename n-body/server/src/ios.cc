/*
 * $File: ios.cc
 * $Date: Sun Aug 26 19:23:52 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "ios.hh"
using namespace std;

istream& operator >> (std::istream& instr, Vector &v)
{
	return instr >> v.x >> v.y >> v.z;
}

ostream& operator << (std::ostream& outstr, const Vector &v)
{
	return outstr << '{' << v.x << ", " << v.y << ", " << v.z << '}';
}

// vim: syntax=cpp11.doxygen

