/*
 * $File: seq.cc
 * $Date: Fri Aug 24 10:28:28 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker.hh"

void worker_seq(ImgoutBase *imgout)
{
	if (!imgout)
		return;
	WORKER_INIT;
	for (int i = 0; i < conf::res_x; i ++)
	{
		real_t x = WORKER_COORD2X(i);
		for (int j = 0; j < conf::res_y; j ++)
			imgout->draw(i, j,
					get_niter(x, WORKER_COORD2Y(j)));
	}
}

// vim: syntax=cpp11.doxygen

