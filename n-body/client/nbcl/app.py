# -*- coding: utf-8 -*-
# $File: app.py
# $Date: Mon Aug 27 20:40:13 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import threading
import sys
from Queue import Queue
from time import sleep, time

class Frame(object):
    bdlist = None
    """list<:class:`Body`>"""

    poslist = None
    """list<double * (nbody * )>, positions"""

    def __init__(self, bdlist, poslist):
        self.bdlist = bdlist
        self.poslist = poslist


def run_app(cli, draw_cb, qsize = 30):
    """
    :param cli: a :class:`nbcl.client.Client` instance
    :param draw_cb: call back drawing function, taking a :class:`Frame`
    object and the time as float, return the whether the drawing loop should
    continue"""
    queue = Queue(qsize)
    stop_flag = [False]

    def draw_thread():
        prev = time()
        frcnt = 0
        tot_time = 0
        while True:
            cur = time()
            tot_time += cli.time_delta
            if cur - prev > 1:
                print 'network fps=' + str(frcnt / (cur - prev))
                prev = cur
                frcnt = 0
            frcnt += 1
            if not draw_cb(Frame(cli.body, queue.get()), tot_time):
                stop_flag[0] = True
                return
            sleep(cli.time_delta)

    threading.Thread(target = draw_thread).start()

    frnum = 0
    while True:
        if stop_flag[0]:
            cli.send_int32(-1)
            cli.close()
            sys.exit()
        cli.send_int32(frnum)
        frnum += 1
        queue.put(cli.recv_double(len(cli.body) * 3))


