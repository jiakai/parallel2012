# -*- coding: utf-8 -*-
# $File: efficiency.py
# $Date: Fri Aug 31 09:13:47 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency"""


from nbdplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers, xlabel = '# Threads'):
    baseline = filter(default_filter(worker = 'seq'), data)
    assert len(baseline) == 1
    baseline = baseline[0]
    ax.set_ylabel('Efficiency')
    ax.set_xlabel(xlabel)
    cyc = get_line_cycler()
    for wk in workers:
        if wk == 'mpi':
            ft = default_filter(worker = wk, nthread = None, chunksize = 30)
        else:
            ft = default_filter(worker = wk, nthread = None)
        d = filter(ft, data)
        d.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
        ax.plot([i.nthread_real for i in d], [baseline.tot_real / (i.tot_real *
            i.nthread_real) for i in d], next(cyc), label = i.worker)
    ax.legend()

def plot_realtime(data, ax, workers, xlabel = '# Threads'):
    ax.set_ylabel('Real Time [sec]')
    ax.set_xlabel(xlabel)
    cyc = get_line_cycler()
    for wk in workers:
        if wk == 'mpi':
            ft = default_filter(worker = wk, nthread = None, chunksize = 30)
        else:
            ft = default_filter(worker = wk, nthread = None)
        d = filter(ft, data)
        d.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
        ax.plot([i.nthread_real for i in d], [i.tot_real for i in d],
                next(cyc), label = i.worker)
    ax.legend()

def plot(data):
    fig = plt.figure()
    fig.suptitle('Efficiency and Real Time Comparison({0} bodies,{1} iterations)'.
            format(default_filter.size, default_filter.niter))
    plot_efficiency(data, fig.add_subplot(221), ['pthread', 'pthreadpf', 'omp'])
    plot_efficiency(data, fig.add_subplot(222), ['mpi'], '# Processes')
    plot_realtime(data, fig.add_subplot(223), ['pthread', 'pthreadpf', 'omp'])
    plot_realtime(data, fig.add_subplot(224), ['mpi'], '# Processes')
    plt.savefig('effi.pdf')

