/*
 * $File: utils.hh
 * $Date: Sat Aug 18 12:07:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_UTILS_
#define _HEADER_UTILS_

#include <pthread.h>

class MutexLock
{
	pthread_mutex_t m_lock;

	public:

		MutexLock();
		~MutexLock();
		void acquire();
		void release();
};

/*!
 * \brief print error information and exit the process
 */
void error_exit(const char *fmt, ...) __attribute__((format(printf, 1, 2),
			noreturn));

#define ITER_STD(vec, var) \
	for (typeof(vec.begin()) var = vec.begin(); var != vec.end(); var ++)


class ScopedLock
{
	MutexLock &m_lock;

	public:

		ScopedLock(MutexLock &lock):
			m_lock(lock)
		{
			m_lock.acquire();
		}

		~ScopedLock()
		{
			m_lock.release();
		}
};

#define __lock_name(x) __lock_ ## x 
#define _lock_name(x) __lock_name(x)
#define LOCK(locker) ScopedLock _lock_name(__LINE__)(locker)


/*!
 * \brief a timer using the hardware clock to measure real time
 */
class HWTimer
{
	timeval m_start;

	public:
		
		HWTimer();
		void reset();
		double get_sec() const;
};

#endif // _HEADER_UTILS_

// vim: syntax=cpp11.doxygen

