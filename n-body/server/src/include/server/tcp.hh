/*
 * $File: tcp.hh
 * $Date: Wed Aug 29 19:49:48 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SERVER_TCP_
#define _HEADER_SERVER_TCP_

#include "server/base.hh"
#include "utils.hh"

class TCPServer: public ServerBase
{
	public:
		/*!
		 * \param buf_size number of frames to be cached
		 */
		TCPServer(int port, int buf_size);
		~TCPServer();

	protected:
		void do_run(WorkerBase &worker);

	private:
		struct DVector;
		bool m_stop_flag;
		DVector **m_buf;
		int m_port,
			m_buf_head, m_buf_tail, m_buf_size, ///< current buffer size
			m_buf_max_size;
		Condition m_cond_buf;

		WorkerBase *m_worker;

		/*!
		 * \brief wait for client connection and return socket id
		 */
		int wait_client();

		static void* thread_calc_wrapper(void*);
		void thread_calc();
};

#endif // _HEADER_SERVER_TCP_

// vim: syntax=cpp11.doxygen

