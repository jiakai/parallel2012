/*
 * $File: base.cc
 * $Date: Mon Sep 03 00:27:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "disp/base.hh"
#include "utils.hh"
#include "worker/mpi.hh"

#include <cstdio>

DispBase::Pixel* DispBase::load_colormap(int &ncolor)
{
	FILE *fin = fopen(COLORMAP_FILEPATH, "r");
	if (!fin)
		error_exit("failed to open colormap %s: %m", COLORMAP_FILEPATH);
	fscanf(fin, "%d", &ncolor);
	Pixel *ret = new Pixel[ncolor];
	for (int i = 0; i < ncolor; i ++)
		fscanf(fin, "%d%d%d", &ret[i].r, &ret[i].g, &ret[i].b);
	return ret;
}

void DispBase::run(WorkerBase &worker, BoundaryBase &bd_cond)
{
	MPIWorker *mpi = dynamic_cast<MPIWorker*>(&worker);
	if (mpi && !mpi->is_master())
		mpi->next_step(bd_cond);
	else
		do_run(worker, bd_cond);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

