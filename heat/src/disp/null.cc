/*
 * $File: null.cc
 * $Date: Tue Sep 04 07:29:25 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "disp/null.hh"
#include "utils.hh"
#include <cstdio>

void NullDisp::do_run(WorkerBase &worker, BoundaryBase &bd_cond)
{
	printf("nulldisp: niter=%d\n", m_niter);
	HWTimer timer;
	double start_cpu = get_cputime(), prev_prog = 0;
	for (int i = 0; i < m_niter; i ++)
	{
		double prog = i * 100.0 / m_niter;
		if (prog - prev_prog > 1)
		{
			fprintf(stderr, "nulldisp: %.3lf%% done\r", prog);
			fflush(stderr);
			prog = prev_prog;
		}
		worker.next_step(bd_cond);
	}
	fprintf(stderr, "\n");
	printf("cputime=%.3lf realtime=%.3lf fps=%.3lf\n",
			get_cputime() - start_cpu, timer.get_sec(),
			m_niter / timer.get_sec());
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

