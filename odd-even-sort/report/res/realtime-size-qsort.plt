#!/usr/bin/env gnuplot
# $File: realtime-size-qsort.plt
# $Date: Tue Aug 14 11:37:19 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale 
set title "Quick Sort" 
set xlabel "Data Size"
set ylabel "Real Run Time [sec]"

set xrange [5e5:5e11]

set terminal postscript enhanced color
set output "realtime-size-qsort.eps"

set style data linespoints

cd "../data/qsort-ext"
plot \
	"nproc12" using 1:4 title "nproc=12" , \
	"nproc24" using 1:4 title "nproc=24" , \
	"nproc48" using 1:4 title "nproc=48" , \
	"nproc96" using 1:4 title "nproc=96" , \
	"nproc120" using 1:4 title "nproc=120"
