/*
 * $File: worker.hh
 * $Date: Sat Aug 25 10:18:35 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_
#define _HEADER_WORKER_

#include "imgout/base.hh"
#include "config.hh"

#define WORKER_INIT \
	real_t \
		scale = conf::domain_width / conf::res_x, \
		trans_x = conf::domain_xmin, \
		trans_y = conf::domain_ymin

#define WORKER_COORD2X(x) (((x) + 0.5) * scale + trans_x)
#define WORKER_COORD2Y(y) (((y) + 0.5) * scale + trans_y)

// imgout = NULL for finalize
void worker_seq(ImgoutBase *imgout);
void worker_omp(ImgoutBase *imgout);
void worker_omp_static(ImgoutBase *imgout);
void worker_pthread(ImgoutBase *imgout);

// imgout = NULL in slave processes
void worker_mpi(ImgoutBase *imgout);

static inline int get_niter(real_t re0, real_t im0)
{
	real_t register
		re = re0, im = im0,
		resq = re * re, imsq = im * im;
	int register cur = conf::niter_max;
	while (resq + imsq < 4 && (-- cur))
	{
		im *= re;
		im *= 2;
		im += im0;
		re = resq - imsq + re0;
		resq = re * re;
		imsq = im * im;
	}
	return conf::niter_max - cur;
}

#endif // _HEADER_WORKER_
// vim: syntax=cpp11.doxygen

