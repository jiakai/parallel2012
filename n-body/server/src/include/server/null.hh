/*
 * $File: null.hh
 * $Date: Wed Aug 29 19:49:42 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SERVER_NULL_
#define _HEADER_SERVER_NULL_

#include "server/base.hh"

class NullServer: public ServerBase
{
	int m_niter;
	void do_run(WorkerBase &worker);

	public:
		NullServer(int niter):
			m_niter(niter)
		{}
};

#endif // _HEADER_SERVER_NULL_

// vim: syntax=cpp11.doxygen

