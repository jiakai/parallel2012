/*
 * $File: config.hh
 * $Date: Mon Sep 03 00:11:07 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_CONFIG_
#define _HEADER_CONFIG_

typedef float real_t;
#define REAL_T_MPI	MPI_FLOAT
#define COLORMAP_FILEPATH	"res/colormap.txt"

#endif // _HEADER_CONFIG_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

