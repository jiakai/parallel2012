/*
 * $File: utils.cc
 * $Date: Sat Aug 18 12:08:57 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"

#include <sys/time.h>

#include <cstdlib>
#include <cstdarg>
#include <cstdio>

MutexLock::MutexLock()
{
	if (pthread_mutex_init(&m_lock, NULL))
		error_exit("pthread_mutex_init");
}

MutexLock::~MutexLock()
{
	pthread_mutex_destroy(&m_lock);
}

void MutexLock::acquire()
{
	if (pthread_mutex_lock(&m_lock))
		error_exit("pthread_mutex_lock");
}

void MutexLock::release()
{
	if (pthread_mutex_unlock(&m_lock))
		error_exit("pthread_mutex_unlock");
}

void error_exit(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

HWTimer::HWTimer()
{
	reset();
}

void HWTimer::reset()
{
	gettimeofday(&m_start, NULL);
}

double HWTimer::get_sec() const
{
	timeval tv;
	gettimeofday(&tv, NULL);
	double ret = tv.tv_sec - m_start.tv_sec +
		1e-6 * (tv.tv_usec - m_start.tv_usec);
	if (ret < 0)
		ret += 24 * 3600;
	return ret;
}

// vim: syntax=cpp11.doxygen

