/*
 * $File: mpi.cc
 * $Date: Fri Aug 24 10:43:57 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker.hh"
#include "utils.hh"

#include <mpi.h>

#include <list>

namespace tag
{
	enum _tag_t {
		NEW_TASK_STATUS = 23,
			/*
			 * master -> slave
			 * int[1] {<whether there are new tasks>}
			 */
		NEW_TASK_ARGS,
			/*
			 * master -> slave
			 * real_t[3] {domain_xmin, domain_ymin, domain_width}
			 */
		REQUEST_TASK,
			/*
			 * slave -> master
			 * int[1] {<slave number, starting from 1>}
			 */
		REPLY_TASK,
			/*
			 * master -> slave
			 * int[1] {<x coordinate, -1 if no more task>}
			 */
		RESULT
			/*
			 * slave -> master
			 * int[3 * res_y] {{<x>, <y>, <niter>} * res_y}
			 */
	};
}

static void master(ImgoutBase *imgout, int nslave);
static void slave(int slave_num);
static inline void recv(void *buf, int len, int from, tag::_tag_t tag,
		const MPI_Datatype &datatype = MPI_INTEGER)
{
	MPI_Recv(buf, len, datatype, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

static inline int recv_int(int from, tag::_tag_t tag)
{
	int n;
	MPI_Recv(&n, 1, MPI_INTEGER, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	return n;
}

static inline void send(const void *buf, int len, int to, tag::_tag_t tag,
		const MPI_Datatype &datatype = MPI_INTEGER)
{
	MPI_Send((void*)(buf), len, datatype, to, tag, MPI_COMM_WORLD);
}

static inline void send_int(int val, int to, tag::_tag_t tag)
{
	MPI_Send(&val, 1, MPI_INTEGER, to, tag, MPI_COMM_WORLD);
}

void worker_mpi(ImgoutBase *imgout)
{
	int rank, comm_size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
	if (comm_size == 1)
		error_exit("at least two processes are needed\n");
	if (!rank)
		master(imgout, comm_size - 1);
	else
		slave(rank);
}

void master(ImgoutBase *imgout, int nslave)
{
	for (int i = 1; i <= nslave; i ++)
		send_int(imgout != NULL, i, tag::NEW_TASK_STATUS);
	if (!imgout)
		return;
	{
		real_t buf[] = {conf::domain_xmin, conf::domain_ymin, conf::domain_width};
		for (int i = 1; i <= nslave; i ++)
			send(buf, 3, i, tag::NEW_TASK_ARGS, REAL_T_MPI);
	}
	int cur_x = 0;
	for (int nfinish = 0; nfinish < nslave; )
	{
		int num = recv_int(MPI_ANY_SOURCE, tag::REQUEST_TASK);
		if (cur_x == conf::res_x)
		{
			send_int(-1, num, tag::REPLY_TASK);
			nfinish ++;
		} else
			send_int(cur_x ++, num, tag::REPLY_TASK);
	}

	int *buf = new int[conf::res_y * 3];
	for (int i = 0; i < conf::res_x; i ++)
	{
		recv(buf, conf::res_y * 3, MPI_ANY_SOURCE, tag::RESULT);
		imgout->draw((ImgoutBase::Point*)buf, conf::res_y);
	}
	delete []buf;
}

void slave(int slave_num)
{
	for (; ;)
	{
		if (!recv_int(0, tag::NEW_TASK_STATUS))
			return;
		{
			real_t buf[3];
			recv(buf, 3, 0, tag::NEW_TASK_ARGS, REAL_T_MPI);
			conf::domain_xmin = buf[0];
			conf::domain_ymin = buf[1];
			conf::domain_width = buf[2];
		}
		WORKER_INIT;
		std::list<ImgoutBase::Point*> buf;
		for (; ;)
		{
			send_int(slave_num, 0, tag::REQUEST_TASK);
			int xcoord = recv_int(0, tag::REPLY_TASK);
			if (xcoord == -1)
				break;
			ImgoutBase::Point *cur = new ImgoutBase::Point[conf::res_y];
			buf.push_back(cur);
			real_t re = WORKER_COORD2X(xcoord);

			for (int i = 0; i < conf::res_y; i ++)
				cur[i].set(xcoord, i, get_niter(re, WORKER_COORD2Y(i)));
		}

		ITER_STD(buf, i)
		{
			send(*i, conf::res_y * 3, 0, tag::RESULT);
			delete *i;
		}
	}
}

// vim: syntax=cpp11.doxygen

