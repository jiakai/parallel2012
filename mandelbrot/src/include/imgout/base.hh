/*
 * $File: base.hh
 * $Date: Fri Aug 24 23:20:26 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IMGOUT_BASE_
#define _HEADER_IMGOUT_BASE_

#include "config.hh"

#include <cstddef>

/*!
 * \brief base class for image output implementations
 */
class ImgoutBase
{
	protected:
		struct Pixel
		{
			int r, g, b;
		};
		/*!
		 * \brief load colormap into a buffer allocated by new[], whose size is
		 *		conf::niter_max + 1
		 */
		static Pixel* load_colormap();
	public:
		struct Point
		{
			int x, y, niter;
			void set(int x_, int y_, int n)
			{ x = x_; y = y_; niter = n; }
		};

		/*!
		 * \brief this function should be called before draw
		 */
		virtual void init() = 0;

		virtual void draw(const Point *buf, size_t size) = 0;

		void draw(int x, int y, int niter)
		{
			Point p;
			p.set(x, y, niter);
			draw(&p, 1);
		}

		/*!
		 * \brief wait for the draw to complete (e.g. a key event in X11)
		 * \return whether requesting redraw 
		 */
		virtual bool wait() = 0;
		virtual ~ImgoutBase() {}
};

#endif // _HEADER_IMGOUT_BASE_

// vim: syntax=cpp11.doxygen

