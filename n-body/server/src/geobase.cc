/*
 * $File: geobase.cc
 * $Date: Tue Aug 28 09:17:29 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "geobase.hh"

static inline bool solve_linear_collide(real_t m0, real_t v0,
		real_t m1, real_t v1, real_t &v0n, real_t &v1n);

real_t Body::get_collision_time(const Body &body) const
{
	if (num == body.num)
		return -1;
	Vector dv(velocity, body.velocity),
		   dp(center, body.center);
	real_t a = dv.mod_sqr(),
		   b = 2 * dv.dot(dp),
		   c = dp.mod_sqr() - sqr(radius + body.radius),
		   delta = b * b - a * c * 4;
	if (dv.is_zero())
		return fabs(c) < EPS ? 0 : -1;
	if (delta < 0)
		return -1;
	delta = sqrt(delta);
	real_t x0 = (-b - delta) / (a * 2),
		   x1 = (-b + delta) / (a * 2);
	if (x0 < -EPS)
		return x1;
	return x0;
}

bool Body::collide(Body &body)
{
	Vector vct(center, body.center);
	real_t m = vct.mod_sqr();
	if (fabs(m - sqr(radius + body.radius)) > EPS_COLLIDE)
		return false;
	vct *= 1 / sqrt(m);
	real_t v0 = velocity.dot(vct),
		   v1 = body.velocity.dot(vct),
		   v0n, v1n;
	if (!solve_linear_collide(mass, v0, body.mass, v1, v0n, v1n))
		return false;
	velocity += vct * (v0n - v0);
	body.velocity += vct * (v1n - v1);
	return true;
}

bool solve_linear_collide(real_t m0, real_t v0,
		real_t m1, real_t v1, real_t &v0n, real_t &v1n)
{
	if (v1 > v0)
		return false;
	real_t d = v1 - v0,
		   p = m0 * v0 + m1 * v1;
	v0n = (p + d * m1) / (m0 + m1);
	v1n = v0n - d;
	return true;
}

// vim: syntax=cpp11.doxygen

