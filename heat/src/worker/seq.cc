/*
 * $File: seq.cc
 * $Date: Tue Sep 04 06:58:07 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/seq.hh"

SeqWorker::SeqWorker(int w, int h, real_t t0):
	WorkerBase(w, h, t0)
{
}

real_t** SeqWorker::next_step(BoundaryBase &bd_cond)
{
	real_t **src = m_buf[m_cur_buf],
		   **dest = m_buf[m_cur_buf ^= 1];
	update(update_arg_t(src, dest, 1, 1, m_width - 2, m_height - 2));
	bd_cond.update(dest, m_width, m_height);
	return dest;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

