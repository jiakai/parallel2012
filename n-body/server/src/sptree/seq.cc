/*
 * $File: seq.cc
 * $Date: Tue Aug 28 14:45:07 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sptree/seq.hh"
#include "profile.hh"

class SeqSPTreeFactory::SPTree: public SPTreeBase
{
	protected:
		sptptr_t alloc_node(bodypbuf_t &body, int d)
		{
			return SeqSPTreeFactory::do_mktree(body, d);
		}
};

SeqSPTreeFactory::SPTree* SeqSPTreeFactory::do_mktree(bodypbuf_t &body, int d)
{
	SPTree *ret = new SPTree;
	ret->build(body, d);
	return ret;
}

sptptr_t SeqSPTreeFactory::mktree(bodypbuf_t &body)
{
	profile_start("seq_mktree");
	SPTree *ret = do_mktree(body, 0);
	profile_end("seq_mktree");
	return ret;
}

// vim: syntax=cpp11.doxygen

