#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: extract.py
# $Date: Tue Aug 14 11:55:16 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import sys
import os
import os.path
import re
if len(sys.argv) != 3:
    sys.exit('usage: {} <log directory> <output directory>'.format(sys.argv[0]))

re_size_nproc = re.compile('.*size(.*)\.nproc(.*)')

class LogEntry(object):
    _re_cpu_time = re.compile('.*CPU time.* (.*) sec.*')
    _re_realtime = re.compile('.*time elapsed: (.*)sec.*')
    _re_comm = re.compile('.*: (.*)/(.*) communication payload.*')

    size = None
    nproc = None
    cpu_time = None
    realtime = None

    comm_real = 0
    comm_tot = 0

    def __init__(self, size, nproc, fin):
        self.size = size
        self.nproc = nproc
        for i in fin.readlines():
            m = self._re_cpu_time.match(i)
            if m:
                self.cpu_time = float(m.group(1))
            m = self._re_realtime.match(i)
            if m:
                self.realtime = float(m.group(1))
            m = self._re_comm.match(i)
            if m:
                self.comm_real += int(m.group(1))
                self.comm_tot += int(m.group(2))

    def __str__(self):
        return ' '.join(str(i) for i in (
            self.size, self.nproc, self.cpu_time, self.realtime,
            self.comm_tot, self.comm_real))

    __repr__ = __str__

    @staticmethod
    def get_header():
        return 'size nproc cpu_time realtime orig_comm_size real_comm_size'

by_size = dict()
by_nproc = dict()
pjoin_log = lambda x: os.path.join(sys.argv[1], x)

for errfile in os.listdir(sys.argv[1]):
    errfile = pjoin_log(errfile)
    if 'err.' not in errfile or os.stat(errfile).st_size:
        continue
    logfile = errfile.replace('err.', 'output.')
    [size, nproc] = [int(re_size_nproc.match(logfile).group(i)) for i in
            range(1, 3)]

    with open(logfile) as f:
        entry = LogEntry(size, nproc, f)

    print entry
    by_size.setdefault(size, list()).append(entry)
    by_nproc.setdefault(nproc, list()).append(entry)


pjoin_out = lambda x: os.path.join(sys.argv[2], x)
for (size, cur_list) in by_size.iteritems():
    cur_list.sort(cmp = lambda x, y: cmp(x.nproc, y.nproc))
    with open(pjoin_out('size{}'.format(size)), 'w') as f:
        f.write('# {}\n'.format(LogEntry.get_header()))
        for i in cur_list:
            f.write(str(i) + '\n')

for (nproc, cur_list) in by_nproc.iteritems():
    cur_list.sort(cmp = lambda x, y: cmp(x.size, y.size))
    with open(pjoin_out('nproc{}'.format(nproc)), 'w') as f:
        f.write('# {}\n'.format(LogEntry.get_header()))
        for i in cur_list:
            f.write(str(i) + '\n')
