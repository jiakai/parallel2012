#!/usr/bin/env gnuplot
# $File: rt-imgsize.plt
# $Date: Sat Aug 25 13:53:44 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set terminal postscript enhanced color
set output "rt-imgsize.eps"
set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

set title "Run Time Comparison (pthread,nthread=12,niter=10000)"
set xlabel "Resolution"
set ylabel "Real Time [sec]" 
set y2label "Total CPU Time [sec]" 

set y2tics nomirror tc lt 2

cd "data"
plot \
	"xres" using 4:6 title "real time", \
	"xres" using 4:7 title "total CPU time" axes x1y2, \
	"xres" using 4:($6/$4/$4 + 25) title "realtime/x^2+c", \
	"xres" using 4:($7/$4/$4 + 30) title "cputime/x^2+c"
