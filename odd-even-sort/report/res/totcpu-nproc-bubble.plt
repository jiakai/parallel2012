#!/usr/bin/env gnuplot
# $File: totcpu-nproc-bubble.plt
# $Date: Thu Aug 16 23:39:22 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale y
set title "Bubble Sort" 
set xlabel "# Processes"
set ylabel "Total CPU Time [sec]"
set xtics 12 12
set xrange [10:75]

set terminal postscript enhanced color
set output "totcpu-nproc-bubble.eps"

set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

cd "../data/bubble-ext"
plot \
	"size10000" using 2:3 title "n=1e4" , \
	"size50000" using 2:3 title "n=5e4" , \
	"size100000" using 2:3 title "n=1e5" , \
	"size500000" using 2:3 title "n=5e5" , \
	"size1000000" using 2:3 title "n=1e6" , \
	"size5000000" using 2:3 title "n=5e6" ls 6 


