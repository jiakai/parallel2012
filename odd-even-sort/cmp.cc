/*
 * $File: cmp.cc
 * $Date: Thu Aug 09 21:50:11 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include <cstdio>

int main()
{
	FILE *fout = fopen("sort.out", "rb"),
		 *fans = fopen("sort.ans", "rb");
	int n, m, num = 0;
#define read(fin, x) \
	fread(&x, sizeof(int), 1, fin)
	while (read(fans, m) == 1)
	{
		num ++;
		if (read(fout, n) != 1 || n != m)
			printf("%d n=%d m=%d\n", num, n, m);
	}
	if (read(fout, n) == 1)
		printf("list\n");
}

// vim: syntax=cpp11.doxygen

