/*
 * $File: base.hh
 * $Date: Wed Aug 29 17:17:13 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SERVER_BASE_
#define _HEADER_SERVER_BASE_

#include "worker/base.hh"

class ServerBase
{
	protected:
		virtual void do_run(WorkerBase &worker) = 0;
	public:
		// wraps do_run, do some basic checking
		void run(WorkerBase &worker);

		virtual ~ServerBase() {}
};

#endif // _HEADER_SERVER_BASE_

// vim: syntax=cpp11.doxygen

