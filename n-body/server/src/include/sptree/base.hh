/*
 * $File: base.hh
 * $Date: Mon Aug 27 21:57:13 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SPTREE_BASE_
#define _HEADER_SPTREE_BASE_

#include "geobase.hh"
#include "refcntptr.hh"
#include "utils.hh"

/*!
 * \brief space partition tree
 */
class SPTreeBase;
typedef RefCntPtr<SPTreeBase> sptptr_t;

class SPTreeBase
{
	public:
		/*!
		 * \brief data associated to each tree node
		 */
		struct NodeData
		{
			Body synthesis_body;
			Point bv_center;	// bounding volume
			real_t synthesis_min_dist, bv_radius;
			bodypbuf_t body;
			RefCntPtr<SPTreeBase> chl, chr;
			bool is_leaf() const
			{ return !chl; }
		};

		virtual ~SPTreeBase() {}

		NodeData* get_data()
		{ return &m_data; }

		static const int
			DIVIDE_MIN_NBODY,
			///< if number of bodies is less than this, a node will not be divided
			MAX_DEPTH;
			///< maximal node depth

		/*!
		 * \brief return the body that collides earlist with the supplied body,
		 *		or NULL if no collision seems to happen
		 * \param time collision time
		 */
		Body* get_collision_body(const Body &body, real_t &time);

		void build(bodypbuf_t &body, int depth = 0);

	protected:
		Point m_coord_min;
		Vector m_diam;
		NodeData m_data;

		virtual sptptr_t alloc_node(bodypbuf_t &body, int depth) = 0;

	private:
		/*!
		 * \brief compute m_coord_min and m_dim
		 */
		void init(const bodypbuf_t &body);
};

class SPTreeFactoryBase
{
	public:
		virtual ~SPTreeFactoryBase() {}
		virtual sptptr_t mktree(bodypbuf_t &body) = 0;
};

#endif // _HEADER_SPTREE_BASE_

// vim: syntax=cpp11.doxygen

