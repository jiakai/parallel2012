#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: plot.py
# $Date: Fri Aug 31 09:28:57 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from nbdplt.data import scan_dir
from nbdplt.plot import phasecpu, efficiency, effi_niter, effi_size, \
        complexity, chunksize

import matplotlib.pyplot as plt

fig_width_pt = 1100  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inch
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height = fig_width * 0.64
fig_size =  [fig_width,fig_height]
params = {'axes.labelsize': 10,
          'text.fontsize': 10,
          'legend.fontsize': 10,
          'xtick.labelsize': 10,
          'ytick.labelsize': 10,
          'legend.shadow': True,
          'figure.figsize': fig_size}
plt.rcParams.update(params)

data_cluster = scan_dir('log/cluster')

for i in [phasecpu, efficiency, effi_niter, effi_size, complexity, chunksize]:
    i.plot(data_cluster)
plt.show()
