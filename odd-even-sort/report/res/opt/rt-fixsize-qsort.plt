#!/usr/bin/env gnuplot
# $File: rt-fixsize-qsort.plt
# $Date: Wed Aug 15 23:10:13 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set title "Real Run Time Comparison (Quick Sort, n=1e9)"
set xlabel "# Processes"
set ylabel "Real Run Time [sec]"
set y2label "Decrease/Original" tc lt 3

set xrange[10:130]
set xtics 12 12
set y2range[-0.5:0.5]
set y2tics 0 0.1 nomirror tc lt 3

set terminal postscript enhanced color
set output "rt-fixsize-qsort.eps"

set style data linespoints

cd "../../data"
plot \
	"qsort-ext/size1000000000" using 2:4 title "original", \
	"optimized/qsort-ext/size1000000000" using 2:4 title "optimized", \
	"cmp-qsort-size1000000000" using 1:(1-$9/$4) title "decrease/original" axes x1y2, \
	0 notitle axes x1y2 lt 3
