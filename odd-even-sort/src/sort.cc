/*
 * $File: sort.cc
 * $Date: Mon Aug 13 19:15:42 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sort.hh"
#include "utils.hh"

#include <mpi.h>

#include <limits>
#include <algorithm>

// note that worker id starts from 1

namespace tag
{
	enum _tag_t {
		METADATA = 23,
			/* scheduler -> worker
			 * int[2] {<total number of workers>, <data size for each worker>}
			 */
		INIT_DATA,
			/* scheduler -> worker
			 *	int[1+] {<data for each worker>}
			 */
		MERGE_UPPERBOUND,
			/* worker_merge -> worker_data
			 * int[1] {<maximal number of worker_merge>}
			 */
		MERGE_REQUEST_SIZE,
			/* worker_data -> worker_merge
			 * int[1] {<number of numbers to be merged>}
			 */
		MERGE_REQUEST_DATA,
			/* worker_data -> worker_merge
			 * int[1+] {<elements to be merged>}
			 */
		MERGE_RESULT,
			/* worker_merge -> worker_data
			 * int[1+] {<the larger elements>}
			 */
		FINISH,
			/* worker -> scheduler
			 * int[1+] {<result>}
			 */
	};
}

static void scheduler(int nworker, CallBackBase &callback);
static void do_sort(int pid, CallBackBase &callback);

/**
 * \brief return the index of the first number in buf not less than key
 */
static int find_gte(const int *buf, int len, int key);

static inline void recv(void *buf, int len, int from, int tag)
{
	MPI_Recv(buf, len, MPI_INTEGER, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

static inline int recv_int(int from, int tag)
{
	int n;
	MPI_Recv(&n, 1, MPI_INTEGER, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	return n;
}

static inline void send(const void *buf, int len, int to, int tag)
{
	MPI_Send((void*)(buf), len, MPI_INTEGER, to, tag, MPI_COMM_WORLD);
}

static inline void send_int(int val, int to, int tag)
{
	MPI_Send(&val, 1, MPI_INTEGER, to, tag, MPI_COMM_WORLD);
}

void sort(int rank, int comm_size, CallBackBase &callback)
{
	if (comm_size == 1)
		error_exit("at least 2 processes are needed\n");
	if (!rank)
		scheduler(comm_size - 1, callback);
	else do_sort(rank, callback);
}

void do_sort(int pid, CallBackBase &callback)
{
	int metadata[2];
	recv(metadata, 2, 0, tag::METADATA);
	int nworker = metadata[0], data_size = metadata[1],
		*data = new int[data_size], *recv_buf = new int[data_size],
		*merge_tmp = new int[data_size];
	log_printf("worker %d started, nworker=%d data_size=%d, receiving data ...", pid,
			nworker, data_size);
	recv(data, data_size, 0, tag::INIT_DATA);

	log_printf("worker %d: received %d numbers: {%d, ..., %d}", pid, data_size,
			data[0], data[data_size - 1]);

	long real_comm = 0, tot_comm = 0;
	for (int i = 0; i < nworker; i ++)
	{
		if (data_size > 1)
			callback.sort(data, data + data_size);
		if ((i ^ pid) & 1)
		{
			// send data and recv merge result
			if (pid == 1)
				continue;
			int upper = recv_int(pid - 1, tag::MERGE_UPPERBOUND),
				size = find_gte(data, data_size, upper);
			real_comm += size;
			tot_comm += data_size;
			send_int(size, pid - 1, tag::MERGE_REQUEST_SIZE);
			if (size)
			{
				send(data, size, pid - 1, tag::MERGE_REQUEST_DATA);
				recv(data, size, pid - 1, tag::MERGE_RESULT);
			}
		} else
		{
			// recv data, merge and send back
			if (pid == nworker)
				continue;
			send_int(data[data_size - 1], pid + 1, tag::MERGE_UPPERBOUND);
			int size = recv_int(pid + 1, tag::MERGE_REQUEST_SIZE);
			if (!size)
				continue;
			recv(recv_buf, size, pid + 1, tag::MERGE_REQUEST_DATA);
			memcpy(merge_tmp, data, sizeof(int) * data_size);
			int *p0 = merge_tmp + data_size - 1, *p1 = recv_buf + size - 1;
			for (int *t = data + size - 1; t >= data; t --)
				*t = *p0 > *p1 ? *(p0 --) : *(p1 --);
			send(data, size, pid + 1, tag::MERGE_RESULT);

			int *t = data + data_size - 1;
			while (p0 >= merge_tmp && p1 >= recv_buf)
				*(t --) = *p0 > *p1 ? *(p0 --) : *(p1 --);
			memcpy(data, p0 >= merge_tmp ? merge_tmp : recv_buf,
					sizeof(int) * (t - data + 1));
		}
	}

	if (tot_comm)
	{
		log_printf("worker %d: %ld/%ld communication payload (%.3lf%%)",
				pid, real_comm, tot_comm, real_comm * 100.0 / tot_comm);
	}


	send(data, data_size, 0, tag::FINISH);

	delete []data;
	delete []recv_buf;
	delete []merge_tmp;
}

void scheduler(int nworker, CallBackBase &callback)
{
	size_t orig_size = callback.get_data_size();
	if (orig_size < (size_t)nworker)
		error_exit("too many workers\n");
	if (orig_size / nworker > (size_t)std::numeric_limits<int>::max())
		error_exit("dataset too large, or too few workers\n");

	int dist_size = orig_size / nworker;
	if (orig_size % nworker)
		dist_size ++;
	int *data = new int[dist_size];
	int metadata[2] = {nworker, dist_size};
	for (int i = 1; i <= nworker; i ++)
	{
		log_printf("sending data to worker %d ...", i);
		for (int j = callback.read_data(data, dist_size); j < dist_size; j ++)
			data[j] = std::numeric_limits<int>::max();
		send(metadata, 2, i, tag::METADATA);
		send(data, dist_size, i, tag::INIT_DATA);
		log_printf("data sent to worker %d", i);
	}

	size_t cnt = 0;
	for (int i = 1, prev = std::numeric_limits<int>::min();
			i <= nworker; i ++)
	{
		log_printf("receiving data from worker %d ...", i);
		recv(data, dist_size, i, tag::FINISH);
		log_printf("data received from worker %d", i);
		callback.write_data(data, dist_size);
		for (int j = 0; j < dist_size && cnt < orig_size; j ++, cnt ++)
		{
			int num = data[j];
			if (num < prev)
			{
				log_printf("ERROR! descending sequence for worker %d data[%d], prev=%d, num=%d",
						i, j, prev, num);
			}
			prev = num;
		}
	}
	delete []data;
}

int find_gte(const int *buf, int len, int key)
{
	long register left = 0, right = len;
	while (left != right)
	{
		long register mid = (left + right) >> 1;
		if (buf[mid] < key)
			left = mid + 1;
		else
			right = mid;
	}
	return left;
}

// vim: syntax=cpp11.doxygen

