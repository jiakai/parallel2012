# -*- coding: utf-8 -*-
# $File: utils.py
# $Date: Fri Aug 31 09:28:29 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

__all__ = ['sumentry', 'get_line_cycler', 'default_filter']

class SummedEntry(object):
    niter = 0
    nthread = 0
    nthread_real = 0
    size = 0
    chunksize = 0

    tot_cpu = 0
    tot_real = 0
    collision_cpu = 0
    gravity_cpu = 0
    mktree_cpu = 0

    cnt = 0


def sumentry(lst):
    """:return: dict(<name> => virtual entry with all fields summed)"""
    ret = dict()
    for i in lst:
        s = ret.get(i.worker)
        if not s:
            s = SummedEntry()
            ret[i.worker] = s
        for item in vars(SummedEntry).keys():
            if item == 'cnt' or item[0] == '_':
                continue
            setattr(s, item, getattr(s, item) + getattr(i, item))
        s.cnt += 1
    return ret

def get_line_cycler():
    lines = ['-', '-.', ':', '--']
    markers = ['o', 'v', '^', '<', '>',  '2', '3', '4', 's', 'p',
            '*', 'h', 'H', '+', 'x', 'D', 'd', '|', '_']
    lst = list()
    for i in range(len(markers)):
        lst.append(lines[i % len(lines)] + markers[i])
    from itertools import cycle
    return cycle(lst)

class default_filter(object):
    worker = 'pthread'
    niter = 500
    size = 10000
    nthread = 12
    chunksize = 100

    def __init__(self, **kargs):
        for (k, v) in kargs.iteritems():
            setattr(self, k, v)

    def __call__(self, x):
        for i in vars(default_filter).keys():
            if i[0] == '_' or getattr(self, i) is None:
                continue
            if getattr(self, i) != getattr(x, i):
                return False
        return True

