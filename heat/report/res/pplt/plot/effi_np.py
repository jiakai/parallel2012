# -*- coding: utf-8 -*-
# $File: effi_np.py
# $Date: Tue Sep 04 19:33:16 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency vs # processes"""

from pplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers, xlabel = '# Threads'):
    baseline = filter(default_filter(worker = 'seq'), data)
    assert len(baseline) == 1
    baseline = baseline[0]
    ax.set_ylabel('Efficiency')
    ax.set_xlabel(xlabel)
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk, nthread = None), data)
        d.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
        ax.plot([i.nthread for i in d], [baseline.tot_real / (i.tot_real *
            i.nthread) for i in d], next(cyc), label = i.worker)
    ax.legend()

def plot_realtime(data, ax, workers, xlabel = '# Threads'):
    ax.set_ylabel('Real Time [sec]')
    ax.set_xlabel(xlabel)
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk, nthread = None), data)
        d.sort(cmp = lambda x, y: cmp(x.nthread, y.nthread))
        ax.plot([i.nthread for i in d], [i.tot_real for i in d],
                next(cyc), label = i.worker)
    ax.legend()

def plot(data):
    fig = plt.figure()
    fig.suptitle(('Efficiency Comparison (size={d.width}x{d.height},' +
            'niter={d.niter})').format(d = default_filter))
    plot_efficiency(data, fig.add_subplot(221), ['pthread', 'omp'])
    plot_efficiency(data, fig.add_subplot(222), ['mpi'], '# Processes')
    plot_realtime(data, fig.add_subplot(223), ['pthread', 'omp'])
    plot_realtime(data, fig.add_subplot(224), ['mpi'], '# Processes')
    plt.savefig('effi-np.pdf')

