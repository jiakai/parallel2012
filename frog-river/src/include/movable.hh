/*
 * $File: movable.hh
 * $Date: Sat Aug 18 11:56:16 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_MOVABLE_
#define _HEADER_MOVABLE_

#include "utils.hh"

/*!
 * \brief a movable object, which is updated by a mover thread
 */
struct Movable
{
	int x0, y0, width, height;
	MutexLock lock;

	Movable(int x0_, int y0_, int w, int h):
		x0(x0_), y0(y0_), width(w), height(h)
	{}
};

struct ThreadData
{
	int row,
		// frog can only move between adjacent rows, range: [-1, NROW-1]
		// top most row is NROW - 1
		velocity;	// should be initialzed before invoking the thread
	Movable *vehicle, *load; // load must sit within vehicle

	ThreadData *adj_left, *adj_right;
	MutexLock lock_load;

	ThreadData(int r, int velo, Movable *vehi, ThreadData *left = NULL, 
			ThreadData *right = NULL):
		row(r), velocity(velo), vehicle(vehi), load(NULL), adj_left(left),
		adj_right(right)
	{
	}
};

void* thread_mover(ThreadData *data);

#endif // _HEADER_MOVABLE_

// vim: syntax=cpp11.doxygen

