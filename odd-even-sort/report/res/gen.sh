#!/bin/bash -e
# $File: gen.sh
# $Date: Tue Aug 14 17:21:22 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in $(find . -name "*.plt")
do
	pushd . > /dev/null
	cd $(dirname $i)
	./$(basename $i)
	popd > /dev/null
done

