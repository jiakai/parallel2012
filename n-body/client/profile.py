#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: profile.py
# $Date: Tue Aug 28 23:31:44 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>


if __name__ == '__main__':
    import sys
    from cProfile import run

    if len(sys.argv) != 3:
        sys.exit('usage: {0} <host> <port>'.format(sys.argv[0]))
    from nbcl import main

    run('main(sys.argv[1], sys.argv[2])', 'profile.output')



