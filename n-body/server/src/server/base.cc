/*
 * $File: base.cc
 * $Date: Wed Aug 29 19:44:22 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "server/base.hh"
#include "worker/mpi.hh"

void ServerBase::run(WorkerBase &worker)
{
	MPIWorker *m = dynamic_cast<MPIWorker*>(&worker);
	if (m && !m->is_master())
		m->next_step();
	else
		do_run(worker);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

