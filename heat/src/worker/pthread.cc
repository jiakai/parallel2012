/*
 * $File: pthread.cc
 * $Date: Tue Sep 04 06:56:59 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/pthread.hh"
#include <cassert>
#include <cstdio>

PthreadWorker::PthreadWorker(int nthread, int w, int h, real_t t0):
	WorkerBase(w, h, t0), m_nthread(nthread),
	m_thread_id(new pthread_t[nthread]),
	m_thread_arg(new update_arg_t[nthread])
{
	printf("pthread: nthread=%d\n", nthread);
	divide_task(m_thread_arg, nthread);
}

PthreadWorker::~PthreadWorker()
{
	delete []m_thread_id;
	delete []m_thread_arg;
}

real_t** PthreadWorker::next_step(BoundaryBase &bd_cond)
{
	real_t **src = m_buf[m_cur_buf],
		   **dest = m_buf[m_cur_buf ^= 1];
	for (int i = 1; i < m_nthread; i ++)
	{
		update_arg_t &a = m_thread_arg[i];
		a.src = src;
		a.dest = dest;
		PTHREAD_CHKERR_CALL(pthread_create, &m_thread_id[i],
				NULL, thread_wrapper, &a);
	}
	m_thread_arg[0].src = src;
	m_thread_arg[0].dest = dest;
	update(m_thread_arg[0]);
	for (int i = 1; i < m_nthread; i ++)
		PTHREAD_CHKERR_CALL(pthread_join, m_thread_id[i], NULL);
	bd_cond.update(dest, m_width, m_height);
	return dest;
}

void* PthreadWorker::thread_wrapper(void *arg)
{
	WorkerBase::update(*static_cast<WorkerBase::update_arg_t*>(arg));
	return NULL;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

