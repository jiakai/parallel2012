/*
 * $File: area.cc
 * $Date: Sat Aug 25 15:52:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "imgout/area.hh"
using namespace conf;

#include <cstdio>

AreaImgout::AreaImgout(): m_cnt(0)
{ }

void AreaImgout::draw(const Point *buf, size_t size)
{
	LOCK(m_lock_cnt);
	for (size_t i = 0; i < size; i ++)
		m_cnt += buf[i].niter == conf::niter_max;
}

bool AreaImgout::wait()
{
	printf("area=%.5lf\n", double(m_cnt * domain_width * domain_width /
			(res_x * res_x)));
	return false;
}
// vim: syntax=cpp11.doxygen

