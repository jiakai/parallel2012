#!/bin/bash -e
# $File: mksrclist.sh
# $Date: Sat Aug 25 16:31:40 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in $(find ../src -type f)
do
	echo "\\subsubsection{${i#../src/}}"
	echo "\\cppsrc{${i#../}}"
done

