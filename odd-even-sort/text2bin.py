#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# $File: text2bin.py
# $Date: Thu Aug 09 14:15:08 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import array
import sys
n = int(raw_input())
seq = [int(raw_input()) for i in range(n)]
arr = array.array('i', [n])
arr.extend(seq)
arr.tofile(sys.stdout)

