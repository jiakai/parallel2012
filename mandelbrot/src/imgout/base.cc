/*
 * $File: base.cc
 * $Date: Fri Aug 24 23:55:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "imgout/base.hh"
#include "utils.hh"

#include <cstdio>

ImgoutBase::Pixel* ImgoutBase::load_colormap()
{
	using conf::niter_max;
	FILE *fin = fopen(COLORMAP_FILEPATH, "r");
	if (!fin)
		error_exit("failed to open colormap %s: %m", COLORMAP_FILEPATH);
	int ncolor;
	fscanf(fin, "%d", &ncolor);
	Pixel *orig = new Pixel[ncolor],
		  *buf = new Pixel[niter_max + 1];
	for (int i = 0; i < ncolor; i ++)
		fscanf(fin, "%d%d%d", &orig[i].r, &orig[i].g, &orig[i].b);
	for (int i = 0; i < niter_max; i ++)
		buf[i] = orig[int(double(i) / (niter_max - 1) * (ncolor - 1))];
	delete []orig;
	fclose(fin);
	buf[niter_max].r = buf[niter_max].g = buf[niter_max].b = 0;
	return buf;
}

// vim: syntax=cpp11.doxygen

