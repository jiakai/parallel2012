#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: stat.py
# $Date: Tue Aug 28 23:34:15 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from pstats import Stats
s = Stats('profile.output')
s.print_stats()

