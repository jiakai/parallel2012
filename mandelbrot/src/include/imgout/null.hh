/*
 * $File: null.hh
 * $Date: Fri Aug 24 12:01:37 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IMGOUT_NULL_
#define _HEADER_IMGOUT_NULL_

#include "imgout/base.hh"
class NullImgout: public ImgoutBase
{
	public:
		void draw(const Point *buf, size_t size){}
		void init() {}
		bool wait() { return false; }
};

#endif // _HEADER_IMGOUT_NULL_

// vim: syntax=cpp11.doxygen

