#!/usr/bin/env pypy
# -*- coding: utf-8 -*-
# $File: mkdata.py
# $Date: Thu Aug 30 18:41:45 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from random import random
from math import *
import sys
sys.path.append('../client')
from nbcl.vector import Vector

if len(sys.argv) != 2:
    sys.exit('usage: {0} <nbody>'.format(sys.argv[0]))

def rrand(min, max):
    return random() * (max - min) + min

sqr = lambda x: x * x
class Body:
    pos = None
    velo = None
    r = None
    m = None
    def __init__(self, p, v, r, m):
        self.pos = p
        self.velo = v
        self.r = r
        self.m = m
    def test_collision(self, b):
        d = sqrt(sum([sqr(self.pos[i] - b.pos[i])
            for i in range(3)]))
        return d <= self.r + b.r

    def __str__(self):
        return ' '.join(str(i) for i in self.pos + self.velo + [self.r, self.m])

nbody = int(sys.argv[1])

def cluster():
    def make_cluster(center, r0 = 6, rbody = 1, mass = 1):
        bd = list()
        for i in range(nbody):
            while True:
                a = random() * pi * 2
                b = random() * pi * 2
                r = random() * r0
                cur = Body([center[0] + cos(a) * cos(b) * r, 
                    center[1] + cos(a) * sin(b) * r,
                    center[2] + sin(a) * r],
                    [0, 0, 0], rbody, mass)
                ok = True
                for j in bd:
                    if cur.test_collision(j):
                        ok = False
                        break
                if ok:
                    bd.append(cur)
                    break
        return bd

    data = [Body([-60, 0, 0], [0, 0, 0], 0.1, 1e5)] + \
            make_cluster([60, 0, 0], r0 = 13, mass = 1)
    #data = [Body([-500, 0, -100], [200, 0, 0], 1, 1e5)] + \
    #        make_cluster([0, 0, -100], r0 = 15, rbody = 2, mass = 3)

    print len(data)
    for i in data:
        print i


def rand(rng = 595, v = 30, r = 0.6, mass = 20):
    print nbody
    lst = [Body([0, 0, 0], [0, 0, 0], r + 1, 1e4)]
    print lst[0]
    for i in range(nbody - 1):
        while True:
            cur = Body([rrand(-rng, rng) for i in range(3)],
                    [rrand(-v, v) for i in range(3)],
                    rrand(0.5, r),
                    rrand(0.5, mass))
            ok = True
            if len(lst) < 8000:
                for i in lst:
                    if i.test_collision(cur):
                        ok = False
                        break
            if ok:
                lst.append(cur)
                print cur
                break

def matrix(rng = 595):
    lst = []
    ndiv = int(ceil(pow(nbody, 1.0 / 3)))
    zdiv = nbody / ndiv / ndiv
    p = 2.0 * rng / ndiv
    for x in range(0, ndiv):
        xp = x * p - rng
        for y in range(0, ndiv):
            yp = y * p - rng
            for z in range(0, zdiv):
                lst.append(Body([xp, yp, z * 2.0 * rng / zdiv - rng],
                    [rrand(-5, 5) for i in range(3)],
                    1, 10))
    print len(lst)
    for i in lst:
        print i

def rand_orbit(rng = 20):
    m0 = 1e6
    lst = [Body([0, 0, 0], [0, 0, 0], 5, m0)]
    for i in range(nbody):
        while True:
            pos = Vector(*[rrand(-rng, rng) for i in range(3)]).normalize() * \
                    rrand(10, rng)
            pos1 = Vector(*[rrand(-rng, rng) for i in range(3)])
            velo = pos.cross(pos1).normalize() * sqrt(m0 / pos.mod())
            cur = Body(pos.tolist(), velo.tolist(),
                    rrand(0.5, 2),
                    rrand(0.5, 10))
            ok = True
            for i in lst:
                if i.test_collision(cur):
                    ok = False
                    break
            if ok:
                lst.append(cur)
                break
    print len(lst)
    for i in lst:
        print i

def circle(r0 = 1):
    R = nbody * 2.5 * r0 / (pi * 2)
    bd = list()
    for i in range(nbody):
        t = float(i) / nbody * pi * 2
        bd.append(Body([R * cos(t), R * sin(t), 0], [-R * sin(t), R * cos(t),
            0], r0, 100))
    print len(bd)
    for i in bd:
        print i

circle()
