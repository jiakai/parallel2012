# -*- coding: utf-8 -*-
# $File: __init__.py
# $Date: Mon Aug 27 23:24:47 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""n-body client display package"""

def main(srvaddr, port):
    from nbcl.client import Client
    from nbcl.app import run_app
    from nbcl.gl import GLDrawer
    cli = Client(srvaddr, port)
    gld = GLDrawer('n-body display client', cli.boundary)
    gld.start()
    run_app(cli, gld.draw_callback)

