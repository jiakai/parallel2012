#!/usr/bin/env gnuplot
# $File: realtime-nproc-bubble.plt
# $Date: Tue Aug 14 18:44:16 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale y
set title "Bubble Sort" 
set xlabel "# Processes"
set ylabel "Real Run Time [sec]"
set xtics 12, 12
set xrange [10:80]

set terminal postscript enhanced color
set output "realtime-nproc-bubble.eps"

set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

cd "../data/bubble-ext"
plot \
	"size10000" using 2:4 title "n=1e4" , \
	"size50000" using 2:4 title "n=5e4" , \
	"size100000" using 2:4 title "n=1e5" , \
	"size500000" using 2:4 title "n=5e5" , \
	"size1000000" using 2:4 title "n=1e6" , \
	"size5000000" using 2:4 title "n=5e6" ls 6, \
	x <= 62 && x >= 10 ? 1362.96 / x : 1 / 0 title "k*n^2/m"


