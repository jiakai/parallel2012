/*
 * $File: pthread.cc
 * $Date: Fri Aug 31 10:16:41 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/pthread.hh"
#include "profile.hh"
#include <cstdio>

struct PthreadWorker::ThreadArg
{
	PthreadWorker *self;
	PthreadWorker::phase_func_t func;
	SPTreeBase *root;
};

PthreadWorker::PthreadWorker(int nthread, Body *body, int nbody, SPTreeFactoryBase &tree_factory):
	WorkerBase(body, nbody, tree_factory), m_nthread(nthread)
{
	printf("pthread: nslave=%d\n", nthread);
}

void PthreadWorker::enter_multithread(ThreadArg &arg, phase_func_t func)
{
	arg.func = func;
	m_nthread_remain = m_nthread;
	m_nbody_remain = m_body.size();
	for (int i = 0; i < m_nthread; i ++)
	{
		pthread_t pt;
		PTHREAD_CHKERR_CALL(pthread_create, &pt, NULL, thread_wrapper, &arg);
	}
	for (; ;)
	{
		LOCK(m_cond_nthreadrem);
		if (!m_nthread_remain)
			return;
		while (m_nthread_remain)
			m_cond_nthreadrem.wait();
	}
}

void* PthreadWorker::thread_wrapper(void *arg_)
{
	pthread_detach(pthread_self());
	ThreadArg *arg = static_cast<ThreadArg*>(arg_);
	PthreadWorker *self = arg->self;
	phase_func_t func = arg->func;

	Body **body = self->m_body.begin();
	int &nbodyrem = self->m_nbody_remain;

	for (; ;)
	{
		int cur;
		{
			LOCK(self->m_lock_nbodyrem);
			if (!nbodyrem)
				break;
			cur = --nbodyrem;
		}
		(self->*func)(*body[cur], arg->root);
	}
	{
		LOCK(self->m_cond_nthreadrem);
		self->m_nthread_remain --;
	}
	self->m_cond_nthreadrem.signal();
	return NULL;
}

void PthreadWorker::next_step()
{
	m_ncollision = 0;

	sptptr_t tree = m_tree_factory.mktree(m_body);
	ThreadArg targ;
	targ.self = this;
	targ.root = tree.get_ptr();

	profile_start("pthread_gravity");
	enter_multithread(targ, &PthreadWorker::update_gravity);
	profile_end("pthread_gravity");

	profile_start("pthread_collision");
	enter_multithread(targ, &PthreadWorker::update_collision);
	for (int i = 0; i < m_ncollision; i ++)
	{
		Body &b = *m_collision[i];
		BodyData &d = m_body_data[b.num];
		b.center = d.ctnew;
		b.velocity = d.vnew;
	}
	profile_end("pthread_collision");

	enter_multithread(targ, &PthreadWorker::update_pos);
}

void PthreadWorker::log_collision(Body *body)
{
	LOCK(m_lock_collog);
	m_collision[m_ncollision ++] = body;
}

// vim: syntax=cpp11.doxygen

