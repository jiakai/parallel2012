/*
 * $File: utils.hh
 * $Date: Fri Aug 24 11:24:06 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_UTILS_
#define _HEADER_UTILS_

#include <pthread.h>
#include <sys/time.h>

#include <cstring>

class MutexLock
{
	protected:
		pthread_mutex_t m_lock;

	public:

		MutexLock();
		~MutexLock();
		void acquire();
		void release();
};

/*!
 * \brief print error information and exit the process
 */
void error_exit(const char *fmt, ...) __attribute__((format(printf, 1, 2),
			noreturn));

#define ITER_STD(vec, var) \
	for (typeof(vec.begin()) var = vec.begin(); var != vec.end(); var ++)


class ScopedLock
{
	MutexLock &m_lock;

	public:

		ScopedLock(MutexLock &lock):
			m_lock(lock)
		{
			m_lock.acquire();
		}

		~ScopedLock()
		{
			m_lock.release();
		}
};

#define __lock_name(x) __lock_ ## x 
#define _lock_name(x) __lock_name(x)

/*!
 * \brief keep the lock in the current scope
 */
#define LOCK(locker) ScopedLock _lock_name(__LINE__)(locker)


class Condition: public MutexLock
{
	pthread_cond_t m_cond;

	public:
		Condition();
		~Condition();

		void signal();
		void wait();
};


/*!
 * \brief a timer using the hardware clock to measure real time
 */
class HWTimer
{
	timeval m_start;

	public:
		
		HWTimer();
		void reset();
		double get_sec() const;
};

///< call a pthread function and checks for error
#define PTHREAD_CHKERR_CALL(func, ...) \
	do \
{ \
	int ret =  func(__VA_ARGS__); \
	if (ret) \
		error_exit("failed to call %s: %s", #func, strerror(ret)); \
} while(0)


/*!
 * \brief return the current process CPU time in seconds
 */
double get_cputime();

/*!
 * \brief return the number of CPUs
 */
int get_nrcpu();

#endif // _HEADER_UTILS_

// vim: syntax=cpp11.doxygen

