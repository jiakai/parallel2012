/*
 * $File: profile.hh
 * $Date: Tue Aug 28 13:55:05 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_PROFILE_
#define _HEADER_PROFILE_

#include <cstdio>

void profile_start(const char *name);
void profile_end(const char *name);
void profile_print(FILE *fout);

#endif // _HEADER_PROFILE_

// vim: syntax=cpp11.doxygen

