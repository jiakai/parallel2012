/*
 * $File: utils.cc
 * $Date: Fri Aug 24 10:41:35 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <cstdlib>
#include <cstdarg>
#include <cstdio>

MutexLock::MutexLock()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_init, &m_lock, NULL);
}

MutexLock::~MutexLock()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_destroy, &m_lock);
}

void MutexLock::acquire()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_lock, &m_lock);
}

void MutexLock::release()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_unlock, &m_lock);
}

Condition::Condition()
{
	PTHREAD_CHKERR_CALL(pthread_cond_init, &m_cond, NULL);
}

Condition::~Condition()
{
	PTHREAD_CHKERR_CALL(pthread_cond_destroy, &m_cond);
}

void Condition::signal()
{
	PTHREAD_CHKERR_CALL(pthread_cond_signal, &m_cond);
}

void Condition::wait()
{
	PTHREAD_CHKERR_CALL(pthread_cond_wait, &m_cond, &m_lock);
}

void error_exit(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(EXIT_FAILURE);
}

HWTimer::HWTimer()
{
	reset();
}

void HWTimer::reset()
{
	gettimeofday(&m_start, NULL);
}

double HWTimer::get_sec() const
{
	timeval tv;
	gettimeofday(&tv, NULL);
	double ret = tv.tv_sec - m_start.tv_sec +
		1e-6 * (tv.tv_usec - m_start.tv_usec);
	if (ret < 0)
		ret += 24 * 3600;
	return ret;
}

double get_cputime()
{
	struct rusage u;
	getrusage(RUSAGE_SELF, &u);
#define t(x) x.tv_sec + x.tv_usec * 1e-6
	return t(u.ru_utime) + t(u.ru_stime);
#undef t
}

int get_nrcpu()
{
	return sysconf(_SC_NPROCESSORS_ONLN);
}

// vim: syntax=cpp11.doxygen

