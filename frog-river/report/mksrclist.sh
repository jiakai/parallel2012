#!/bin/bash -e
# $File: mksrclist.sh
# $Date: Tue Aug 14 18:28:43 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in $(find ../src -type f)
do
	echo "\\subsubsection{$(basename $i)}"
	echo "\\cppsrc{${i#../}}"
done

