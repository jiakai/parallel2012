/*
 * $File: base.cc
 * $Date: Tue Aug 28 22:39:58 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "sptree/base.hh"
#include "config.hh"

#include <algorithm>
using namespace std;

const int SPTreeBase::DIVIDE_MIN_NBODY = 10,
	  SPTreeBase::MAX_DEPTH = 15;

static bool cmp_x(const Body *b0, const Body *b1)
{ return b0->center.x < b1->center.x; }
static bool cmp_y(const Body *b0, const Body *b1)
{ return b0->center.y < b1->center.y; }
static bool cmp_z(const Body *b0, const Body *b1)
{ return b0->center.z < b1->center.z; }

void SPTreeBase::build(bodypbuf_t &body, int depth)
{
	m_data.chl = m_data.chr = NULL;
	m_data.body = body;
	if (!body.size())
		return;
	init(body);
	if ((int)body.size() <= DIVIDE_MIN_NBODY || depth > MAX_DEPTH)
		return;
	real_t maxcomp_val;
	int maxcomp = m_diam.get_max_comp(maxcomp_val);
	sort(body.begin(), body.end(), maxcomp == 0 ? cmp_x : maxcomp == 1 ? cmp_y : cmp_z);
	size_t d = 0;
	real_t dv = m_coord_min.comp(maxcomp) + maxcomp_val * 0.5;
	Body **pd = body.begin();
	while (d < body.size() && (*pd)->center.comp(maxcomp) < dv)
		d ++, pd ++;
	bodypbuf_t chl(body.begin(), d),
			  chr(pd, body.size() - d);
	m_data.chl = alloc_node(chl, depth + 1);
	m_data.chr = alloc_node(chr, depth + 1);
}

void SPTreeBase::init(const bodypbuf_t &body)
{
	Point coord_max;
	for (int i = 0; i < 3; i ++)
	{
		coord_max.comp(i) = -REAL_MAX;
		m_coord_min.comp(i) = REAL_MAX;
	}
	real_t tot_mass = 0;
	Vector tot_velo;
	Point tot_center;
	ITER_STD(body, p)
	{
		const Body &b = **p;
		for (int i = 0; i < 3; i ++)
		{
			update_max(coord_max.comp(i), b.center.comp(i));
			update_min(m_coord_min.comp(i), b.center.comp(i));
		}
		tot_mass += b.mass;
		tot_velo += b.velocity * b.mass;
		tot_center += b.center * b.mass;
	}
	m_diam = coord_max - m_coord_min;

	Body &sbd = m_data.synthesis_body;
	sbd.mass = tot_mass;
	real_t f = 1 / tot_mass;
	sbd.velocity = tot_velo * f;
	sbd.center = tot_center * f;

	real_t r = 0, r_bv = 0;
	m_data.bv_center = (m_coord_min + coord_max) * 0.5;
	ITER_STD(body, p)
	{
		const Body &b = **p;
		Vector ptend(b.center + b.velocity * conf::time_delta);	// point of time_delta end
		update_max(r, get_dist(sbd.center, b.center) + b.radius);
		update_max(r, get_dist(sbd.center, ptend) + b.radius);
		update_max(r_bv, get_dist(m_data.bv_center, b.center) + b.radius);
		update_max(r_bv, get_dist(m_data.bv_center, ptend) + b.radius);
	}
	sbd.radius = r;
	if (r < r_bv)
		m_data.bv_center = sbd.center, r_bv = r;
	m_data.bv_radius = r_bv;
	m_data.synthesis_min_dist = 2 * r_bv / conf::opening_agl_threshold;
}

Body* SPTreeBase::get_collision_body(const Body &body, real_t &time)
{
	if (get_dist(body.center, m_data.bv_center) >= body.radius + m_data.bv_radius)
		return NULL;
	if (m_data.is_leaf())
	{
		time = REAL_MAX;
		Body *ret = NULL;
		for (Body **p0 = m_data.body.begin(), **pt = m_data.body.end();
				p0 != pt; p0 ++)
		{
			Body *p = *p0;
			real_t tmp = p->get_collision_time(body);
			if (tmp >= 0 && tmp < time)
			{
				time = tmp;
				ret = p;
			}
		}
		return ret;
	}
	real_t t1, t2;
	Body *p1 = m_data.chl->get_collision_body(body, t1),
		 *p2 = m_data.chr->get_collision_body(body, t2);
	if (p1 && p2)
	{
		if (t1 < t2)
		{
			time = t1;
			return p1;
		}
		time = t2;
		return p2;
	}
	if (p2)
		p1 = p2, t1 = t2;
	time = t1;
	return p1;
}

// vim: syntax=cpp11.doxygen

