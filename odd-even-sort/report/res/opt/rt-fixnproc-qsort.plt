#!/usr/bin/env gnuplot
# $File: rt-fixnproc-qsort.plt
# $Date: Wed Aug 15 23:02:56 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale x
set logscale y
set title "Real Run Time Comparison (Quick Sort, nproc=48)"
set xlabel "Data Size"
set ylabel "Real Run Time [sec]" 
set y2label "Decrease/Original" tc lt 3

set xrange[9e5:6e9]
set y2range[-0.5:0.5]
set y2tics 0 0.1 nomirror tc lt 3

set terminal postscript enhanced color
set output "rt-fixnproc-qsort.eps"

set style data linespoints

cd "../../data"
plot \
	"qsort-ext/nproc48" using 1:4 title "original", \
	"optimized/qsort-ext/nproc48" using 1:4 title "optimized", \
	"cmp-qsort-nproc48" using 1:(1-$9/$4) title "decrease/original" axes x1y2, \
	0 notitle lt 3 axes x1y2
