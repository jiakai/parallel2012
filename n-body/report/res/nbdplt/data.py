# -*- coding: utf-8 -*-
# $File: data.py
# $Date: Thu Aug 30 22:04:18 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import re
import os.path
import os

class LogEntry(object):
    worker = None
    niter = None
    nthread = None
    nthread_real = None
    size = None
    chunksize = None

    tot_cpu = 0
    tot_real = 0
    collision_cpu = 0
    gravity_cpu = 0
    mktree_cpu = 0

    def __str__(s):
        return '.'.join(str(i) for i in (s.worker, s.niter, s.nthread, s.size,
            s.chunksize)) + ': {' + ','.join(str(i) for i in (s.tot_cpu,
                s.tot_real, s.collision_cpu, s.gravity_cpu, s.mktree_cpu)) + '}'

    _re_tot_time = [re.compile(r'^tot_cputime=(.*) tot_realtime=(.*) fps.*$'),
            'tot_cpu', 'tot_real']
    _re_collision = [re.compile(r'^.*_collision: cpu=(.*) real.*$'),
            'collision_cpu']
    _re_gravity = [re.compile(r'^.*_gravity: cpu=(.*) real.*$'),
            'gravity_cpu']
    _re_mktree = [re.compile(r'^seq_mktree: cpu=(.*) .*$'), 'mktree_cpu']

    _re_cpu_mpi = re.compile(r'^ *CPU time.*: *([0-9.]*) sec.*$')

    def __init__(self, fpath):
        name = os.path.basename(fpath).split('.')
        self.worker = name[0]
        [self.niter, self.nthread, self.size, self.chunksize] = [int(i)
                for i in name[1:-1]]
        with open(fpath) as f:
            if self.worker == 'mpi':
                self._parse_mpi(f)
            else:
                self._parse_normal(f)
        #assert self.tot_cpu and self.tot_real and self.collision_cpu and \
        #        self.gravity_cpu

    def _parse_normal(self, fobj):
        for line in fobj.readlines():
            for i in self._re_tot_time, self._re_collision, \
                    self._re_gravity, self._re_mktree:
                m = i[0].match(line)
                if m:
                    for j in range(1, len(i)):
                        setattr(self, i[j], float(m.group(j)) +
                                getattr(self, i[j]))
        self.nthread_real = self.nthread

    def _parse_mpi(self, fobj):
        self._parse_normal(fobj)
        fobj.seek(0)
        for line in fobj.readlines():
            m = self._re_cpu_mpi.match(line)
            if m:
                self.tot_cpu = float(m.group(1))
                return
        self.nthread_real = self.nthread - 1

def scan_dir(path):
    ret = list()
    for i in os.listdir(path):
        l = LogEntry(os.path.join(path, i))
        if l.tot_cpu > 0:
            ret.append(l)
    return ret

