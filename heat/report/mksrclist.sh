#!/bin/bash -e
# $File: mksrclist.sh
# $Date: Tue Sep 04 21:00:14 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in $(find ../src -type f)
do
	echo "\\subsubsection{${i#../src/}}"
	echo "\\cppsrc{${i#../}}"
done

