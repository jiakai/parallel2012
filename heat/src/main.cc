/*
 * $File: main.cc
 * $Date: Tue Sep 04 07:03:45 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/seq.hh"
#include "worker/pthread.hh"
#include "worker/omp.hh"
#include "worker/mpi.hh"

#include "boundary/border.hh"

#include "disp/x11.hh"
#include "disp/null.hh"

#include "utils.hh"

#include <getopt.h>

#include <cstdio>
#include <cstring>
#include <cstdlib>

static WorkerBase *worker;
static DispBase *disp;
static BoundaryBase *bd_cond;
static int read_int(const char *arg);
static real_t init_temp, bd_temp = 400;

static void parse_opt(int &argc, char **&argv);

int main(int argc, char **argv)
{
	parse_opt(argc, argv);
	disp->run(*worker, *bd_cond);
	delete disp;
	delete worker;
	delete bd_cond;
}

void parse_opt(int &argc, char **&argv)
{
	static const struct option long_opt[] = {
		{"worker", required_argument, NULL, 'w'},
		{"disp", required_argument, NULL, 'd'},
		{"nthread", required_argument, NULL, 'n'},
		{"width", required_argument, NULL, 'W'},
		{"height", required_argument, NULL, 'H'},
		{"help", no_argument, NULL, 'h'}
	};
	int width = 600, height = 600, nthread = get_nrcpu();
	bd_cond = new BorderBoundary(bd_temp);
	for (; ;)
	{
		int c = getopt_long(argc, argv, "w:d:n:W:H:h", long_opt, NULL);
		switch (c)
		{
			case -1:
				if (!worker)
					worker = new PthreadWorker(nthread, width, height, init_temp);
				if (!disp)
					disp = new X11Disp(bd_temp, width, height, "heat distribution");
				return;
			case 'w':
				if (!strcmp(optarg, "seq"))
					worker = new SeqWorker(width, height, init_temp);
				else if (!strcmp(optarg, "pthread"))
					worker = new PthreadWorker(nthread, width, height, init_temp);
				else if (!strcmp(optarg, "omp"))
					worker = new OMPWorker(nthread, width, height, init_temp);
				else if (!strcmp(optarg, "mpi"))
					worker = new MPIWorker(argc, argv, width, height, init_temp);
				else error_exit("unknown worker: %s", optarg);
				break;
			case 'd':
				if (!strcmp(optarg, "x11"))
					disp = new X11Disp(bd_temp, width, height, "heat distribution");
				else if (!strncmp(optarg, "null", 4))
					disp = new NullDisp(read_int(optarg + 4));
				else error_exit("unknown disp: %s", optarg);
				break;
			case 'n':
				if (worker)
					error_exit("nthread must be supplied before worker");
				nthread = read_int(optarg);
				break;
			case 'W':
				if (disp || worker)
					error_exit("width/height must be supplied before worker/disp");
				width = read_int(optarg);
				break;
			case 'H':
				if (disp || worker)
					error_exit("width/height must be supplied before worker/disp");
				height = read_int(optarg);
				break;
			default:
				fprintf(stderr, "usage: %s -w, -d, -n, -W, -H, -h\n", argv[0]);
				exit(c != 'h');
		}
	}
}

int read_int(const char *arg)
{
	int ret;
	if (sscanf(arg, "%d", &ret) != 1)
		error_exit("invalid integer format: %s", arg);
	return ret;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

