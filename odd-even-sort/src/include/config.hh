/*
 * $File: config.hh
 * $Date: Mon Aug 13 17:04:41 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_CONFIG_
#define _HEADER_CONFIG_

#define LOG_TIMEFMT		"%Y-%m-%d %T"

#endif // _HEADER_CONFIG_

// vim: syntax=cpp11.doxygen

