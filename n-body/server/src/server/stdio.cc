/*
 * $File: stdio.cc
 * $Date: Wed Aug 29 19:50:21 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"
#include "ios.hh"
#include "server/stdio.hh"
#include <string>
#include <cstdio>
#include <iostream>
using namespace std;

void STDIOServer::do_run(WorkerBase &worker)
{
	real_t time = 0;
	Body *body = worker.get_body_buf();
	int nbody = worker.get_nr_body();
	for (; ;)
	{
		printf("time=%.3lf\n", (double)time);
		for (int i = 0; i < nbody; i ++)
		{
			printf("  %d:", body[i].num);
			cout << "p=" << body[i].center <<
				" v=" << body[i].velocity << endl;
		}
		string str;
		getline(cin, str);
		worker.next_step();
		time += conf::time_delta;
	}
}

// vim: syntax=cpp11.doxygen

