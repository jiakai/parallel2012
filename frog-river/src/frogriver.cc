/*
 * $File: frogriver.cc
 * $Date: Sat Aug 18 19:44:30 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "frogriver.hh"
#include "config.hh"
#include "utils.hh"
#include "movable.hh"

#include <gtkmm/main.h>
#include <gdk/gdkkeysyms.h>

FrogRiverWindow::FrogRiverWindow():
	m_frog_velocity(0), m_frog_accel(0), m_frog_img(FROG_IMG_PATH)
{
	Glib::signal_timeout().connect(
			sigc::mem_fun(*this, &FrogRiverWindow::on_timeout),
			REFRESH_INTERVAL / 1000);
	add_vehicles();
	add(m_fixed_container);
	set_title("Frog River Game");
	set_size_request(WINDOW_WIDTH, WINDOW_HEIGHT);
	set_resizable(false);
	set_position(Gtk::WIN_POS_CENTER);

#ifndef GLIBMM_DEFAULT_SIGNAL_HANDLERS_ENABLED
	signal_key_press_event().connect(sigc::mem_fun(*this,
				&FrogRiverWindow::on_key_press_event), false);
	signal_key_release_event().connect(sigc::mem_fun(*this,
				&FrogRiverWindow::on_key_release_event), false);
#endif

	show_all();
}

FrogRiverWindow::~FrogRiverWindow()
{
	for (int i = 0; i < m_nvehicle; i ++)
	{
		ThreadInfo &p = m_vehicle_threads[i];
		if (i + 1 < m_nvehicle)
			pthread_cancel(p.pid);
		delete p.data->vehicle;
		delete p.data->load;
		delete p.data;
	}
	delete []m_vehicle_imgs;
	delete []m_vehicle_threads;
}

void FrogRiverWindow::add_vehicles()
{
	m_nvehicle = NROW * NVEHICLE_PER_ROW + 1;
	m_vehicle_imgs = new Gtk::Image[m_nvehicle];
	m_vehicle_threads = new ThreadInfo[m_nvehicle];
	for (int i = 0, p = 0; i < NROW; i ++)
	{
		int y = (NROW - 1 - i) * VEHICLE_HEIGHT + TOP_SPACE_SIZE;
		for (int j = 0; j < NVEHICLE_PER_ROW; j ++, p ++)
		{
			int x = j * WINDOW_WIDTH / NVEHICLE_PER_ROW;

			m_vehicle_imgs[p].set(VEHICLE_IMG_PATH);
			Glib::RefPtr<Gdk::Pixbuf> pb = m_vehicle_imgs[p].get_pixbuf();

			m_fixed_container.put(m_vehicle_imgs[p], x, y);
			ThreadInfo &info = m_vehicle_threads[p];
			info.data = new ThreadData(i, get_velocity(i, j),
					new Movable(x, y, pb->get_width(), pb->get_height()),
					j ? m_vehicle_threads[p - 1].data : NULL);
			if (j)
				m_vehicle_threads[p - 1].data->adj_right = info.data;
			if (pthread_create(&info.pid, NULL, (void*(*)(void*))thread_mover,
					info.data))
				error_exit("pthread_create");
		}
	}

	m_frog_img.set(FROG_IMG_PATH);
	int p = m_nvehicle - 1;
	Glib::RefPtr<Gdk::Pixbuf> frog_pb = m_frog_img.get_pixbuf();
	int frog_w = frog_pb->get_width(), frog_h = frog_pb->get_height(),
		frog_x = (WINDOW_WIDTH - frog_w) / 2,
		frog_y = VEHICLE_HEIGHT * NROW + TOP_SPACE_SIZE;
	m_fixed_container.put(m_frog_img, frog_x, frog_y);
	m_fixed_container.put(m_vehicle_imgs[p], 0, frog_y);
	m_vehicle_threads[p].data = new ThreadData(-1, 0, new Movable(0, frog_y,
				WINDOW_WIDTH, frog_h));
	m_vehicle_threads[p].data->load = new Movable(
			frog_x, frog_y, frog_w, frog_h);

	m_frog_pos = m_vehicle_threads[p].data;
}

bool FrogRiverWindow::on_timeout()
{
	Glib::RefPtr<Gdk::Window> win = get_window();
	if (win)
	{
		m_frog_velocity += m_frog_accel;
		try_move_hori(m_frog_velocity);
		for (int i = 0; i < m_nvehicle; i ++)
		{
			Movable *p = m_vehicle_threads[i].data->vehicle;
			{
				LOCK(p->lock);
				m_fixed_container.move(m_vehicle_imgs[i], p->x0, p->y0);
			}
			p = m_vehicle_threads[i].data->load;
			if (p)
			{
				LOCK(p->lock);
				m_fixed_container.move(m_frog_img, p->x0, p->y0);
			}
		}
		win->invalidate(true);
	}
	return true;
}

bool FrogRiverWindow::on_key_press_event(GdkEventKey *event)
{
	switch (event->keyval)
	{
		case 'q':
			Gtk::Main::quit();
			break;
		case GDK_Left:
			m_frog_accel = -FROG_MOVE_ACCEL;
			break;
		case GDK_Right:
			m_frog_accel = FROG_MOVE_ACCEL;
			break;
		case GDK_Up:
			try_move_vert(1);
			break;
		case GDK_Down:
			try_move_vert(-1);
			break;
	}
	return true;
}

bool FrogRiverWindow::on_key_release_event(GdkEventKey *event)
{
	switch (event->keyval)
	{
		case GDK_Left:
		case GDK_Right:
			m_frog_accel = 0;
			m_frog_velocity = 0;
			break;
	}
	return true;
}

void FrogRiverWindow::try_move_hori(int delta)
{
	if (!delta)
		return;
	Movable *frog = m_frog_pos->load,
			*veh = m_frog_pos->vehicle;
	{
		LOCK(frog->lock);
		LOCK(veh->lock);
		int x = frog->x0 + delta;
		if (x >= veh->x0 && x + frog->width <= veh->x0 + veh->width)
			frog->x0 = x;
	}
}

void FrogRiverWindow::try_move_vert(int delta)
{
	bool reset = false;
	Movable *frog = m_frog_pos->load;
	int r = m_frog_pos->row + delta, pos = -1;
	if (r < -1)
		return;
	if (r == NROW)
	{
		printf("Survived! time=%.3lf[sec]\n", m_timer.get_sec());
		reset = true;
		r = -1;
	}
	if (r == -1)
		pos = m_nvehicle - 1;
	else
	{
		LOCK(frog->lock);
		int xmin = frog->x0, xmax = frog->x0 + frog->width;
		for (int i = NVEHICLE_PER_ROW * r, it = i + NVEHICLE_PER_ROW;
				i < it; i ++)
		{
			Movable *v = m_vehicle_threads[i].data->vehicle;
			LOCK(v->lock);
			if (xmin >= v->x0 && xmax <= v->x0 + v->width)
			{
				pos = i;
				break;
			}
		}
		if (pos == -1)
		{
			printf("Dead!\n");
			pos = m_nvehicle - 1;
			reset = true;
		}
	}
	{
		LOCK(m_frog_pos->lock_load);
		m_frog_pos->load = NULL;
	}
	m_frog_pos = m_vehicle_threads[pos].data;
	{
		LOCK(m_frog_pos->lock_load);
		m_frog_pos->load = frog;
	}
	{
		Movable *v = m_frog_pos->vehicle;
		LOCK(frog->lock);
		LOCK(v->lock);
		frog->y0 = v->y0;
		if (reset)
		{
			frog->x0 = (WINDOW_WIDTH - frog->width) / 2;
			m_timer.reset();
		}
	}
}

// vim: syntax=cpp11.doxygen

