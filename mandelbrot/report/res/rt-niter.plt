#!/usr/bin/env gnuplot
# $File: rt-niter.plt
# $Date: Sat Aug 25 14:22:16 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set terminal postscript enhanced color
set output "rt-niter.eps"
set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

set title "Run Time Comparison (nthread=12 for pthread,size=5000x5000)"
set xlabel "Maximal Number of Iteration Steps"
set ylabel "Real Time [sec]" 
set y2label "Total CPU Time [sec]" 

set y2tics nomirror tc lt 2
set y2range [0.03*12:24*12]
set yrange [0.03:24]

set logscale 
set xrange [5:20000]
set xtics  1 10

cd "data/niter"
plot \
	"pthread" using 3:6 title "pthread real time", \
	"pthread" using 3:7 title "pthread total CPU time" axes x1y2, \
	"seq" using 3:7 title "sequential total CPU time" axes x1y2
