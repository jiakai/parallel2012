/*
 * $File: stdio.hh
 * $Date: Wed Aug 29 19:49:54 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_STDIO_
#define _HEADER_STDIO_

#include "server/base.hh"

class STDIOServer: public ServerBase
{
	protected:
		void do_run(WorkerBase &worker);
};

#endif // _HEADER_STDIO_

// vim: syntax=cpp11.doxygen

