/*
 * $File: border.hh
 * $Date: Sun Sep 02 11:00:02 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_BORDER_BOUNDARY_
#define _HEADER_BORDER_BOUNDARY_

#include "boundary/base.hh"

class BorderBoundary: public BoundaryBase
{
	real_t m_val;
	public:
		BorderBoundary(real_t val):
			m_val(val)
		{}

		void update(real_t **buf, int width, int height);
};

#endif // _HEADER_BORDER_BOUNDARY_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

