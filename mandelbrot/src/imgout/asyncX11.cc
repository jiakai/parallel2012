/*
 * $File: asyncX11.cc
 * $Date: Sat Aug 25 13:13:19 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "imgout/asyncX11.hh"

#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>

#include <unistd.h>

#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
using namespace conf;

AsyncX11Imgout::AsyncX11Imgout(const char *window_name, const char *disp_name):
	m_enter_wait(0),
	m_queue(new Point[res_x * res_y]),
	m_qh(0), m_qt(0), m_gc(NULL)
{
	if (!XInitThreads())
		error_exit("failed to init X threads");
	if ((m_display = XOpenDisplay(disp_name)) == NULL)
		error_exit("failed to open display: %s", XDisplayName(disp_name));

	int screen = DefaultScreen(m_display),
		disp_w = DisplayWidth(m_display, screen),
		disp_h = DisplayHeight(m_display, screen);
	m_win = XCreateSimpleWindow(m_display, RootWindow(m_display, screen),
			(disp_w - res_x) / 2, (disp_h - res_y) / 2, // x, y
			res_x, res_y,	// width, height
			1, // border_width
			BlackPixel(m_display, screen),	// border
			WhitePixel(m_display, screen));	// background

	XStoreName(m_display, m_win, window_name);
	init_gc();
	
	XMapWindow(m_display, m_win);
	XSync(m_display, false);
	PTHREAD_CHKERR_CALL(pthread_create, &m_pid_xevent, NULL, thread_xevent, this);
}

AsyncX11Imgout::~AsyncX11Imgout()
{
	XDestroyWindow(m_display, m_win);
	XCloseDisplay(m_display);
	delete []m_queue;
	delete []m_gc;
}

void AsyncX11Imgout::init()
{
	m_wait_ret = 0;
	m_enter_wait = 0;
	PTHREAD_CHKERR_CALL(pthread_create, &m_pid_draw, NULL, thread_draw, this);
}

void* AsyncX11Imgout::thread_draw(void *self_)
{
	AsyncX11Imgout *self = static_cast<AsyncX11Imgout*>(self_);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	int &qh = self->m_qh;
	for (; ;)
	{
		usleep(X11_DRAW_REFERESH_INTERVAL);
		for (; ;)
		{
			int qt;
			{
				LOCK(self->m_lock_qt);
				qt = self->m_qt;
			}
			LOCK(self->m_lock_qh);
			if (qh == qt)
				break;
			LOCK(self->m_lock_x11);
			while (qh < qt)
			{
				Point &p = self->m_queue[qh ++];
				XDrawPoint(self->m_display, self->m_win,
						self->m_gc[p.niter], p.x, p.y);
			}
		}
	}
	return NULL;
}

void AsyncX11Imgout::get_xevent(XEvent &ev)
{
	for (; ;)
	{
		{
			LOCK(m_lock_x11);
			if (XPending(m_display))
			{
				XNextEvent(m_display, &ev);
				return;
			}
		}
		usleep(X11_DRAW_REFERESH_INTERVAL);
	}
}

void* AsyncX11Imgout::thread_xevent(void *self_)
{
	AsyncX11Imgout *self = static_cast<AsyncX11Imgout*>(self_);
	XSelectInput(self->m_display, self->m_win,
			ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask);
	Atom wm_del_msg = XInternAtom(self->m_display, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(self->m_display, self->m_win, &wm_del_msg, 1);


	bool select_p0 = false;
	real_t select_x0 = 0, select_y0 = 0;

	for (; ;)
	{
		XEvent ev;
		self->get_xevent(ev);
		if (ev.type == Expose)
		{
			LOCK(self->m_lock_qh);
			self->m_qh = 0;
			continue;
		}
		if (!self->m_enter_wait)
			continue;
		bool quit = false, domain_changed = false, res_changed = false;
		switch(ev.type)
		{
			case ConfigureNotify:
				// f{{{
				{
					int w = ev.xconfigure.width,
						h = ev.xconfigure.height;
					if (w != res_x || h != res_y)
					{
						res_x = w;
						res_y = h;
						res_changed = true;
					}
				}
				break;
				// f}}}
			case KeyPress:
				// f{{{
				{
					int key = XLookupKeysym(&ev.xkey, 0);
					switch (key)
					{
						case XK_q:
							quit = true;
							break;
						case XK_s:
							save_domain();
							printf("domain saved to %s\n", DOMAIN_FILEPATH);
							break;
						case XK_z:
							domain_width *= 2;
							domain_changed = true;
							break;
					}
				}
				break;
				// f}}}
			case ClientMessage:
				// f{{{
				if (ev.xclient.data.l[0] == wm_del_msg)
					quit = true;
				break;
				// f}}}
			case ButtonPress:
				// f{{{
				{
					real_t
						x = ev.xbutton.x * domain_width / res_x + domain_xmin,
						y = ev.xbutton.y * domain_width / res_x + domain_ymin;
					if (ev.xbutton.button == Button1)
					{
						if (!select_p0)
						{
							printf("select region point 0\n");
							select_p0 = true;
							select_x0 = x;
							select_y0 = y;
						} else
						{
							printf("select region point 1\n");
							domain_xmin = min(x, select_x0);
							domain_ymin = min(y, select_y0);
							domain_width = max(fabs(x - select_x0),
									fabs(y - select_y0) / res_y * res_x);
							domain_changed = true;
						}
						break;
					}
					if (ev.xbutton.button == Button3)
					{
						domain_xmin = x - domain_width / 2;
						domain_ymin = y - domain_width / res_x * res_y / 2;
						domain_changed = true;
					}
				}
				break;
				// f}}}
		}
		if (domain_changed || res_changed)
		{
			select_p0 = false;
			self->m_wait_ret = 1;
			PTHREAD_CHKERR_CALL(pthread_cancel, self->m_pid_draw);
			self->m_qh = 0;
			self->m_qt = 0;
			if (res_changed)
			{
				delete []self->m_queue;
				self->m_queue = new ImgoutBase::Point[res_x * res_y];
			}
		}
		if (quit)
		{
			PTHREAD_CHKERR_CALL(pthread_cancel, self->m_pid_draw);
			return NULL;
		}
	}
	return NULL;
}

bool AsyncX11Imgout::wait()
{
	m_enter_wait = 1;
	PTHREAD_CHKERR_CALL(pthread_join, m_pid_draw, NULL);
	m_enter_wait = 0;
	return m_wait_ret;
}

void AsyncX11Imgout::init_gc()
{
	Colormap colormap = DefaultColormap(m_display, DefaultScreen(m_display));
	ImgoutBase::Pixel *n2c = load_colormap();
	m_gc = new GC[niter_max + 1];
	for (int i = 0; i <= niter_max; i ++)
	{
		m_gc[i] = XCreateGC(m_display, m_win, 0, NULL);
		char name[10];
		sprintf(name, "#%.2X%.2X%.2X", n2c[i].r, n2c[i].g, n2c[i].b);
		XColor color;
		XParseColor(m_display, colormap, name, &color);
		XAllocColor(m_display, colormap, &color);
		XSetForeground(m_display, m_gc[i], color.pixel);
	}
	delete []n2c;
}

void AsyncX11Imgout::draw(const Point *buf, size_t size)
{
	int qt0;
	{
		LOCK(m_lock_qt);
		qt0 = m_qt;
		m_qt += size;
	}
	memcpy(m_queue + qt0, buf, sizeof(Point) * size);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

