/*
 * $File: omp.hh
 * $Date: Sun Sep 02 17:28:42 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_OMP_
#define _HEADER_WORKER_OMP_

#include "worker/base.hh"

class OMPWorker: public WorkerBase
{
	int m_nthread;
	public:
		OMPWorker(int nthread, int w, int h, real_t t0);
		real_t **next_step(BoundaryBase &bd_cond);
};

#endif // _HEADER_WORKER_OMP_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

