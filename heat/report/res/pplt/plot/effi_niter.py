# -*- coding: utf-8 -*-
# $File: effi_niter.py
# $Date: Tue Sep 04 20:46:11 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency vs # processes"""

from pplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers,
        nthread = default_filter.nthread, thname = 'Threads'):
    baseline = filter(default_filter(worker = 'seq', niter = None), data)
    baseline = {i.niter: i for i in baseline}
    ax.set_ylabel('Efficiency')
    ax.set_xlabel('# Iterations')
    ax.set_title('({0} {1})'.format(nthread, thname))
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk,
            nthread = nthread, niter = None), data)
        d.sort(cmp = lambda x, y: cmp(x.niter, y.niter))
        ax.plot([i.niter for i in d],
                [baseline[i.niter].tot_real / (i.tot_real * i.nthread)
                    for i in d], next(cyc), label = i.worker)
    ax.legend()

def plot_realtime(data, ax, workers,
        nthread = default_filter.nthread, thname = 'Threads'):
    ax.set_ylabel('Real Time [sec]')
    ax.set_xlabel('# Iterations')
    ax.set_title('({0} {1})'.format(nthread, thname))
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk,
            nthread = nthread, niter = None), data)
        d.sort(cmp = lambda x, y: cmp(x.niter, y.niter))
        ax.plot([i.niter for i in d], [i.tot_real for i in d],
                next(cyc), label = i.worker)
    ax.legend()

def plot(data):
    fig = plt.figure()
    fig.suptitle('Efficiency Comparison (size={d.width}x{d.height})'
        .format(d = default_filter))
    plot_efficiency(data, fig.add_subplot(221), ['pthread', 'omp'])
    plot_efficiency(data, fig.add_subplot(222), ['mpi'], 48, 'Processes')
    plot_realtime(data, fig.add_subplot(223), ['pthread', 'omp'])
    plot_realtime(data, fig.add_subplot(224), ['mpi'], 48, 'Processes')
    plt.savefig('effi-niter.pdf')

