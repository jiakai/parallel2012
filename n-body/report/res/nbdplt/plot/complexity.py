# -*- coding: utf-8 -*-
# $File: complexity.py
# $Date: Fri Aug 31 09:27:48 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""algorithm complexity"""

from nbdplt.utils import *
import matplotlib.pyplot as plt
from math import *

def plot(data):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_title('Algorithm Complexity')
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('# Bodies')
    ax.set_ylabel('Total CPU Time')
    ax2 = ax.twinx()
    ax2.set_ylabel('proportion')
    cyc = get_line_cycler()
    dseq = None
    for worker in ['seq', 'pthread', 'pthreadpf', 'omp']:
        d = filter(default_filter(worker = worker, size = None), data)
        if worker == 'seq':
            dseq = d
        d.sort(cmp = lambda x, y: cmp(x.size, y.size))
        ax.plot([i.size for i in d], [i.tot_cpu for i in d], next(cyc),
                label = worker)
    xval = [i.size for i in dseq]
    scale_log = xval[0] * log(xval[0]) / dseq[0].tot_cpu
    scale_sqr = xval[0] * xval[0] / dseq[0].tot_cpu
    dseq = {i.size:i for i in dseq}
    ax2.plot(xval, [scale_log * dseq[x].tot_cpu / (x * log(x)) for x in xval],
        next(cyc), label = 'seq/(n*log(n))')
    ax2.plot(xval, [scale_sqr * dseq[x].tot_cpu / (x * x) for x in xval],
        next(cyc), label = 'seq/n^2')
    ax.legend(loc = "upper left")
    ax2.legend()
    ax2.set_yscale('log')

    plt.savefig('complexity.pdf')

