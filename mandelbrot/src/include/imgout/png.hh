/*
 * $File: png.hh
 * $Date: Fri Aug 24 15:31:54 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IMGOUT_PNG_
#define _HEADER_IMGOUT_PNG_

#include "imgout/base.hh"
#include "utils.hh"

#include <string>

class PNGImgout: public ImgoutBase
{
	Point *m_buf, *m_buf_pos;
	MutexLock m_lock_buf;

	std::string m_fpath;

	void save();

	public:
		PNGImgout(const char *fpath);
		~PNGImgout();
		void draw(const Point *buf, size_t size);
		void init() {}
		bool wait() { return false; }
};

#endif // _HEADER_IMGOUT_PNG_

// vim: syntax=cpp11.doxygen

