/*
 * $File: pthread-prefork.hh
 * $Date: Wed Aug 29 17:37:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_PTHREAD_PREFORK_
#define _HEADER_WORKER_PTHREAD_PREFORK_

#include "worker/base.hh"
#include "utils.hh"

class PthreadPreforkWorker: public WorkerBase
{
	int m_nthread, m_nbody_remain, m_nthread_remain;
	MutexLock m_lock_collog,	// lock for collision log
			  m_lock_nbodyrem;
	Condition m_cond_nthreadrem;

	///< thread signal type
	enum tsig_t { 
		TSIG_WAIT, TSIG_START, TSIG_EXIT
	};
	struct ThreadCtlInfo
	{
		tsig_t tsig;
		Condition cond;
		int tasknum;
		// broadcast to the condition to inform change of tsig or tasknum
		// thread waiting if tsig == TSIG_WAIT or tasknum == previous

		ThreadCtlInfo(): tsig(TSIG_WAIT), tasknum(0) {}
	};
	ThreadCtlInfo m_tctl_collision, m_tctl_gravity, m_tctl_pos;

	struct ThreadArg;
	friend struct ThreadArg;
	ThreadArg *m_targ;


	void log_collision(Body *body);
	static void* thread_wrapper(void*);

	void trigger_thread(ThreadCtlInfo &ctl, tsig_t tsig = TSIG_START);

	void update_collision(Body &b, SPTreeBase *spt)
	{ WorkerBase::update_collision(b, spt); }
	void update_gravity(Body &b, SPTreeBase *spt)
	{ WorkerBase::update_gravity(b, spt); }
	void update_pos(Body &b, SPTreeBase *)
	{ WorkerBase::update_pos(b); }

	public:
		PthreadPreforkWorker(int nthread, Body *body, int nbody, SPTreeFactoryBase &tree_factory);
		~PthreadPreforkWorker();

		const char* get_name()
		{ return "pthread"; }

		void next_step();
};

#endif // _HEADER_WORKER_PTHREAD_PREFORK_

// vim: syntax=cpp11.doxygen

