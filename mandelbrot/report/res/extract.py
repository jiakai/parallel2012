#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: extract.py
# $Date: Sat Aug 25 10:13:41 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import sys
import os
import re
if len(sys.argv) != 2:
    sys.exit('usage: {0} <predicate>')

_re_cputime = re.compile('.*CPU time.* (.*) sec.*')
_re_realtime = re.compile('.*realtime=(.*) cpu.*')
def extract_time(fname):
    if 'mpi' in fname:
        with open(fname) as f:
            for i in f.readlines():
                m = _re_cputime.match(i)
                if m:
                    cputime = m.group(1)
                m = _re_realtime.match(i)
                if m:
                    realtime = m.group(1)
            return (realtime, cputime)
    else:
        with open(fname) as f:
            data = f.readlines()[2][:-1]
            assert 'realtime' in data
            data = data.split(' ')
            return (data[i].split('=')[1] for i in (0, 1))


print '# backend nthread niter xres yres realtime cputime'
for i in sorted(os.listdir('log'), cmp=lambda a, b: cmp([int(i) for i in
    a.split('.')[1:-1]], [int(i) for i in b.split('.')[1:-1]])):
    (backend, nthread, niter, xres, yres) = i.split('.')[:-1]
    if eval(sys.argv[1]):
        (realtime, cputime) = extract_time('log/' + i)
        print backend, nthread, niter, xres, yres, realtime, cputime

