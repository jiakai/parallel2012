/*
 * $File: null.hh
 * $Date: Sun Sep 02 16:52:03 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_DISP_NULL_
#define _HEADER_DISP_NULL_

#include "disp/base.hh"

class NullDisp: public DispBase
{
	int m_niter;
	void do_run(WorkerBase &worker, BoundaryBase &bd_cond);

	public:
		NullDisp(int niter):
			m_niter(niter)
		{}
};


#endif // _HEADER_DISP_NULL_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

