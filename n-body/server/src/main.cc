/*
 * $File: main.cc
 * $Date: Thu Aug 30 10:45:23 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/seq.hh"
#include "worker/pthread.hh"
#include "worker/pthread-prefork.hh"
#include "worker/omp.hh"
#include "worker/mpi.hh"

#include "sptree/seq.hh"

#include "server/tcp.hh"
#include "server/stdio.hh"
#include "server/null.hh"

#include "config.hh"
#include "ios.hh"
#include "profile.hh"
#include "utils.hh"

#include <getopt.h>

#include <cstdlib>
#include <cstring>
#include <fstream>

using namespace std;

static ServerBase *server;
static WorkerBase *worker;
static SeqSPTreeFactory spt_factory;
static Body *body;
static int nbody, nthread;
static bool need_validate = true;

static const int DEFAULT_PORT = 8080;

static void validate(const Body *body, int nbody);
static void parse_opt(int &argc, char **&argv);
static WorkerBase* make_worker(const char *spec, int &argc, char **&argv);
static ServerBase* make_server(const char *spec);
static void read_input(const char *fpath);
static void usage(const char *name);

namespace conf
{
	real_t
		opening_agl_threshold = 0.85,
		time_delta = 0.04,
		range[3][2] = {
			{-600, 600},
			{-600, 600},
			{-600, 600}
		};
	int mpi_chunk_size = 100;
}
using namespace conf;

int main(int argc, char **argv)
{
	parse_opt(argc, argv);
	if (need_validate)
		validate(body, nbody);
	printf("worker=%s time_delta=%.3lf opening_agl_threshold=%.3lf mpi_chunk_size=%d\n",
			worker->get_name(), time_delta, opening_agl_threshold, mpi_chunk_size);
	server->run(*worker);
	delete server;
	delete worker;
	delete []body;
	profile_print(stdout);
}

void parse_opt(int &argc, char **&argv)
{
	static const struct option long_opt[] = {
		{"worker", required_argument, NULL, 'w'},
		{"server", required_argument, NULL, 's'},
		{"nthread", required_argument, NULL, 'n'},
		{"input", required_argument, NULL, 'i'},
		{"no-validate", required_argument, NULL, 'N'},
		{"frame-time", required_argument, NULL, 't'},
		{"opening-angle", required_argument, NULL, 'a'},
		{"mpi-chunk-size", required_argument, NULL, 'c'},
		{"help", no_argument, NULL, 'h'}
	};
	nthread = get_nrcpu();
	for (; ;)
	{
		int c = getopt_long(argc, argv, "w:s:n:i:Nt:a:c:h", long_opt, NULL);
		switch (c)
		{
			case -1:
				if (!body)
					error_exit("no input data");
				if (!worker)
					worker = new PthreadPreforkWorker(nthread, body, nbody, spt_factory);
				if (!server)
					server = new TCPServer(DEFAULT_PORT, 1.5 / time_delta);
				return;
			case 'w':
				if (!body)
					error_exit("no input data for constructing worker");
				worker = make_worker(optarg, argc, argv);
				break;
			case 's':
				server = make_server(optarg);
				break;
			case 'n':
				if (sscanf(optarg, "%d", &nthread) != 1)
					error_exit("invalid argument for --nthread: %s", optarg);
				break;
			case 'i':
				if (body)
					error_exit("duplicate data provider");
				read_input(optarg);
				break;
			case 'N':
				need_validate = false;
				break;
			case 't':
				if (sscanf(optarg, "%lf", &time_delta) != 1)
					error_exit("invalid argument for --frame-time: %s", optarg);
				break;
			case 'a':
				if (sscanf(optarg, "%lf", &opening_agl_threshold) != 1)
					error_exit("invalid argument for --opening-angle: %s", optarg);
				break;
			case 'c':
				if (sscanf(optarg, "%d", &mpi_chunk_size) != 1)
					error_exit("invalid argument for --mpi-chunk-size: %s", optarg);
				break;
			default:
				usage(argv[0]);
				exit(c == '?');
		}
	}
}

void validate(const Body *body, int nbody)
{
	for (int i = 0; i < nbody; i ++)
	{
		for (int j = i + 1; j < nbody; j ++)
			if (body[i].chk_overlap(body[j]))
				error_exit("%d and %d overlaps", i, j);
		real_t r = body[i].radius;
		for (int j = 0; j < 3; j ++)
		{
			real_t p = body[i].center.comp(j);
			if (p - r < range[j][0] || p + r > range[j][1])
				error_exit("%d out of boundary", j);
		}
	}
}

WorkerBase* make_worker(const char *spec, int &argc, char **&argv)
{
	if (!strcmp(spec, "seq"))
		return new SeqWorker(body, nbody, spt_factory);
	if (!strcmp(spec, "pthread"))
		return new PthreadWorker(nthread, body, nbody, spt_factory);
	if (!strcmp(spec, "pthreadpf"))
		return new PthreadPreforkWorker(nthread, body, nbody, spt_factory);
	if (!strcmp(spec, "omp"))
		return new OMPWorker(nthread, body, nbody, spt_factory);
	if (!strcmp(spec, "mpi"))
		return new MPIWorker(argc, argv, body, nbody, spt_factory);
	error_exit("invalid worker: %s", spec);
}

ServerBase* make_server(const char *spec)
{
	if (!strncmp(spec, "tcp", 3))
	{
		int len = strlen(spec), port = DEFAULT_PORT;
		if (len > 3)
		{
			if (spec[3] != ':')
				error_exit("invalid format for tcp server");
			spec += 4;
			if (sscanf(spec, "%d", &port) != 1)
				error_exit("invalid port: %s", spec);
		}
		return new TCPServer(port, 1.5 / time_delta);
	}
	if (!strncmp(spec, "null", 4))
	{
		spec += 4;
		int niter;
		if (spec[0] != ':' || sscanf(spec + 1, "%d", &niter) != 1)
			error_exit("invalid format for null server: %s", spec - 4);
		return new NullServer(niter);
	}
	if (!strcmp(spec, "stdio"))
		return new STDIOServer();
	error_exit("invalid server: %s", spec);
}

void read_input(const char *fpath)
{
	ifstream fin(fpath);
	fin >> nbody;
	body = new Body[nbody];
	for (int i = 0; i < nbody; i ++)
		fin >> body[i].center >> body[i].velocity >> body[i].radius >>
			body[i].mass;
	if (!fin.good())
		error_exit("invalid input file format");
}

void usage(const char *name)
{
	fprintf(stderr, "usage: %s -i <filename>|-r <size> [options]\n", name);
	fputs("options:\n"
			"  --worker, -w <worker>         one of seq, pthread, pthreadpf, omp, mpi\n"
			"  --server, -s <server>         one of tcp[:port], null:niter, stdio\n"
			"  --nthread, -n <number>        number of threads for pthread and omp\n"
			"  --input, -i <filename>        input filename\n"
			"  --no-validate, -N             do not validate input date\n"
			"  --frame-time, -t <nsec>       time for each frame\n"
			"  --opening-angle, -a <agl>     opening angle threshold\n"
			"  --mpi-chunk-size, -c <size>   chunk size for MPI task distribution\n",
			stderr);
}

// vim: syntax=cpp11.doxygen

