/*
 * $File: main.cc
 * $Date: Fri Aug 17 09:14:14 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "frogriver.hh"

#include <gtkmm/main.h>

int main(int argc, char **argv)
{
	Gtk::Main kit(argc, argv);
	FrogRiverWindow win;
	Gtk::Main::run(win);
}

// vim: syntax=cpp11.doxygen

