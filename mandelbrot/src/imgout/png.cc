/*
 * $File: png.cc
 * $Date: Fri Aug 24 23:20:33 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "imgout/png.hh"
#include "config.hh"

#include <png.h>

#include <setjmp.h>
#include <cstdio>
#include <cstdlib>
#include <stdint.h>

PNGImgout::PNGImgout(const char *fpath):
	m_buf(new Point[conf::res_x * conf::res_y]),
	m_buf_pos(m_buf), m_fpath(fpath)
{
}

PNGImgout::~PNGImgout()
{
	save();
	delete []m_buf;
}

void PNGImgout::draw(const Point *buf, size_t size)
{
	Point *p;
	{
		LOCK(m_lock_buf);
		p = m_buf_pos;
		m_buf_pos += size;
	}
	memcpy(p, buf, sizeof(Point) * size);
}
    
// reference: http://www.lemoda.net/c/write-png
void PNGImgout::save()
{
    /*
	 * The following number is set by trial and error only. I cannot see where
	 * it it is documented in the libpng manual.
	 */
    int pixel_size = 3, depth = 8;
    
    FILE *fp = fopen(m_fpath.c_str(), "wb");
    if (!fp)
		error_exit("failed to open %s: %m\n", m_fpath.c_str());

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
			NULL, NULL, NULL);
    if (!png_ptr)
		error_exit("faied to create png write structure\n");
    
    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
		error_exit("faied to create png info structure\n");
    

    /* Set up error handling. */
    if (setjmp(png_jmpbuf(png_ptr))) 
		error_exit("png error\n");
    
	png_set_IHDR(png_ptr,
			info_ptr,
			conf::res_x,
			conf::res_y,
			depth,
			PNG_COLOR_TYPE_RGB,
			PNG_INTERLACE_NONE,
			PNG_COMPRESSION_TYPE_DEFAULT,
			PNG_FILTER_TYPE_DEFAULT);
    
    /* Initialize rows of PNG. */
    png_byte **row_pointers = static_cast<png_byte**>(png_malloc(png_ptr,
				conf::res_y * sizeof (png_byte *)));
    for (int y = 0; y < conf::res_y; y ++)
	{
		row_pointers[y]= static_cast<png_byte*>(png_malloc(png_ptr,
				sizeof(uint8_t) * conf::res_x * pixel_size));
	}
	Pixel *colormap = load_colormap();
	for (Point *pt = m_buf; pt < m_buf_pos; pt ++)
	{
		png_byte *t = &row_pointers[pt->y][pt->x * 3];
		Pixel &p = colormap[pt->niter];
		*(t ++) = p.r;
		*(t ++) = p.g;
		*(t ++) = p.b;
	}
	delete []colormap;
    
    /* Write the image data to "fp". */
    png_init_io(png_ptr, fp);
    png_set_rows(png_ptr, info_ptr, row_pointers);
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    for (int y = 0; y < conf::res_y; y ++)
		png_free(png_ptr, row_pointers[y]);
	png_free(png_ptr, row_pointers);
}


// vim: syntax=cpp11.doxygen

