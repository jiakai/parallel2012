# -*- coding: utf-8 -*-
# $File: effi_niter.py
# $Date: Fri Aug 31 01:55:41 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency vs niter"""


from nbdplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers, size):
    baseline = filter(default_filter(worker = 'seq', size = size, niter = None), data)
    baseline = {i.niter: i for i in baseline}
    ax.set_ylabel('Efficiency')
    ax.set_xlabel("Number of Iterations")
    ax.set_xscale("log")
    cyc = get_line_cycler()
    nthread = None
    for wk in workers:
        d = filter(default_filter(worker = wk, nthread = None, size=size, niter
            = None), data)
        d.sort(cmp = lambda x, y: cmp(x.niter, y.niter))
        ax.plot([i.niter for i in d], [baseline[i.niter].tot_real / (i.tot_real *
            i.nthread_real) for i in d], next(cyc), label = i.worker)
        for i in d:
            if nthread is None:
                nthread = i.nthread_real
            assert nthread == i.nthread_real
    ax.set_title('({0} bodies, {1} threads)'.format(size, nthread))
    ax.legend()

def plot_realtime(data, ax, workers, size):
    ax.set_ylabel('Real Time [sec]')
    ax.set_xlabel("Number of Iterations")
    ax.set_yscale("log")
    ax.set_xscale("log")
    cyc = get_line_cycler()
    nthread = None
    for wk in workers:
        d = filter(default_filter(worker = wk, nthread = None, size=size, niter
            = None), data)
        d.sort(cmp = lambda x, y: cmp(x.niter, y.niter))
        ax.plot([i.niter for i in d], [i.tot_real for i in d], next(cyc), label = i.worker)
        for i in d:
            if nthread is None:
                nthread = i.nthread_real
            assert nthread == i.nthread_real
    ax.set_title('({0} bodies, {1} threads)'.format(size, nthread))
    ax.legend()

def plot(data):
    fig = plt.figure()
    fig.suptitle('Efficiency and Real Time Comparison for Different #Iters')
    plot_efficiency(data, fig.add_subplot(221), ['pthread', 'pthreadpf',
        'omp'], 100)
    plot_efficiency(data, fig.add_subplot(222), ['mpi'], 3162)
    plot_realtime(data, fig.add_subplot(223), ['pthread', 'pthreadpf',
        'omp'], 100)
    plot_realtime(data, fig.add_subplot(224), ['mpi'], 3162)
    plt.savefig('effi-niter.pdf')

