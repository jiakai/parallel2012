/*
 * $File: asyncX11.hh
 * $Date: Sat Aug 25 13:06:01 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IMGOUT_ASYNCX11_
#define _HEADER_IMGOUT_ASYNCX11_

#include "imgout/base.hh"
#include "utils.hh"

#include <pthread.h>

#include <X11/Xlib.h>

/*!
 * \brief asynchronous output to an X11 window
 */
class AsyncX11Imgout: public ImgoutBase
{
	bool m_wait_ret, m_enter_wait;

	Display *m_display;
	Window m_win;

	Point *m_queue;
	int m_qh, m_qt;

	GC *m_gc;

	MutexLock m_lock_qh, m_lock_qt, m_lock_x11;
	pthread_t m_pid_draw, m_pid_xevent;

	void init_gc();
	static void* thread_draw(void *self_);
	static void* thread_xevent(void *self_);
	void get_xevent(XEvent &ev);

	public:
		AsyncX11Imgout(const char *window_name, const char *disp_name = NULL);
		~AsyncX11Imgout();
		void draw(const Point *buf, size_t size);
		void init();
		bool wait();
};

#endif // _HEADER_IMGOUT_ASYNCX11_

// vim: syntax=cpp11.doxygen

