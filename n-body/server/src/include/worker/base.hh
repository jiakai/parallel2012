/*
 * $File: base.hh
 * $Date: Wed Aug 29 17:49:48 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_BASE_
#define _HEADER_WORKER_BASE_

#include "geobase.hh"
#include "sptree/base.hh"

class WorkerBase
{
	public:
		WorkerBase(Body *body, int nbody, SPTreeFactoryBase &tree_factory);
		virtual ~WorkerBase();

		virtual void next_step() = 0;
		virtual const char* get_name() = 0;

		Body* get_body_buf()
		{ return m_body_num2ins; }

		int get_nr_body() const
		{ return m_body.size(); }

	protected:
		Body *m_body_num2ins;
		bodypbuf_t m_body;
		SPTreeFactoryBase &m_tree_factory;

		/*!
		 * \brief data associated with each body
		 */
		struct BodyData
		{
			Point ctnew; ///< center after collision
			Vector vnew; ///< velocity after collision
		};
		BodyData *m_body_data;
		Body** m_collision;
		int m_ncollision;

		/*!
		 * \brief update velocity by gravity
		 */
		virtual void update_gravity(Body &body, SPTreeBase *root);

		/*!
		 * \brief add collision results to m_collision
		 */
		virtual void update_collision(Body &body, SPTreeBase *root);

		/*!
		 * \brief update position by velocity and check for boundries
		 */
		virtual void update_pos(Body &body);

		virtual void log_collision(Body *b)
		{ m_collision[m_ncollision ++] = b; }


		void chg_body_buf(Body *body, int nbody);

	private:
		void alloc_bufs(Body *body, int nbody);
		void destroy_bufs();
};

#endif // _HEADER_WORKER_BASE_

// vim: syntax=cpp11.doxygen

