/*
 * $File: profile.cc
 * $Date: Tue Aug 28 14:16:22 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "profile.hh"
#include "utils.hh"

#include <map>
#include <string>
using namespace std;

static map<string, double> cputime, realtime;
static HWTimer timer;

void profile_start(const char *name)
{
	cputime[name] -= get_cputime();
	realtime[name] -= timer.get_sec();
}

void profile_end(const char *name)
{
	cputime[name] += get_cputime();
	realtime[name] += timer.get_sec();
}

void profile_print(FILE *fout)
{
	ITER_STD(cputime, i)
		fprintf(fout, "%s: cpu=%.4lf real=%.4lf\n", i->first.c_str(), i->second,
				realtime[i->first]);
}


// vim: syntax=cpp11.doxygen

