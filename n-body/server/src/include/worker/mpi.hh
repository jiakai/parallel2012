/*
 * $File: mpi.hh
 * $Date: Wed Aug 29 17:14:26 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */
#ifndef _HEADER_WORKER_MPI_
#define _HEADER_WORKER_MPI_

#include "worker/base.hh"

#include <list>

class MPIWorker: public WorkerBase
{
	int m_rank, m_nslave, m_nbody;

	class MPIPackSize;
	class MPIPacked;
	class BodyPosBuf;

	MPIPacked *m_packed;
	BodyPosBuf *m_bodyposbuf;
	int *m_collision_numbuf;

	struct TaskRecord
	{
		int begin, end;
		TaskRecord(int b, int e):
			begin(b), end(e)
		{}
	};
	std::list<TaskRecord>* m_tkrec; // only used by master

	void init_master();
	void init_slave();

	void finalize_master();
	void finalize_slave();

	void ns_master();
	void ns_slave();

	void update_collision_mpi();
	void master_scatter_task();
	void master_bcast_exit();

	public:
		MPIWorker(int &argc, char **& argv, Body *body, int nbody, SPTreeFactoryBase &tree_factory);
		~MPIWorker();

		bool is_master()
		{ return !m_rank; }

		const char* get_name()
		{ return "MPI"; }

		// for slaves, this should be called only once
		void next_step();
};

#endif // _HEADER_WORKER_MPI_

// vim: syntax=cpp11.doxygen

