/*
 * $File: pthread.hh
 * $Date: Sun Sep 02 16:35:16 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_PTHREAD_
#define _HEADER_WORKER_PTHREAD_

#include "worker/base.hh"
#include "utils.hh"

class PthreadWorker: public WorkerBase
{
	int m_nthread;
	pthread_t *m_thread_id;
	update_arg_t *m_thread_arg;

	static void* thread_wrapper(void*arg);

	public:
		PthreadWorker(int nthread, int w, int h, real_t t0);
		real_t **next_step(BoundaryBase &bd_cond);
		~PthreadWorker();
};

#endif // _HEADER_WORKER_PTHREAD_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

