#!/usr/bin/env gnuplot
# $File: realtime-nproc-qsort.plt
# $Date: Tue Aug 14 18:44:18 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale y
set title "Quick Sort"
set xlabel "# Processes"
set ylabel "Real Run Time [sec]"
set xtics 12, 12
set xrange [10:175]

set terminal postscript enhanced color
set output "realtime-nproc-qsort.eps"

set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

cd "../data/qsort-ext"
plot \
	"size10000000" using 2:4 title "n=1e7" , \
	"size50000000" using 2:4 title "n=5e7" , \
	"size100000000" using 2:4 title "n=1e8" , \
	"size500000000" using 2:4 title "n=5e8" , \
	"size1000000000" using 2:4 title "n=1e9" , \
	"size5000000000" using 2:4 title "n=5e9" ls 6, \
	x < 132 ? 3.235805259646546 * (log(1e9) - log(x)) : 1/0 \
		title "k*n(log(n)-log(m))"
