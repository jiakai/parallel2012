/*
 * $File: sort.hh
 * $Date: Tue Aug 14 18:32:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_SORT_
#define _HEADER_SORT_

#include <cstddef>

/*!
 * \brief definition for callback functions used by sort
 */
class CallBackBase
{
	public:
		virtual void sort(int *begin, int *end) = 0;
		virtual size_t get_data_size() = 0;
		virtual size_t read_data(int *dest, size_t len) = 0;
		virtual void write_data(const int *src, size_t size) = 0;

		virtual ~CallBackBase() {}
};

void sort(int rank, int comm_size, CallBackBase &callback);

#endif // _HEADER_SORT_

// vim: syntax=cpp11.doxygen

