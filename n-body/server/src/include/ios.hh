/*
 * $File: ios.hh
 * $Date: Sun Aug 26 19:23:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IOS_
#define _HEADER_IOS_

#include "geobase.hh"
#include <iostream>

std::istream& operator >> (std::istream& instr, Vector &v);
std::ostream& operator << (std::ostream& outstr, const Vector &v);

#endif // _HEADER_IOS_

// vim: syntax=cpp11.doxygen

