#!/usr/bin/env gnuplot
# $File: rt-fixnproc-bubble.plt
# $Date: Wed Aug 15 23:04:01 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale x
set logscale y
set title "Real Run Time Comparison (Bubble Sort, nproc=60)"
set xlabel "Data Size"
set ylabel "Real Run Time [sec]"
set y2label "Decrease/Original" tc lt 3

set xrange[900:5e7]
set y2range[-1:1]
set y2tics 0 0.2 nomirror tc lt 3

set terminal postscript enhanced color
set output "rt-fixnproc-bubble.eps"

set style data linespoints

cd "../../data"
plot \
	"bubble-ext/nproc60" using 1:4 title "original", \
	"optimized/bubble-ext/nproc60" using 1:4 title "optimized", \
	"cmp-bubble-nproc60" using 1:(1-$9/$4) title "decrease/original" axes x1y2, \
	0 notitle lt 3 axes x1y2
