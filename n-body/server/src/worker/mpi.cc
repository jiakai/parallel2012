/*
 * $File: mpi.cc
 * $Date: Wed Aug 29 23:36:33 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include <mpi.h>
// intel mpi compains that mpi.h should be included before stdio.h
#include <algorithm>

#include "worker/mpi.hh"
#include "config.hh"
#include "profile.hh"



// first bcast nbody and conf::time_delta + (mass, radius) for each body
// in each iteration, first bcast (center, velocity) for each body
namespace tag
{
	enum _tag_t {
		QUERY_TASK = 23,
			/*
			 * slave -> master
			 * int[1] {<slave number>}
			 */
		TASK_REQUST,
			/*
			 * master -> slave
			 * int[2] {<begin number>, <end number>} or
			 * int[1] {<control command>}
			 */
		COLLISION_RST,
			/*
			 * slave -> master
			 * <packed> {<int:ncollision>, <int:number>*n, <double:c,v>*n}
			 *
			 * then bcast from master in the same format
			 */
		GRAVITY_RST,
			/*
			 * slave -> master
			 * double[] {<c,v> * m}
			 */
	};
}
static const int
	CTL_ALLFINISH = -1,
	CTL_EXIT = -2;

// f{{{ MPI function wrappers

static inline void recv(void *buf, int len, int from, tag::_tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Recv(buf, len, datatype, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

static inline int recv_withsender(void *buf, int len, int from, tag::_tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Status st;
	MPI_Recv(buf, len, datatype, from, tag, MPI_COMM_WORLD, &st);
	return st.MPI_SOURCE;
}

static inline int recv_int(int from, tag::_tag_t tag)
{
	int n;
	MPI_Recv(&n, 1, MPI_INTEGER, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	return n;
}

static inline void send(const void *buf, int len, int to, tag::_tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Send((void*)(buf), len, datatype, to, tag, MPI_COMM_WORLD);
}

static inline void send_int(int val, int to, tag::_tag_t tag)
{
	MPI_Send(&val, 1, MPI_INTEGER, to, tag, MPI_COMM_WORLD);
}

static inline void bcast_int(int &val, int root)
{
	MPI_Bcast(&val, 1, MPI_INTEGER, root, MPI_COMM_WORLD);
}

static inline void bcast_double_buf(double *buf, int cnt, int root)
{
	MPI_Bcast(buf, cnt, MPI_DOUBLE, root, MPI_COMM_WORLD);
}

class MPIWorker::MPIPackSize
{
	mutable int m_size;
	public:
		MPIPackSize():
			m_size(0)
		{}

		const MPIPackSize& add(const MPI_Datatype &type, int cnt) const
		{
			int s;
			MPI_Pack_size(cnt, type, MPI_COMM_WORLD, &s);
			m_size += s;
			return *this;
		}
		int size() const
		{ return m_size; }
};

class MPIWorker::MPIPacked
{
	char *m_buf;
	int m_pos, m_size;

	public:
		MPIPacked(int size):
			m_buf(new char[size]),
			m_pos(0), m_size(size)
		{}
		~MPIPacked()
		{ delete []m_buf; }

		void reset()
		{ m_pos = 0; }

		void send(int to, tag::_tag_t tag)
		{
			MPI_Send(m_buf, m_pos, MPI_PACKED, to, tag, MPI_COMM_WORLD);
			m_pos = 0;
		}

		void recv(int from, tag::_tag_t tag)
		{
			MPI_Recv(m_buf, m_size, MPI_PACKED, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			m_pos = 0;
		}

		void bcast_send(int root)
		{
			bcast_int(m_pos, root);
			MPI_Bcast(m_buf, m_pos, MPI_PACKED, root, MPI_COMM_WORLD);
			m_pos = 0;
		}

		void bcast_recv(int root)
		{
			int size;
			bcast_int(size, root);
			MPI_Bcast(m_buf, size, MPI_PACKED, root, MPI_COMM_WORLD);
			m_pos = 0;
		}

		void write(const void *buf, int cnt, const MPI_Datatype &type)
		{
			MPI_Pack((void*)buf, cnt, type, m_buf, m_size, &m_pos, MPI_COMM_WORLD);
		}

		void read(void *buf, int cnt, const MPI_Datatype &type)
		{
			MPI_Unpack(m_buf, m_size, &m_pos, buf, cnt, type, MPI_COMM_WORLD);
		}

		void write_int(int val)
		{ write(&val, 1, MPI_INTEGER); }

		int read_int()
		{ int v; read(&v, 1, MPI_INTEGER); return v; }
};

// f}}}

class MPIWorker::BodyPosBuf
// f{{{
{
	double *m_buf;
	int m_pos;
	public:
		static const int ITEM_SIZE;

		BodyPosBuf(int nbody):
			m_buf(new double[nbody * ITEM_SIZE]),
			m_pos(0)
		{}

		~BodyPosBuf()
		{ delete []m_buf; }

		void reset()
		{ m_pos = 0; }

		double* get_buf()
		{ return m_buf; }

		int get_size()
		{ return m_pos; }

		void write(const Body &b)
		{
			double *t = &m_buf[m_pos];
			m_pos += ITEM_SIZE;
			*(t ++) = b.center.x;
			*(t ++) = b.center.y;
			*(t ++) = b.center.z;
			*(t ++) = b.velocity.x;
			*(t ++) = b.velocity.y;
			*(t ++) = b.velocity.z;
		}

		void read(Body &b) 
		{
			const double *s = &m_buf[m_pos];
			m_pos += ITEM_SIZE;
			b.center.x = *(s ++);
			b.center.y = *(s ++);
			b.center.z = *(s ++);
			b.velocity.x = *(s ++);
			b.velocity.y = *(s ++);
			b.velocity.z = *(s ++);
		}
};
const int MPIWorker::BodyPosBuf::ITEM_SIZE = 6;
// f}}}

MPIWorker::MPIWorker(int &argc, char **&argv,
		Body *body, int nbody, SPTreeFactoryBase &tree_factory):
	WorkerBase(NULL, 0, tree_factory), m_nbody(nbody)
{
	m_body_num2ins = body;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &m_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &m_nslave);
	m_nslave --;

	if (m_rank)
		init_slave();
	else
	{
		chg_body_buf(body, nbody);
		init_master();
	}
	m_bodyposbuf = new BodyPosBuf(m_nbody);
	m_packed = new MPIPacked(MPIPackSize().
			add(MPI_INTEGER, 1).
			add(MPI_INTEGER, m_nbody).
			add(MPI_DOUBLE, m_nbody * BodyPosBuf::ITEM_SIZE).
			size());
	m_collision_numbuf = new int[m_nbody];
}

MPIWorker::~MPIWorker()
{
	if (m_rank)
		finalize_slave();
	else
		finalize_master();
	delete m_bodyposbuf;
	delete m_packed;
	delete m_collision_numbuf;
	MPI_Finalize();
}

void MPIWorker::next_step()
{
	if (m_rank)
		ns_slave();
	else
		ns_master();
}

void MPIWorker::ns_master()
{
	m_bodyposbuf->reset();
	for (int i = 0; i < m_nbody; i ++)
		m_bodyposbuf->write(m_body_num2ins[i]);
	bcast_double_buf(m_bodyposbuf->get_buf(), m_nbody * BodyPosBuf::ITEM_SIZE, 0);

	// f{{{ collision
	master_scatter_task();
	m_ncollision = 0;
	m_bodyposbuf->reset();
	for (int i = 0; i < m_nslave; i ++)
	{
		m_packed->recv(MPI_ANY_SOURCE, tag::COLLISION_RST);
		int n = m_packed->read_int();
		m_packed->read(m_collision_numbuf + m_ncollision, n, MPI_INTEGER);
		m_packed->read(m_bodyposbuf->get_buf() + m_ncollision *
				BodyPosBuf::ITEM_SIZE, n * BodyPosBuf::ITEM_SIZE, MPI_DOUBLE);
		m_ncollision += n;
	}
	m_packed->reset();
	m_packed->write_int(m_ncollision);
	m_packed->write(m_collision_numbuf, m_ncollision, MPI_INTEGER);
	m_packed->write(m_bodyposbuf->get_buf(), m_ncollision * BodyPosBuf::ITEM_SIZE, MPI_DOUBLE);
	m_packed->bcast_send(0);
	update_collision_mpi();
	// f}}}

	// f{{{ gravity
	master_scatter_task();
	for (int i = 0; i < m_nslave; i ++)
	{
		m_bodyposbuf->reset();
		int rk = recv_withsender(m_bodyposbuf->get_buf(), m_nbody *
				BodyPosBuf::ITEM_SIZE, MPI_ANY_SOURCE, tag::GRAVITY_RST,
				MPI_DOUBLE);
		ITER_STD(m_tkrec[rk], t)
			for (int j = t->begin; j < t->end; j ++)
				m_bodyposbuf->read(m_body_num2ins[j]);
	}
	// f}}}
	ITER_STD(m_body, p)
		update_pos(**p);
}

void MPIWorker::master_scatter_task()
{
	for (int i = 1; i <= m_nslave; i ++)
		m_tkrec[i].clear();
	int task_pos = m_nbody, nslave_remain = m_nslave;
	while (nslave_remain)
	{
		int slnum = recv_int(MPI_ANY_SOURCE, tag::QUERY_TASK);
		if (!task_pos)
		{
			send_int(CTL_ALLFINISH, slnum, tag::TASK_REQUST);
			nslave_remain --;
			continue;
		}
		int task[2] = {std::max(task_pos - conf::mpi_chunk_size, 0), task_pos};
		task_pos = task[0];
		send(task, 2, slnum, tag::TASK_REQUST, MPI_INTEGER);
		m_tkrec[slnum].push_back(TaskRecord(task[0], task[1]));
	}
}

void MPIWorker::ns_slave()
{
	for (; ;)
	{
		m_bodyposbuf->reset();
		bcast_double_buf(m_bodyposbuf->get_buf(), m_nbody * BodyPosBuf::ITEM_SIZE, 0);
		for (int i = 0; i < m_nbody; i ++)
			m_bodyposbuf->read(m_body_num2ins[i]);

		send_int(m_rank, 0, tag::QUERY_TASK);
		int task[2];
		recv(task, 2, 0, tag::TASK_REQUST, MPI_INTEGER);
		if (task[0] == CTL_EXIT)
			break;

		sptptr_t tree = m_tree_factory.mktree(m_body);
		char logname[256];

		// f{{{ collision
		sprintf(logname, "slave%d_collision", m_rank);
		profile_start(logname);
		m_ncollision = 0;
		while (task[0] != CTL_ALLFINISH)
		{
			for (int i = task[0]; i < task[1]; i ++)
				update_collision(m_body_num2ins[i], tree.get_ptr());
			send_int(m_rank, 0, tag::QUERY_TASK);
			recv(task, 2, 0, tag::TASK_REQUST, MPI_INTEGER);
		}
		m_packed->reset();
		m_bodyposbuf->reset();
		m_packed->write_int(m_ncollision);
		for (int i = 0; i < m_ncollision; i ++)
		{
			Body &b = *m_collision[i];
			m_collision_numbuf[i] = b.num;
			BodyData &d = m_body_data[b.num];
			b.center = d.ctnew;
			b.velocity = d.vnew;
			m_bodyposbuf->write(b);
		}
		m_packed->write(m_collision_numbuf, m_ncollision, MPI_INTEGER);
		m_packed->write(m_bodyposbuf->get_buf(), m_ncollision * BodyPosBuf::ITEM_SIZE, MPI_DOUBLE);
		m_packed->send(0, tag::COLLISION_RST);

		// reading from master
		m_packed->bcast_recv(0);
		m_ncollision = m_packed->read_int();
		m_packed->read(m_collision_numbuf, m_ncollision, MPI_INTEGER);
		m_packed->read(m_bodyposbuf->get_buf(), m_ncollision * BodyPosBuf::ITEM_SIZE, MPI_DOUBLE);
		update_collision_mpi();
		profile_end(logname);
		// f}}}

		// f{{{ gravity
		sprintf(logname, "slave%d_gravity", m_rank);
		profile_start(logname);
		m_bodyposbuf->reset();
		for (; ;)
		{
			int task[2];
			send_int(m_rank, 0, tag::QUERY_TASK);
			recv(task, 2, 0, tag::TASK_REQUST, MPI_INTEGER);
			if (task[0] == CTL_ALLFINISH)
				break;
			for (int i = task[0]; i < task[1]; i ++)
			{
				Body &b = m_body_num2ins[i];
				update_gravity(b, tree.get_ptr());
				m_bodyposbuf->write(b);
			}
		}
		send(m_bodyposbuf->get_buf(), m_bodyposbuf->get_size(), 0,
				tag::GRAVITY_RST, MPI_DOUBLE);
		profile_end(logname);
		// f}}}
	}
}

void MPIWorker::master_bcast_exit()
{
	bcast_double_buf(m_bodyposbuf->get_buf(), m_nbody * BodyPosBuf::ITEM_SIZE, 0);
	for (int i = 0; i < m_nslave; i ++)
	{
		int slnum = recv_int(MPI_ANY_SOURCE, tag::QUERY_TASK);
		send_int(CTL_EXIT, slnum, tag::TASK_REQUST);
	}
}

void MPIWorker::init_master()
{
	if (!m_nslave)
		error_exit("too few proceses");
	bcast_int(m_nbody, 0);
	Body **b = m_body.begin();
	double *tmp = new double[1 + m_nbody * 2], *ptr = tmp;
	*(ptr ++) = conf::time_delta;
	for (int i = 0; i < m_nbody; i ++)
	{
		*(ptr ++) = b[i]->mass;
		*(ptr ++) = b[i]->radius;
	}
	bcast_double_buf(tmp, 1 + m_nbody * 2, 0);
	delete []tmp;
	m_tkrec = new std::list<TaskRecord>[m_nslave + 1];
}

void MPIWorker::finalize_master()
{
	master_bcast_exit();
	delete []m_tkrec;
}

void MPIWorker::init_slave()
{
	bcast_int(m_nbody, 0);
	chg_body_buf(new Body[m_nbody], m_nbody);

	double *tmp = new double[1 + m_nbody * 2], *ptr = tmp;
	bcast_double_buf(tmp, 1 + m_nbody * 2, 0);
	conf::time_delta = *(ptr ++);
	for (int i = 0; i < m_nbody; i ++)
	{
		m_body_num2ins[i].mass = *(ptr ++);
		m_body_num2ins[i].radius = *(ptr ++);
	}
	delete []tmp;
}

void MPIWorker::finalize_slave()
{
	delete []m_body_num2ins;
}

void MPIWorker::update_collision_mpi()
{
	m_bodyposbuf->reset();
	for (int i = 0; i < m_ncollision; i ++)
	{
		Body &b = m_body_num2ins[m_collision_numbuf[i]];
		m_bodyposbuf->read(b);
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

