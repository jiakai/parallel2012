/*
 * $File: mpi.cc
 * $Date: Tue Sep 04 07:00:48 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/mpi.hh"
#include <mpi.h>
#include <cstring>

namespace tag
{
	enum tag_t {
		SYNC = 23
	};
}

static const int 
	CTL_CONTINUE = -1,
	CTL_EXIT = -2;

// f{{{ MPI function wrappers

static inline void recv(void *buf, int len, int from, tag::tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Recv(buf, len, datatype, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

static inline int recv_withsender(void *buf, int len, int from, tag::tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Status st;
	MPI_Recv(buf, len, datatype, from, tag, MPI_COMM_WORLD, &st);
	return st.MPI_SOURCE;
}

static inline int recv_int(int from, tag::tag_t tag)
{
	int n;
	MPI_Recv(&n, 1, MPI_INTEGER, from, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	return n;
}

static inline void send(const void *buf, int len, int to, tag::tag_t tag,
		const MPI_Datatype &datatype)
{
	MPI_Send((void*)(buf), len, datatype, to, tag, MPI_COMM_WORLD);
}

static inline void send_int(int val, int to, tag::tag_t tag)
{
	MPI_Send(&val, 1, MPI_INTEGER, to, tag, MPI_COMM_WORLD);
}

static inline void bcast_int(int &val, int root)
{
	MPI_Bcast(&val, 1, MPI_INTEGER, root, MPI_COMM_WORLD);
}

static inline void bcast_double_buf(double *buf, int cnt, int root)
{
	MPI_Bcast(buf, cnt, MPI_DOUBLE, root, MPI_COMM_WORLD);
}

// f}}}

MPIWorker::MPIWorker(int &argc, char **&argv, int w, int h, real_t t0):
	WorkerBase(w, h, t0)
{
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &m_rank);
	MPI_Comm_size(MPI_COMM_WORLD, &m_nworker);

	m_worker_range = new update_arg_t[m_nworker];
	divide_task(m_worker_range, m_nworker);
	m_self_range = &m_worker_range[m_rank];
	m_send_buf = new real_t[m_self_range->height * w];

	if (is_master())
		init_master();
	else
		init_slave();
}

void MPIWorker::init_master()
{
	m_recv_buf = new real_t[m_height * m_width];
	m_displs = new int[m_nworker];
	m_recv_cnt = new int[m_nworker];
	for (int i = 0; i < m_nworker; i ++)
	{
		m_displs[i] = m_worker_range[i].y0 * m_width;
		m_recv_cnt[i] = m_worker_range[i].height * m_width;
	}
}

void MPIWorker::init_slave()
{
}

MPIWorker::~MPIWorker()
{
	if (is_master())
		finalize_master();
	else
		finalize_slave();
	MPI_Finalize();
	delete []m_worker_range;
	delete []m_send_buf;
}

void MPIWorker::finalize_master()
{
	int signal = CTL_EXIT;
	bcast_int(signal, 0);
	delete []m_recv_buf;
	delete []m_displs;
}

void MPIWorker::finalize_slave()
{
}

real_t **MPIWorker::next_step(BoundaryBase &bd_cond)
{
	m_bd_cond = &bd_cond;
	if (is_master())
		ns_master();
	else
		ns_slave();
	return m_buf[m_cur_buf];
}

void MPIWorker::ns_master()
{
	int signal = CTL_CONTINUE;
	bcast_int(signal, 0);
	calc_and_sync();
	real_t *src = m_recv_buf;
	for (int y = 1; y < m_height - 1; y ++)
		memcpy(m_buf[m_cur_buf][y], src + y * m_width,
				sizeof(real_t) * m_width);
}

void MPIWorker::ns_slave()
{
	for (; ;)
	{
		int signal;
		bcast_int(signal, 0);
		if (signal == CTL_EXIT)
			return;
		calc_and_sync();
	}
}

void MPIWorker::calc_and_sync()
{
	real_t **src = m_buf[m_cur_buf],
		   **dest = m_buf[m_cur_buf ^= 1];
	m_self_range->src = src;
	m_self_range->dest = dest;
	update(*m_self_range);
	m_bd_cond->update(dest, m_width, m_height);
	if (m_rank & 1)
	{
		sync_left();
		sync_right();
	} else
	{
		sync_right();
		sync_left();
	}
	for (int y = m_self_range->y0, yt = y + m_self_range->height, t = 0;
			y < yt; y ++, t ++)
		memcpy(&m_send_buf[t * m_width], m_buf[m_cur_buf][y],
				sizeof(real_t) * m_width);
	MPI_Gatherv(
			m_send_buf, m_self_range->height * m_width, REAL_T_MPI,
			m_recv_buf, m_recv_cnt,
			m_displs, REAL_T_MPI,
			0, MPI_COMM_WORLD);
}

void MPIWorker::sync_left()
{
	if (!m_rank)
		return;
	send(m_buf[m_cur_buf][m_self_range->y0], m_width,
			m_rank - 1, tag::SYNC, REAL_T_MPI);
	recv(m_buf[m_cur_buf][m_self_range->y0 - 1], m_width,
			m_rank - 1, tag::SYNC, REAL_T_MPI);
}

void MPIWorker::sync_right()
{
	if (m_rank == m_nworker - 1)
		return;
	int y1 = m_self_range->y0 + m_self_range->height;
	recv(m_buf[m_cur_buf][y1], m_width,
			m_rank + 1, tag::SYNC, REAL_T_MPI);
	send(m_buf[m_cur_buf][y1 - 1], m_width,
			m_rank + 1, tag::SYNC, REAL_T_MPI);
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

