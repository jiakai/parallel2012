#!/usr/bin/env gnuplot
# $File: qsort-speedup.plt
# $Date: Mon Aug 13 23:37:30 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set autoscale
set notitle
set xlabel "# processes"
set ylabel "Speedup Factor"

set terminal postscript enhanced color
set output "qsort-speedup.eps"

plot [1:5000] 1/(1-log(x)/log(10000)) title "n=10000", \
		 1/(1-log(x)/log(100000)) title "n=100000", \
		 1/(1-log(x)/log(1000000)) title "n=1000000", \
		 1/(1-log(x)/log(1e9)) title "n=1e9"

