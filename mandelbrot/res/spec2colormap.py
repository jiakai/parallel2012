#!/usr/bin/env python
# -*- coding: utf-8 -*-
# $File: spec2colormap.py
# $Date: Thu Aug 23 11:39:31 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from PIL import Image

im = Image.open('spec.png')
width = im.size[0]
with open('colormap.txt', 'w') as f:
    print >> f, width
    for i in im.getdata():
        if not width:
            break
        width -= 1
        print >> f, i[0], i[1], i[2]

