/*
 * $File: border.cc
 * $Date: Tue Sep 04 06:57:14 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "boundary/border.hh"

#include <algorithm>

void BorderBoundary::update(real_t **buf, int width, int height)
{
	std::fill(buf[0], buf[0] + width, m_val);
	std::fill(buf[height - 1], buf[height - 1] + width, m_val);
	for (int i = 0; i < height; i ++)
		buf[i][0] = buf[i][width - 1] = m_val;
	buf[height / 2][width / 2] = m_val;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

