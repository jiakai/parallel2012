/*
 * $File: seq.cc
 * $Date: Fri Aug 31 10:17:00 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/seq.hh"
#include "profile.hh"

SeqWorker::SeqWorker(Body *body, int nbody, SPTreeFactoryBase &tree_factory):
	WorkerBase(body, nbody, tree_factory)
{
}

void SeqWorker::next_step()
{
	m_ncollision = 0;

	sptptr_t tree = m_tree_factory.mktree(m_body);

	profile_start("seq_gravity");
	ITER_STD(m_body, p)
		update_gravity(**p, tree.get_ptr());
	profile_end("seq_gravity");

	profile_start("seq_collision");
	ITER_STD(m_body, p)
		update_collision(**p, tree.get_ptr());
	for (int i = 0; i < m_ncollision; i ++)
	{
		Body &b = *m_collision[i];
		BodyData &d = m_body_data[b.num];
		b.center = d.ctnew;
		b.velocity = d.vnew;
	}
	profile_end("seq_collision");

	ITER_STD(m_body, p)
		update_pos(**p);
}


// vim: syntax=cpp11.doxygen

