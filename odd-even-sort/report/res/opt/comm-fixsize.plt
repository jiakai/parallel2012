#!/usr/bin/env gnuplot
# $File: comm-fixsize.plt
# $Date: Wed Aug 15 23:12:29 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set title "Relative Communication Cost Improvement (n=1e9)"
set xlabel "# Processes"
set ylabel "Decrease/Original Value"

set xtics 12 12
set xrange [10:125]
set yrange [0:1]

set terminal postscript enhanced color
set output "comm-fixsize.eps"

set style data linespoints

cd "../../data/optimized/qsort-ext"
plot "size1000000000" using 2:(1-$6/$5) notitle
