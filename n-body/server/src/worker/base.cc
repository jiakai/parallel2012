/*
 * $File: base.cc
 * $Date: Thu Aug 30 18:38:49 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/base.hh"
#include "config.hh"

#include <list>
using namespace conf;

WorkerBase::WorkerBase(Body *body, int nbody, SPTreeFactoryBase &tree_factory):
	m_tree_factory(tree_factory)

{
	alloc_bufs(body, nbody);
}

WorkerBase::~WorkerBase()
{
	destroy_bufs();
}

void WorkerBase::chg_body_buf(Body *body, int nbody)
{
	destroy_bufs();
	alloc_bufs(body, nbody);
}

void WorkerBase::alloc_bufs(Body *body, int nbody)
{
	m_body_num2ins = body;
	m_body.assign(new Body*[nbody], nbody);
	m_body_data = new BodyData[nbody];
	m_collision = new Body*[nbody];
	for (int i = 0; i < nbody; i ++)
	{
		body[i].num = i;
		m_body[i] = &body[i];
	}
}

void WorkerBase::destroy_bufs()
{
	delete []m_body.begin();
	delete []m_body_data;
	delete []m_collision;
}

void WorkerBase::update_pos(Body &body)
{
	body.center += body.velocity * time_delta;
	real_t r = body.radius;
	for (bool done = false; !done; )
	{
		done = true;
		for (int i = 0; i < 3; i ++)
		{
			real_t &p = body.center.comp(i),
				   d0 = p - r - range[i][0],
				   d1 = p + r - range[i][1];
			if (d0 < 0 || d1 > 0)
			{
				p -= (d0 < 0 ? d0 : d1) * 2;
				real_t &v = body.velocity.comp(i);
				v = -v;
				done = false;
			}
		}
	}
}

void WorkerBase::update_collision(Body &body, SPTreeBase *root)
{
	real_t time;
	const Body *tbody = root->get_collision_body(body, time);
	if (tbody && time <= time_delta)
	{
		Body s(body), t(*tbody);
		s.center += s.velocity * time;
		t.center += t.velocity * time;
		if (s.collide(t))
		{
			BodyData &d = m_body_data[body.num];
			d.ctnew = s.center - s.velocity * time;
			d.vnew = s.velocity;
			log_collision(&body);
		}
	}
}

void WorkerBase::update_gravity(Body &body, SPTreeBase *root)
{
	SPTreeBase::NodeData *data = root->get_data();
	Vector ct2syn(body.center, data->synthesis_body.center);
	real_t r;
	if ((r = ct2syn.mod()) > data->synthesis_min_dist)
	{
		body.velocity += ct2syn * (data->synthesis_body.mass / (r * r * r)) * time_delta;
		return;
	}
	if (data->is_leaf())
	{
		Vector asum;
		std::list<Vector> vclr; // asum in these directions should be cleared
		for (Body **p = data->body.begin(), **pt = data->body.end();
				p != pt; p ++)
		{
			Body &b = **p;
			if (&b != &body)
			{
				Vector t(body.center, b.center);
				r = 1 / t.mod();
				t *= r;
				asum += t * (b.mass * r * r);
				if (body.chk_overlap(b, EPS))
					vclr.push_back(t);
			}
		}

		// assume adjacent bodies are not movable
		bool clr_done = false;
		ITER_STD(vclr, i)
		{
			real_t f = asum.dot(*i);
			if (f > 0)
			{
				if (clr_done)
					return;
				asum -= *i * f;
				clr_done = true;
			}
		}
		body.velocity += asum * time_delta * 0.5;
	} else
	{
		update_gravity(body, data->chl.get_ptr());
		update_gravity(body, data->chr.get_ptr());
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

