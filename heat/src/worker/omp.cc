/*
 * $File: omp.cc
 * $Date: Tue Sep 04 06:58:01 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/omp.hh"
#include <cstdio>

#include <omp.h>

OMPWorker::OMPWorker(int nthread, int w, int h, real_t t0):
	WorkerBase(w, h, t0), m_nthread(nthread)
{
	printf("omp: nthread=%d\n", nthread);
	omp_set_num_threads(nthread);
}

real_t** OMPWorker::next_step(BoundaryBase &bd_cond)
{
	real_t **src = m_buf[m_cur_buf],
		   **dest = m_buf[m_cur_buf ^= 1];
#if 0
#pragma omp parallel for
	for (int i = 1; i < m_height - 1; i ++)
		update(update_arg_t(src, dest, 1, i, m_width - 2, 1));
#else
#pragma omp parallel for
	for (int i = 1; i < m_height - 1; i ++)
	{
		real_t *above = src[i - 1] + 1,
			   *cur = src[i] + 1,
			   *below = src[i + 1] + 1,
			   *t = dest[i] + 1;
		for (int j = 0; j < m_width - 2; j ++)
		{
			*(t ++) = (*above + *below + *(cur - 1) + *(cur + 1)) * 0.25;
			above ++;
			cur ++;
			below ++;
		}
	}
#endif
	bd_cond.update(dest, m_width, m_height);
	return dest;
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

