#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: plot.py
# $Date: Wed Sep 05 00:51:14 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

from pplt.data import scan_dir
from pplt.plot import effi_np, effi_niter, effi_imgsize

import matplotlib.pyplot as plt

fig_width_pt = 1100  # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27               # Convert pt to inch
fig_width = fig_width_pt*inches_per_pt  # width in inches
fig_height = fig_width * 0.64
fig_size =  [fig_width,fig_height]
params = {'axes.labelsize': 12,
          'text.fontsize': 14,
          'legend.fontsize': 12,
          'xtick.labelsize': 12,
          'ytick.labelsize': 12,
          'legend.shadow': True,
          'figure.figsize': fig_size}
plt.rcParams.update(params)

data = scan_dir('log')

for i in [effi_np, effi_niter, effi_imgsize]:
    i.plot(data)
plt.show()
