/*
 * $File: base.hh
 * $Date: Sun Sep 02 14:19:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_BOUNDARY_BASE_
#define _HEADER_BOUNDARY_BASE_

#include "config.hh"
class BoundaryBase
{
	public:
		virtual void update(real_t **buf, int width, int height) = 0;
		virtual ~BoundaryBase() {}
};

#endif // _HEADER_BOUNDARY_BASE_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

