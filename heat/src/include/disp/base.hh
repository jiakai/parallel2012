/*
 * $File: base.hh
 * $Date: Mon Sep 03 00:24:27 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_DISP_BASE_
#define _HEADER_DISP_BASE_

#include "worker/base.hh"
#include "boundary/base.hh"

class DispBase
{
	public:
		void run(WorkerBase &worker, BoundaryBase &bd_cond);
		virtual ~DispBase() {}

	protected:
		struct Pixel
		{
			int r, g, b;
		};

		/*!
		 * \brief return a color map allocated by the new[] operator
		 */
		static Pixel *load_colormap(int &ncolor);
		virtual void do_run(WorkerBase &worker, BoundaryBase &bd_cond) = 0;
};

#endif // _HEADER_DISP_BASE_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

