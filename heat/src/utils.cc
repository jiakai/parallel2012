/*
 * $File: utils.cc
 * $Date: Sun Sep 02 15:48:27 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "utils.hh"

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <cstdlib>
#include <cstdarg>
#include <cstdio>

MutexLock::MutexLock()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_init, &m_lock, NULL);
}

MutexLock::~MutexLock()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_destroy, &m_lock);
}

void MutexLock::acquire()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_lock, &m_lock);
}

void MutexLock::release()
{
	PTHREAD_CHKERR_CALL(pthread_mutex_unlock, &m_lock);
}

Condition::Condition()
{
	PTHREAD_CHKERR_CALL(pthread_cond_init, &m_cond, NULL);
}

Condition::~Condition()
{
	PTHREAD_CHKERR_CALL(pthread_cond_destroy, &m_cond);
}

void Condition::signal()
{
	PTHREAD_CHKERR_CALL(pthread_cond_signal, &m_cond);
}

void Condition::broadcast()
{
	PTHREAD_CHKERR_CALL(pthread_cond_broadcast, &m_cond);
}

void Condition::wait()
{
	PTHREAD_CHKERR_CALL(pthread_cond_wait, &m_cond, &m_lock);
}

void error_exit(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	putchar('\n');
	exit(EXIT_FAILURE);
}

HWTimer::HWTimer()
{
	reset();
}

void HWTimer::reset()
{
	gettimeofday(&m_start, NULL);
}

double HWTimer::get_sec() const
{
	timeval tv;
	gettimeofday(&tv, NULL);
	double ret = tv.tv_sec - m_start.tv_sec +
		1e-6 * (tv.tv_usec - m_start.tv_usec);
	if (ret < 0)
		ret += 24 * 3600;
	return ret;
}

double get_cputime()
{
	struct rusage u;
	getrusage(RUSAGE_SELF, &u);
#define t(x) x.tv_sec + x.tv_usec * 1e-6
	return t(u.ru_utime) + t(u.ru_stime);
#undef t
}

int get_nrcpu()
{
	return sysconf(_SC_NPROCESSORS_ONLN);
}


size_t errchk_read(int fd, void *buf, size_t len)
{
	ssize_t ret = read(fd, buf, len);
	if (ret == -1)
		error_exit("failed to read: %m");
	if (ret == 0)
		error_exit("failed to read: no data");
	return ret;
}

void errchk_write(int fd, const void *buf0, size_t len)
{
	char *buf = (char*)buf0;
	while (len)
	{
		ssize_t rst = write(fd, buf, len);
		if (rst == -1)
			error_exit("failed to read: %m");
		buf += rst;
		len -= rst;
	}
}

void readall(int fd, void *buf0, size_t len)
{
	char *buf = (char*)buf0;
	while (len)
	{
		size_t r = errchk_read(fd, buf, len);
		len -= r;
		buf += r;
	}
}

void FPSCalculator::trigger()
{
	double cur = m_timer.get_sec(), dt = cur - m_prev_time;
	if (dt > 5)
	{
		printf("%s_fps: %.3lf\n", m_name, m_nframe / dt);
		m_nframe = 0;
		m_prev_time = cur;
	}
	m_nframe ++;
}

// vim: syntax=cpp11.doxygen

