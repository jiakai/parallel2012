#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: run.py
# $Date: Mon Aug 27 14:45:29 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

if __name__ == '__main__':
    import sys
    if len(sys.argv) != 3:
        sys.exit('usage: {0} <host> <port>'.format(sys.argv[0]))
    from nbcl import main
    main(sys.argv[1], sys.argv[2])


