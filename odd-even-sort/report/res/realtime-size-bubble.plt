#!/usr/bin/env gnuplot
# $File: realtime-size-bubble.plt
# $Date: Tue Aug 14 11:33:13 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale 
set title "Bubble Sort" 
set xlabel "Data Size"
set ylabel "Real Run Time [sec]"

set xrange [500:5e8]

set terminal postscript enhanced color
set output "realtime-size-bubble.eps"

set style data linespoints

cd "../data/bubble-ext"
plot \
	"nproc12" using 1:4 title "nproc=12" , \
	"nproc24" using 1:4 title "nproc=24" , \
	"nproc36" using 1:4 title "nproc=36" , \
	"nproc48" using 1:4 title "nproc=48" , \
	"nproc60" using 1:4 title "nproc=60"
