# -*- coding: utf-8 -*-
# $File: data.py
# $Date: Tue Sep 04 08:20:25 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import re
import os.path
import os

class LogEntry(object):
    worker = None
    nthread = None
    niter = None
    width = None
    height = None

    tot_cpu = 0
    tot_real = 0

    def __str__(s):
        return '.'.join(str(i) for i in (s.worker, s.niter, s.nthread, s.size,
            s.chunksize)) + ': {0} {1}'.format(s.tot_cpu, s.tot_real)

    _re_tot_time = re.compile(r'^cputime=(.*) realtime=(.*) fps.*$')
    _re_cpu_mpi = re.compile(r'^ *CPU time.*: *([0-9.]*) sec.*$')

    def __init__(self, fpath):
        name = os.path.basename(fpath).split('.')
        self.worker = name[0]
        [self.nthread, self.niter, self.width, self.height] = [int(i)
                for i in name[1:-1]]
        with open(fpath) as f:
            if self.worker == 'mpi':
                self._parse_mpi(f)
            else:
                self._parse_normal(f)

    def _parse_normal(self, fobj):
        for line in fobj.readlines():
            m = self._re_tot_time.match(line)
            if m:
                self.tot_cpu = float(m.group(1))
                self.tot_real = float(m.group(2))

    def _parse_mpi(self, fobj):
        self._parse_normal(fobj)
        fobj.seek(0)
        for line in fobj.readlines():
            m = self._re_cpu_mpi.match(line)
            if m:
                self.tot_cpu = float(m.group(1))
                return

def scan_dir(path):
    ret = list()
    for i in os.listdir(path):
        l = LogEntry(os.path.join(path, i))
        if l.tot_cpu > 0:
            ret.append(l)
    return ret

