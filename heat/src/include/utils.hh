/*
 * $File: utils.hh
 * $Date: Sun Sep 02 15:46:32 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_UTILS_
#define _HEADER_UTILS_

#include <pthread.h>
#include <sys/time.h>

#include <cstddef>
#include <cstring>

class MutexLock
{
	protected:
		pthread_mutex_t m_lock;

	public:

		MutexLock();
		~MutexLock();
		void acquire();
		void release();
};

/*!
 * \brief print error information and exit the process
 */
void error_exit(const char *fmt, ...) __attribute__((format(printf, 1, 2),
			noreturn));

#define ITER_STD(vec, var) \
	for (typeof(vec.begin()) var = vec.begin(); var != vec.end(); var ++)


class ScopedLock
{
	MutexLock &m_lock;

	public:

		ScopedLock(MutexLock &lock):
			m_lock(lock)
		{
			m_lock.acquire();
		}

		~ScopedLock()
		{
			m_lock.release();
		}
};

#define __lock_name(x) __lock_ ## x 
#define _lock_name(x) __lock_name(x)

/*!
 * \brief keep the lock in the current scope
 */
#define LOCK(locker) ScopedLock _lock_name(__LINE__)(locker)


class Condition: public MutexLock
{
	pthread_cond_t m_cond;

	public:
		Condition();
		~Condition();

		void signal();
		void broadcast();
		void wait();
};


/*!
 * \brief a timer using the hardware clock to measure real time
 */
class HWTimer
{
	timeval m_start;

	public:
		
		HWTimer();
		void reset();
		double get_sec() const;
};

///< call a pthread function and checks for error
#define PTHREAD_CHKERR_CALL(func, ...) \
	do \
{ \
	int ret =  func(__VA_ARGS__); \
	if (ret) \
		error_exit("failed to call %s: %s", #func, strerror(ret)); \
} while(0)


template<typename T>
class ArrayWrapper
{
	T *m_arr, *m_arr_end;
	size_t m_size;

	public:
		ArrayWrapper(T *a = NULL, size_t s = 0):
			m_arr(a), m_arr_end(m_arr + s), m_size(s)
		{}

		void assign(T *a, size_t s)
		{
			m_arr = a;
			m_arr_end = a + s;
			m_size = s;
		}

		size_t size() const
		{ return m_size; }

		T* begin()
		{ return m_arr; }
		T* end()
		{ return m_arr_end; }

		const T* begin() const
		{ return m_arr; }
		const T* end() const
		{ return m_arr_end; }

		T& operator[] (size_t idx)
		{ return m_arr[idx]; }

		const T& operator[] (size_t idx) const
		{ return m_arr[idx]; }

		operator bool() const
		{ return m_arr != NULL; }
};

/*!
 * \brief return the current process CPU time in seconds
 */
double get_cputime();

/*!
 * \brief return the number of CPUs
 */
int get_nrcpu();

template<typename T>
T** alloc_2d(int width, int height)
{
	T **ret = new T*[height];
	for (int i = 0; i < height; i ++)
		ret[i] = new T[width];
	return ret;
}

template<typename T>
void free_2d(T **buf, int height)
{
	for (int i = 0; i < height; i ++)
		delete []buf[i];
	delete []buf;
}

class FPSCalculator
{
	HWTimer m_timer;
	const char *m_name;
	double m_prev_time;
	int m_nframe;
	public:
		FPSCalculator(const char *name):
			m_name(name), m_prev_time(m_timer.get_sec()), m_nframe(0)
		{}

		void trigger();
};

#endif // _HEADER_UTILS_

// vim: syntax=cpp11.doxygen

