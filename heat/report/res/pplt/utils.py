# -*- coding: utf-8 -*-
# $File: utils.py
# $Date: Tue Sep 04 19:20:22 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

__all__ = ['get_line_cycler', 'default_filter']

def get_line_cycler():
    lines = ['-', '-.', ':', '--']
    markers = ['o', 'v', '^', '<', '>',  '2', '3', '4', 's', 'p',
            '*', 'h', 'H', '+', 'x', 'D', 'd', '|', '_']
    lst = list()
    for i in range(len(markers)):
        lst.append(lines[i % len(lines)] + markers[i])
    from itertools import cycle
    return cycle(lst)

class default_filter(object):
    worker = 'pthread'
    nthread = 12
    niter = 500
    width = 10000
    height = 10000

    def __init__(self, **kargs):
        for (k, v) in kargs.iteritems():
            getattr(self, k) # AttributeError would be raised 
            setattr(self, k, v)

    def __call__(self, x):
        for i in vars(default_filter).keys():
            if i[0] == '_' or getattr(self, i) is None:
                continue
            if getattr(self, i) != getattr(x, i):
                return False
        return True

