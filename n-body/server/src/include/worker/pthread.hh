/*
 * $File: pthread.hh
 * $Date: Wed Aug 29 17:39:33 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_WORKER_PTHREAD_
#define _HEADER_WORKER_PTHREAD_

#include "worker/base.hh"
#include "utils.hh"

class PthreadWorker: public WorkerBase
{
	int m_nthread, m_nbody_remain, m_nthread_remain;
	MutexLock m_lock_collog, m_lock_nbodyrem;	// lock for collision log
	Condition m_cond_nthreadrem;

	typedef void (PthreadWorker::*phase_func_t) (Body&, SPTreeBase*);

	struct ThreadArg;
	friend struct ThreadArg;

	void log_collision(Body *body);
	static void* thread_wrapper(void*);

	void enter_multithread(ThreadArg &arg, phase_func_t func);

	void update_collision(Body &b, SPTreeBase *spt)
	{ WorkerBase::update_collision(b, spt); }
	void update_gravity(Body &b, SPTreeBase *spt)
	{ WorkerBase::update_gravity(b, spt); }
	void update_pos(Body &b, SPTreeBase*)
	{ WorkerBase::update_pos(b); }

	public:
		PthreadWorker(int nthread, Body *body, int nbody, SPTreeFactoryBase &tree_factory);

		const char* get_name()
		{ return "pthread"; }

		void next_step();
};

#endif // _HEADER_WORKER_PTHREAD_

// vim: syntax=cpp11.doxygen

