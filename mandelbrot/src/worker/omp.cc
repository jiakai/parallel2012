/*
 * $File: omp.cc
 * $Date: Sat Aug 25 16:41:26 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker.hh"
#include "config.hh"

#include <omp.h>

void worker_omp(ImgoutBase *imgout)
{
	if (!imgout)
		return;
	WORKER_INIT;
	omp_set_num_threads(conf::nthread);
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < conf::res_x; i ++)
	{
		real_t x = WORKER_COORD2X(i);
		for (int j = 0; j < conf::res_y; j ++)
			imgout->draw(i, j,
					get_niter(x, WORKER_COORD2Y(j)));
	}
}

void worker_omp_static(ImgoutBase *imgout)
{
	if (!imgout)
		return;
	WORKER_INIT;
	omp_set_num_threads(conf::nthread);
#pragma omp parallel for schedule(static)
	for (int i = 0; i < conf::res_x; i ++)
	{
		real_t x = WORKER_COORD2X(i);
		for (int j = 0; j < conf::res_y; j ++)
			imgout->draw(i, j,
					get_niter(x, WORKER_COORD2Y(j)));
	}
}

// vim: syntax=cpp11.doxygen

