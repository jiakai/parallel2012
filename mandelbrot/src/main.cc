/*
 * $File: main.cc
 * $Date: Sat Aug 25 15:52:16 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */


#include <mpi.h>	// it won't compile if put after asyncX11.h ...
#include <getopt.h>

#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <string>

#include "worker.hh"
#include "utils.hh"
#include "config.hh"

#include "imgout/asyncX11.hh"
#include "imgout/null.hh"
#include "imgout/png.hh"
#include "imgout/area.hh"

using namespace conf;

static void (*worker)(ImgoutBase*);
static ImgoutBase* (*make_imgout)();
static std::string png_filename;

static void parse_arg(int &argc, char **&argv);
static void usage(const char *name);
static ImgoutBase* make_imgout_x11()
{ return new AsyncX11Imgout("Mandelbrot Set"); }
static ImgoutBase* make_imgout_null()
{ return new NullImgout(); }
static ImgoutBase* make_imgout_png()
{ return new PNGImgout(png_filename.c_str()); }
static ImgoutBase* make_imgout_area()
{ return new AreaImgout(); }

namespace conf
{
	real_t
		domain_xmin = -2, domain_ymin = -2, domain_width = 4;
	int res_x = 600, res_y = 600, nthread = get_nrcpu(), niter_max = 2048;
}

int main(int argc, char **argv)
{
	parse_arg(argc, argv);
	bool is_mpi = (worker == worker_mpi), is_master = true;
	ImgoutBase *imgout = NULL;
	if (is_mpi)
	{
		MPI_Init(&argc, &argv);
		int rank;
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		if (!rank)
		{
			imgout = make_imgout();
			printf("niter=%d xres=%d yres=%d\n", niter_max, res_x, res_y);
		} else is_master = false;
	} else
	{
		printf("niter=%d xres=%d yres=%d", niter_max, res_x, res_y);
		if (worker != worker_seq)
			printf(" nthread=%d", nthread);
		printf("\n");
		imgout = make_imgout();
	}

	if (is_master)
	{
		do
		{
			printf("draw: x0=" REAL_T_FMT " y0=" REAL_T_FMT
					" width=" REAL_T_FMT "\n",
					domain_xmin, domain_ymin, domain_width);
			double start = get_cputime();
			HWTimer timer;
			imgout->init();
			worker(imgout);
			printf("realtime=%.3lf cputime=%.3lf\n",
					timer.get_sec(), get_cputime() - start);
		} while (imgout->wait());
		worker(NULL);
	} else
		worker(imgout);

	if (is_mpi)
		MPI_Finalize();

	delete imgout;
}

void parse_arg(int &argc, char **&argv)
{
	static const struct option long_opt[] = {
		{"backend", required_argument, NULL, 'b'},
		{"device", required_argument, NULL, 'd'},
		{"load", no_argument, NULL, 'l'},
		{"nthread", required_argument, NULL, 'n'},
		{"niter", required_argument, NULL, 'i'},
		{"xres", required_argument, NULL, 'x'},
		{"yres", required_argument, NULL, 'y'},
		{"help", no_argument, NULL, 'h'}
	};
	using namespace conf;

	for (; ;)
	{
		int c = getopt_long(argc, argv, "b:d:ln:i:x:y:h", long_opt, NULL);
		switch (c)
		{
			case -1:
				if (!worker)
					worker = worker_pthread;
				if (!make_imgout)
					make_imgout = make_imgout_x11;
				return;
			case 'b':
				if (!strcmp(optarg, "seq"))
					worker = worker_seq;
				else if (!strcmp(optarg, "omp"))
					worker = worker_omp;
				else if (!strcmp(optarg, "omp_static"))
					worker = worker_omp_static;
				else if (!strcmp(optarg, "pthread"))
					worker = worker_pthread;
				else if (!strcmp(optarg, "mpi"))
					worker = worker_mpi;
				else error_exit("invalid backend: %s\n", optarg);
				break;
			case 'd':
				if (!strcmp(optarg, "x11"))
					make_imgout = make_imgout_x11;
				else if (!strncmp(optarg, "png:", 4))
				{
					make_imgout = make_imgout_png;
					png_filename = optarg + 4;
				}
				else if (!strcmp(optarg, "null"))
					make_imgout = make_imgout_null;
				else if (!strcmp(optarg, "area"))
					make_imgout = make_imgout_area;
				else error_exit("invalid device: %s\n", optarg);
				break;
			case 'l':
				load_domain();
				break;
			case 'n':
				if (sscanf(optarg, "%d", &nthread) != 1 || nthread < 1)
					error_exit("invalid argument for nthread: %s\n", optarg);
				break;
			case 'i':
				if (sscanf(optarg, "%d", &niter_max) != 1 || niter_max < 10)
					error_exit("invalid argument for niter: %s\n", optarg);
				break;
			case 'x':
				if (sscanf(optarg, "%d", &res_x) != 1)
					error_exit("invalid argument for xres: %s\n", optarg);
				break;
			case 'y':
				if (sscanf(optarg, "%d", &res_y) != 1)
					error_exit("invalid argument for yres: %s\n", optarg);
				break;
			default: // for '?' and 'h':
				usage(argv[0]);
				exit(c == '?');
		}
	}
}

void usage(const char *name)
{
	fprintf(stderr, "usage: %s --backend|-b <backend> --device|-d <output device> [options]\n", name);
	fprintf(stderr, "%s",
			"  where backend is one of: seq, pthread[default], omp, omp_static, mpi\n"
			"        output device is one of: x11[default], png, null, area\n"
			"  options are:\n"
			"    --load, -l       load domain from " DOMAIN_FILEPATH "\n"
			"    --nthread, -n    number of threads to use (for omp and pthread)\n"
			"    --niter, -i      maximal number of iterations per point\n"
			"    --xres, -x       X resolution\n"
			"    --yres, -y       Y resolution\n"
			"    --help, -h       display this help\n"
			"  for x11 output device:\n"
			"    key  bindings:\n"
			"      q   quit\n"
			"      s   save domain to " DOMAIN_FILEPATH "\n"
			"      z   zoom out by a factor of 2\n"
			"    mouse operations:\n"
			"      left click       mark the corner of resize region\n"
			"      right click      make the point the center of the graph\n"
			"  for png output device:\n"
			"      use png:<filename> to specify the output filename\n"
			);
}

// vim: syntax=cpp11.doxygen
