/*
 * $File: base.cc
 * $Date: Tue Sep 04 20:56:58 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/base.hh"

#include <emmintrin.h>

#include <cstring>
#include <cassert>

#include <algorithm>

WorkerBase::WorkerBase(int w, int h, real_t t0):
	m_cur_buf(0), m_width(w), m_height(h)
{
	int real_w  = ((w - 1) / 4 + 2) * 4;
	m_buf_size = real_w * h;
	for (int i = 0; i < 2; i ++)
	{
		m_buf[i] = new real_t*[h];
		real_t *buf = new real_t[m_buf_size + 8];
		m_buf_realpos[i] = buf;
		buf = (real_t*)(((size_t)buf + 16) & ~0xFul) - 1;
		std::fill(buf, buf + m_buf_size, t0);
		for (int j = 0; j < h; j ++)
			m_buf[i][j] = buf + j * real_w;
	}
}

WorkerBase::~WorkerBase()
{
	for (int i = 0; i < 2; i ++)
	{
		delete []m_buf_realpos[i];
		delete []m_buf[i];
	}
}

void WorkerBase::update(const update_arg_t &arg)
{
#if 0
	real_t sse_mul_opr_buf[] __attribute__ ((aligned(16))) = {0.25, 0.25, 0.25, 0.25};
	__m128 register sse_mul_opr = _mm_load_ps(sse_mul_opr_buf);
	int niter = (arg.width - 1) / 4 + 1;
	for (int i = arg.y0, it = arg.y0 + arg.height; i < it; i ++)
	{
		real_t *above = arg.src[i - 1] + arg.x0,
			   *cur = arg.src[i] + arg.x0 - 1,
			   *below = arg.src[i + 1] + arg.x0,
			   *t = arg.dest[i] + arg.x0;
		for (int register j = niter; j; j --)
		{
			_mm_store_ps(t,
					_mm_add_ps(_mm_load_ps(above), _mm_load_ps(below)));
			above += 4;
			below += 4;
			*(t ++) += *cur + *(cur + 2); cur ++;
			*(t ++) += *cur + *(cur + 2); cur ++;
			*(t ++) += *cur + *(cur + 2); cur ++;
			*(t ++) += *cur + *(cur + 2); cur ++;
			_mm_store_ps(t - 4,
					_mm_mul_ps(_mm_load_ps(t - 4), sse_mul_opr));
		}
	}
#else
	for (int i = arg.y0, it = arg.y0 + arg.height; i < it; i ++)
	{
		real_t *above = arg.src[i - 1] + arg.x0,
			   *cur = arg.src[i] + arg.x0,
			   *below = arg.src[i + 1] + arg.x0,
			   *t = arg.dest[i] + arg.x0;
		for (int j = 0; j < arg.width; j ++)
		{
			*(t ++) = (*above + *below + *(cur - 1) + *(cur + 1)) * 0.25;
			above ++;
			cur ++;
			below ++;
		}
	}
#endif
}

void WorkerBase::divide_task(update_arg_t *range, int n) const
{
	int w = m_width - 2,
		h = m_height - 2;
	for (int i = 0, cury = 1, dh = h / n + 1, rem = h % n; i < n; i ++)
	{
		if (i == rem)
			dh --;
		update_arg_t &a = range[i];
		a.x0 = 1;
		a.width = w;
		a.y0 = cury;
		a.height = dh;
		cury += dh;
	}
}

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

