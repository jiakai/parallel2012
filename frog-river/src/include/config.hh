/*
 * $File: config.hh
 * $Date: Sat Aug 18 12:23:50 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_CONFIG_
#define _HEADER_CONFIG_

#define FROG_IMG_PATH		"res/frog.png"
#define VEHICLE_IMG_PATH	"res/vehicle.png"
const int
	WINDOW_WIDTH = 800,
	WINDOW_HEIGHT = 200,
	REFRESH_INTERVAL = 50000, // microsecond
	NROW = 5,
	NVEHICLE_PER_ROW = 4,
	VEHICLE_HEIGHT = 30,
	TOP_SPACE_SIZE = 10;	// space above the first row

const double FROG_MOVE_ACCEL = 0.8;

static inline int get_velocity(int row, int col)
{
	double ret = row * 1.6 + col - 1.5;
	ret = ret < 0 ? -ret : ret;
	if (ret < 0.8)
		ret = 0.8;
	if (col < 2)
		ret = -ret;
	ret *= 2;
	return ret;
}

#endif // _HEADER_CONFIG_

// vim: syntax=cpp11.doxygen

