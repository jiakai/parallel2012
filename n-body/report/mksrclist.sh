#!/bin/bash -e
# $File: mksrclist.sh
# $Date: Fri Aug 31 01:35:33 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

for i in $(find ../server/src -type f)
do
	echo "\\subsubsection{${i#../server/src/}}"
	echo "\\cppsrc{${i#../server/}}"
done

