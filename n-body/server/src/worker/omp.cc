/*
 * $File: omp.cc
 * $Date: Fri Aug 31 10:30:16 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/omp.hh"
#include "config.hh"
#include "profile.hh"

#include <omp.h>

using namespace conf;

OMPWorker::OMPWorker(int nthread, Body *body, int nbody,
		SPTreeFactoryBase &tree_factory):
	WorkerBase(body, nbody, tree_factory)
{
	printf("OpenMP: num_threads=%d\n", nthread);
	omp_set_num_threads(nthread);
}

void OMPWorker::next_step()
{
	m_ncollision = 0;

	sptptr_t tree = m_tree_factory.mktree(m_body);
	Body **body = m_body.begin();
	int nbody = m_body.size();

	profile_start("omp_gravity");
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < nbody; i ++)
		update_gravity(*body[i], tree.get_ptr());
	profile_end("omp_gravity");

	profile_start("omp_collision");
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < nbody; i ++)
		update_collision(*body[i], tree.get_ptr());
#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < m_ncollision; i ++)
	{
		Body &b = *m_collision[i];
		BodyData &d = m_body_data[b.num];
		b.center = d.ctnew;
		b.velocity = d.vnew;
	}
	profile_end("omp_collision");

#pragma omp parallel for schedule(dynamic)
	for (int i = 0; i < nbody; i ++)
		update_pos(*body[i]);
}

void OMPWorker::log_collision(Body *body)
{
	LOCK(m_lock_collog);
	m_collision[m_ncollision ++] = body;
}

// vim: syntax=cpp11.doxygen

