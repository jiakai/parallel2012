#!/usr/bin/env gnuplot
# $File: totcpu-nproc-qsort.plt
# $Date: Thu Aug 16 23:34:18 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

set size 0.7, 0.7
set logscale y
set title "Quick Sort"
set xlabel "# Processes"
set ylabel "Total CPU Time [sec]"
set xtics 12 12
set xrange [10:150]

set terminal postscript enhanced color
set output "totcpu-nproc-qsort.eps"

set style data linespoints
set style line 6 lt 6 lw 1 pt 6 ps 0.8 lc rgb 'red'

cd "../data/qsort-ext"
plot \
	"size10000000" using 2:3 title "n=1e7" , \
	"size50000000" using 2:3 title "n=5e7" , \
	"size100000000" using 2:3 title "n=1e8" , \
	"size500000000" using 2:3 title "n=5e8" , \
	"size1000000000" using 2:3 title "n=1e9" , \
	"size5000000000" using 2:3 title "n=5e9" ls 6, \
	x >= 10 && x <= 125 ? 3.238636267824228 * x * (log(1000000000) - log(x)) : 0/0 title "k*m*n*(log(n)-log(m))"
