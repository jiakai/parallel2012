/*
 * $File: refcntptr.hh
 * $Date: Sun Aug 26 15:31:58 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_REFCNTPTR_
#define _HEADER_REFCNTPTR_

#include "utils.hh"

/*!
 * \brief reference count pointer
 */
template<typename T>
class RefCntPtr
{
	struct Holder
	{
		T* ptr;
		int cnt;
		MutexLock lock;

		Holder(T* p):
			ptr(p), cnt(1)
		{}

		~Holder()
		{ delete ptr; ptr = NULL; }
	};
	Holder *m_holder;

	void incr_ref()
	{
		if (!m_holder)
			return;
		LOCK(m_holder->lock);
		m_holder->cnt ++;
	}

	void decr_ref()
	{
		if (!m_holder)
			return;
		m_holder->lock.acquire();
		if (!(-- m_holder->cnt))
		{
			m_holder->lock.release();
			delete m_holder;
			m_holder = NULL;
		} else
			m_holder->lock.release();
	}

	public:
		RefCntPtr(T *p):
			m_holder(new Holder(p))
		{}

		RefCntPtr():
			m_holder(NULL)
		{}

		RefCntPtr(const RefCntPtr &r):
			m_holder(r.m_holder)
		{ incr_ref(); }

		~RefCntPtr()
		{ decr_ref(); }

		RefCntPtr& operator = (const RefCntPtr &r)
		{
			if (m_holder != r.m_holder)
			{
				decr_ref();
				m_holder = r.m_holder;
				incr_ref();
			}
			return *this;
		}

		T* get_ptr() const { return m_holder->ptr; }

		T* operator -> () const { return get_ptr(); }
		T& operator *() const { return *get_ptr(); }

		bool operator == (const RefCntPtr<T> &p) const
		{ return m_holder->ptr == p.m_holder->ptr; }

		bool operator == (T *p) const
		{ return m_holder->ptr == p; }

		operator bool() const
		{ return m_holder->ptr != NULL; }
};

#endif // _HEADER_REFCNTPTR_

// vim: syntax=cpp11.doxygen

