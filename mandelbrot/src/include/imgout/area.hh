/*
 * $File: area.hh
 * $Date: Sat Aug 25 15:48:42 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_IMGOUT_AREA_
#define _HEADER_IMGOUT_AREA_

#include "imgout/base.hh"

#include "utils.hh"

class AreaImgout: public ImgoutBase
{
	int m_cnt;
	MutexLock m_lock_cnt;

	public:
		AreaImgout();

		void draw(const Point *buf, size_t size);
		void init() {}
		bool wait();
};

#endif // _HEADER_IMGOUT_AREA_

// vim: syntax=cpp11.doxygen

