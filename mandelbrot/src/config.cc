/*
 * $File: config.cc
 * $Date: Fri Aug 24 10:50:53 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "config.hh"

#include <cstdio>
#include <fstream>

namespace conf
{
	void save_domain()
	{
		FILE *fout = fopen(DOMAIN_FILEPATH, "wb");
		real_t buf[] = {domain_xmin, domain_ymin, domain_width};
		fwrite(buf, sizeof(real_t), 3, fout);
		fclose(fout);
	}

	void load_domain()
	{
		FILE *fin = fopen(DOMAIN_FILEPATH, "rb");
		real_t buf[3];
		fread(buf, sizeof(real_t), 3, fin);
		fclose(fin);
		domain_xmin = buf[0];
		domain_ymin = buf[1];
		domain_width = buf[2];
	}
	/*
	void save_domain()
	{
		FILE *fout = fopen(DOMAIN_FILEPATH, "w");
		fprintf(fout, "%.9Le %.9Le %.9Le\n", domain_xmin, domain_ymin, domain_width);
		fclose(fout);
	}
	void load_domain()
	{
		std::ifstream fin(DOMAIN_FILEPATH);
		fin >> domain_xmin >> domain_ymin >> domain_width;
	}
	*/
}

// vim: syntax=cpp11.doxygen

