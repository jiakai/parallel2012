/*
 * $File: pthread-prefork.cc
 * $Date: Fri Aug 31 10:16:51 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#include "worker/pthread-prefork.hh"
#include "profile.hh"
#include <cstdio>

struct PthreadPreforkWorker::ThreadArg
{
	typedef void (PthreadPreforkWorker::*phase_func_t) (Body&, SPTreeBase*);

	PthreadPreforkWorker *self;
	phase_func_t func;
	SPTreeBase *root;
	PthreadPreforkWorker::ThreadCtlInfo *ctl;
};

PthreadPreforkWorker::PthreadPreforkWorker(int nthread,
		Body *body, int nbody, SPTreeFactoryBase &tree_factory):
	WorkerBase(body, nbody, tree_factory),
	m_nthread(nthread), m_targ(new ThreadArg[nthread * 3])
{
	printf("pthread(prefork): nslave=%d\n", nthread);
	for (int i = 0; i < nthread; i ++)
	{
		pthread_t pt;

		ThreadArg *t = m_targ + i;
		t->self = this;
		t->func = &PthreadPreforkWorker::update_collision;
		t->ctl = &m_tctl_collision;
		PTHREAD_CHKERR_CALL(pthread_create, &pt, NULL, thread_wrapper, t);

		t += m_nthread;
		t->self = this;
		t->func = &PthreadPreforkWorker::update_gravity;
		t->ctl = &m_tctl_gravity;
		PTHREAD_CHKERR_CALL(pthread_create, &pt, NULL, thread_wrapper, t);

		t += m_nthread;
		t->self = this;
		t->func = &PthreadPreforkWorker::update_pos;
		t->ctl = &m_tctl_pos;
		PTHREAD_CHKERR_CALL(pthread_create, &pt, NULL, thread_wrapper, t);
	}
}

PthreadPreforkWorker::~PthreadPreforkWorker()
{
	trigger_thread(m_tctl_collision, TSIG_EXIT);
	trigger_thread(m_tctl_gravity, TSIG_EXIT);
	trigger_thread(m_tctl_pos, TSIG_EXIT);
	delete []m_targ;
}

void PthreadPreforkWorker::trigger_thread(ThreadCtlInfo &ctl, tsig_t tsig)
{
	{
		LOCK(m_cond_nthreadrem);
		m_nthread_remain = m_nthread;
	}
	{
		LOCK(m_lock_nbodyrem);
		m_nbody_remain = m_body.size();
	}
	{
		LOCK(ctl.cond);
		ctl.tsig = tsig;
		ctl.tasknum ++;
	}
	ctl.cond.broadcast();
	LOCK(m_cond_nthreadrem);
	while (m_nthread_remain)
		m_cond_nthreadrem.wait();
}

void* PthreadPreforkWorker::thread_wrapper(void *arg_)
{
	pthread_detach(pthread_self());
	ThreadArg *arg = static_cast<ThreadArg*>(arg_);
	PthreadPreforkWorker *self = arg->self;
	ThreadArg::phase_func_t func = arg->func;
	ThreadCtlInfo &ctl = *(arg->ctl);
	int prev_tasknum = ctl.tasknum;

	Body **body = self->m_body.begin();
	int &nbodyrem = self->m_nbody_remain;

	for (; ;)
	{
		tsig_t tsig;
		{
			LOCK(ctl.cond);
			while (ctl.tsig == TSIG_WAIT || ctl.tasknum == prev_tasknum)
				ctl.cond.wait();
			tsig = ctl.tsig;
			prev_tasknum = ctl.tasknum;
		}
		if (tsig == TSIG_START)
		{
			for (; ;)
			{
				int cur;
				{
					LOCK(self->m_lock_nbodyrem);
					if (!nbodyrem)
						break;
					cur = --nbodyrem;
				}
				(self->*func)(*body[cur], arg->root);
			}
		}
		{
			LOCK(self->m_cond_nthreadrem);
			self->m_nthread_remain --;
		}
		self->m_cond_nthreadrem.signal();
		if (tsig == TSIG_EXIT)
			return NULL;
	}
}

void PthreadPreforkWorker::next_step()
{
	m_ncollision = 0;

	sptptr_t tree = m_tree_factory.mktree(m_body);
	for (int i = 0; i < m_nthread * 2; i ++)
		m_targ[i].root = tree.get_ptr();

	profile_start("pthreadpf_gravity");
	trigger_thread(m_tctl_gravity);
	profile_end("pthreadpf_gravity");

	profile_start("pthreadpf_collision");
	trigger_thread(m_tctl_collision);
	for (int i = 0; i < m_ncollision; i ++)
	{
		Body &b = *m_collision[i];
		BodyData &d = m_body_data[b.num];
		b.center = d.ctnew;
		b.velocity = d.vnew;
	}
	profile_end("pthreadpf_collision");

	trigger_thread(m_tctl_pos);
}

void PthreadPreforkWorker::log_collision(Body *body)
{
	LOCK(m_lock_collog);
	m_collision[m_ncollision ++] = body;
}

// vim: syntax=cpp11.doxygen

