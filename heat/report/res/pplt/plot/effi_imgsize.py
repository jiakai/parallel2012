# -*- coding: utf-8 -*-
# $File: effi_imgsize.py
# $Date: Wed Sep 05 00:31:40 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

"""parallel efficiency vs # processes"""

from pplt.utils import *
import matplotlib.pyplot as plt

def plot_efficiency(data, ax, workers,
        nthread = default_filter.nthread, thname = 'Threads'):
    baseline = filter(default_filter(worker = 'seq', niter = None), data)
    baseline = {i.niter: i for i in baseline}
    ax.set_ylabel('Efficiency')
    ax.set_xlabel('# Iterations')
    ax.set_title('({0} {1})'.format(nthread, thname))
    cyc = get_line_cycler()
    for wk in workers:
        d = filter(default_filter(worker = wk,
            nthread = nthread, niter = None), data)
        d.sort(cmp = lambda x, y: cmp(x.niter, y.niter))
        ax.plot([i.niter for i in d],
                [baseline[i.niter].tot_real / (i.tot_real * i.nthread)
                    for i in d], next(cyc), label = i.worker)
    ax.legend()


NTHREAD = 48

def plot_field(data, ax, field, label):
    baseline = filter(default_filter(**{'worker': 'seq', field: None}), data)
    baseline = {getattr(i, field): i for i in baseline}
    data = filter(default_filter(**{
        'worker': 'mpi', field: None, 'nthread': NTHREAD}), data)
    data.sort(cmp = lambda x, y: cmp(getattr(x, field), getattr(y, field)))
    cyc = get_line_cycler()
    ax.plot([getattr(i, field) for i in data],
            [baseline[getattr(i, field)].tot_real / (i.tot_real * i.nthread)
                for i in data],
            next(cyc), label = label)
    for i in data:
        print field, i.width, i.height

def do_plot(data, ax):
    plot_field(data, ax, 'width', 'width')
    plot_field(data, ax, 'height', 'height')
    ax.set_xlabel('Image Size (the other dimension fixed as {})'.
            format(default_filter.width))
    ax.set_ylabel('Efficiency')
    ax.set_title('Efficiency vs Image Size (MPI,{} iters,{} processes)'.
            format(default_filter.niter, NTHREAD))
    ax.legend()

def plot(data):
    fig = plt.figure()
    do_plot(data, fig.add_subplot(111))
    plt.savefig('effi-imgsize.pdf')

