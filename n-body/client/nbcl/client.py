# -*- coding: utf-8 -*-
# $File: client.py
# $Date: Tue Aug 28 01:07:17 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

import socket
import struct
from array import array

def rand(min = 0.2, max = 1):
    from random import random
    return (max - min) * random() + min

class Body(object):
    mass = None
    radius = None
    color = None # list(r, g, b) in 0, 1

    def __init__(self, m, r):
        self.mass = m
        self.radius = r
        self.color = [rand() for i in range(3)]

class Client(object):
    _sock = None
    body = None
    time_delta = None
    boundary = None # list(tuple(min, max))

    def __init__(self, srvaddr, port):
        self._sock = socket.create_connection((srvaddr, port))

        buf = self.recv_double(7)
        self.time_delta = buf[0]
        buf = buf[1:]
        self.boundary = [(buf[i * 2], buf[i * 2 + 1]) for i in range(3)]
        nbody = self.recv_int32(1)[0]
        metadata = self.recv_double(nbody * 2)
        self.body = list()
        for i in range(nbody):
            self.body.append(Body(metadata[i * 2], metadata[i * 2 + 1]))

    def recv_str(self, cnt):
        buf = bytearray(cnt)
        mv = memoryview(buf)
        cur = 0
        while cnt:
            size = self._sock.recv_into(mv[cur:], cnt)
            cur += size
            cnt -= size
        return mv.tobytes()

    def recv_int32(self, cnt):
        """:return: array<int>"""
        arr = array('i')
        arr.fromstring(self.recv_str(cnt * arr.itemsize))
        return arr

    def recv_double(self, cnt):
        """:return: array<double>"""
        arr = array('d')
        arr.fromstring(self.recv_str(cnt * arr.itemsize))
        return arr

    def send_int32(self, *args):
        self._sock.sendall(array('i', args))

    def close(self):
        self._sock.close()

