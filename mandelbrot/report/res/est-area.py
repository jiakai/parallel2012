#!/usr/bin/env python2
# -*- coding: utf-8 -*-
# $File: est_area.py
# $Date: Sat Aug 25 16:11:49 2012 +0800
# $Author: jiakai <jia.kai66@gmail.com>

def mat2str(m):
    return 'matrix(' + ','.join(('[' + ','.join(i) + ']') for i in m) + ')'

mat0 = list()
mat1 = list()
while True:
    try:
        d = raw_input().strip()
        if d[0] == '#':
            continue
        d = d.split(' ')
        if d[0] == 'seq':
            mat1.append([d[2], d[6]])
        else:
            mat0.append([d[2], d[6]])
    except EOFError:
        break

print 'load(lsquares)$;'
print 'M0:', mat2str(mat0), ';'
print 'M1:', mat2str(mat1), ';'
print 't0: subst(lsquares_estimates(M0, [n, T], t0 * n = T, [t0])[1], t0);'
print 'sol: lsquares_estimates(M1, [n, T], t0 * (alpha * n + c) = T, [alpha, c]);'
print 'subst(sol[1], float(alpha * 16));'
