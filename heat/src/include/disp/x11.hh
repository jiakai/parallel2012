/*
 * $File: x11.hh
 * $Date: Mon Sep 03 00:30:43 2012 +0800
 * $Author: jiakai <jia.kai66@gmail.com>
 */

#ifndef _HEADER_DISP_X11_
#define _HEADER_DISP_X11_

#include "disp/base.hh"
#include "utils.hh"

#include <X11/Xlib.h>

class X11Disp: public DispBase
{
	const char *m_window_name, *m_disp_name;
	bool m_stop_flag;
	real_t m_val_max;
	int m_width, m_height;

	Display *m_display;
	Window m_win;
	GC *m_gc, **m_pt_gc;
	int m_ngc;

	MutexLock m_lock_stop_flag;

	void do_run(WorkerBase &worker, BoundaryBase &bd_cond);
	void init_gc();

	static void* thread_draw_wrapper(void*self);
	void thread_draw();
	void init_x11();

	public:
		X11Disp(real_t val_max, int width, int height,
				const char *window_name, const char *disp_name = NULL);
		~X11Disp();
};

#endif // _HEADER_DISP_X11_

// vim: syntax=cpp11.doxygen foldmethod=marker foldmarker=f{{{,f}}}

